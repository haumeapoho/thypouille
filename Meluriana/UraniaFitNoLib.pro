include("../Moonfit/moonfitNoLib.pri")

#name of the executable file generated
TARGET = Urania3.19


#put += console to run in a separate terminal
#CONFIG += console
CONFIG -= QT

#bundles might be required for MAC OS ??
#CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
	bootstrap.h \
    cytonSimu.h \
    distribution.h \
    dividingmodelOnePop.h \
    dividingmodelTwoPops.h \
    mainOldScripts.h \
    mainTwoPops.h \
    random.h \
    statistiques.h \
    expLabelling.h \
    wholethymus.h

SOURCES += \
    cytonSimu.cpp \
    distribution.cpp \
    dividingmodelOnePop.cpp \
    dividingmodelTwoPops.cpp \
    main.cpp \
    mainTwoPops.cpp \
    mainoOldScripts.cpp \
    random.cpp \
    statistiques.cpp \
    wholethymus.cpp \
    explabelling.cpp
