#ifndef CYTONWIDGET_H
#define CYTONWIDGET_H

#include <QWidget>
#include "distribwidget.h"
#include "grapheCustom.h"
#include "cytonSimu.h"

namespace Ui {
class CytonWidget;
}

class CytonWidget : public QWidget
{
    Q_OBJECT

public:
    int cpt;
    explicit CytonWidget(stage* st, QWidget *parent = 0);
    ~CytonWidget();
    void reset(stage *st);
    void applyParamsFromStage();
    void applyParamsToStage();

    void analyze(double time);

public slots:
    //void GFPcheckChanged();
    //void hypDeathChanged();
    //void hypProlifChanged();
    //void run();
    void enable(bool enable);

private:
    stage* thisStage;


    grapheCustom* grapheEDU_BRDU;
    grapheCustom* grapheCyclePost;
    grapheCustom* grapheCyclePre;
    grapheCustom* grapheCycleMixed;

    DistribWidget* currentDistribWidgetDeath;
    DistribWidget* currentDistribWidgetG0;
    DistribWidget* currentDistribWidgetG1;
    DistribWidget* currentDistribWidgetS;
    DistribWidget* currentDistribWidgetG2;
    DistribWidget* currentDistribWidgetTot;
    Ui::CytonWidget *ui;

    bool running;
};

#endif // CYTONWIDGET_H
