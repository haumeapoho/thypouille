#ifndef STATISTIQUES_H
#define STATISTIQUES_H

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath> // NAN
using namespace std;
//double uni_real();
//double uni_int(int low, int high);
//int binomial(const int size, const double probability);


class observer{
public:
    observer(std::string _filename): filename(_filename) {}
    ~observer(){}
    std::string filename;
    std::vector<double> variable;
    std::vector<double> time;
    // 2019-05-08
    double last(){if(variable.size() > 0) return variable.back(); else return 0;} // to change
    void writeToFile(){
        std::ofstream outFile(filename.c_str(),std::ios::out);
        size_t row = variable.size();
        if (outFile){
            for (size_t i = 0; i<row; ++i)
                outFile << time[i] << "\t" << variable[i] << std::endl;
            outFile.close();
        }
        else
            std::cout << "Observer: the file cannot be created." << filename << std::endl;
    }
    void pushData(double value, double _time){
        variable.push_back(value);
        time.push_back(_time);
    }
    void clear(){
        variable.clear();
        time.clear();
    }
};

class observerTable{
public:
    observerTable(std::string _filename, int _nbCols): filename(_filename), nbCols(_nbCols) {if(nbCols > 10) cerr << "observerTable is not programmed for more than 10 columns/n";}
    ~observerTable(){}
    std::string filename;
    int nbCols;


    std::vector<string> colNames;
    std::vector< vector<double> > variable;

    void writeToFile(){
        size_t row = variable.size();
        if(row == 0) return;
        std::ofstream outFile(filename.c_str(),std::ios::out);
        if (outFile){
            for(int j = 0; j < nbCols; ++j){
                outFile << (j == 0 ? "" : "\t") << colNames[j] << std::endl;
            }
            outFile << "/n";
            for (size_t i = 0; i < row; ++i){
                if(static_cast<int>(variable[i].size()) != nbCols) cerr << "AAARGH WTF the size of the observerTable is not square /n";
                for(int j = 0; j < nbCols; ++j){
                    outFile << (j == 0 ? "" : "\t") << variable[i][j] << std::endl;
                }
                outFile << "/n";
            }
            outFile.close();
        }
        else
            std::cout << "Observer: the file cannot be created." << filename << std::endl;
    }
    void pushData(double v1 = NAN, double v2 = NAN, double v3 = NAN, double v4 = NAN, double v5 = NAN, double v6 = NAN, double v7 = NAN, double v8 = NAN, double v9 = NAN, double v10 = NAN){
        vector<double> newLine = vector<double>();
        newLine.resize(nbCols);
        if(nbCols <= 1) newLine[0] = v1;
        if(nbCols <= 2) newLine[1] = v2;
        if(nbCols <= 3) newLine[2] = v3;
        if(nbCols <= 4) newLine[3] = v4;
        if(nbCols <= 5) newLine[4] = v5;
        if(nbCols <= 6) newLine[5] = v6;
        if(nbCols <= 7) newLine[6] = v7;
        if(nbCols <= 8) newLine[7] = v8;
        if(nbCols <= 9) newLine[8] = v9;
        if(nbCols <= 10) newLine[9] = v10;
        variable.push_back(newLine);
    }
    void giveColNames(string v1 = string(""), string v2 = string(""), string v3 = string(""), string v4 = string(""), string v5 = string(""), string v6 = string(""), string v7 = string(""), string v8 = string(""), string v9 = string(""), string v10 = string("")){
        if(nbCols <= 1) colNames[0] = v1;
        if(nbCols <= 2) colNames[1] = v2;
        if(nbCols <= 3) colNames[2] = v3;
        if(nbCols <= 4) colNames[3] = v4;
        if(nbCols <= 5) colNames[4] = v5;
        if(nbCols <= 6) colNames[5] = v6;
        if(nbCols <= 7) colNames[6] = v7;
        if(nbCols <= 8) colNames[7] = v8;
        if(nbCols <= 9) colNames[8] = v9;
        if(nbCols <= 10) colNames[9] = v10;
    }

    void clear(){
        variable.clear();
    }
};


class observerOneSim {
public:
    observerOneSim(string folder = string("")) :
        o_doseEDU(folder + string("doseEDU.txt")),
        o_doseBRDU(folder + string("doseBRDU.txt")),
        o_popSize(folder + string("popSize.txt")),
        o_prcBRDU(folder + string("pctBRDU+.txt")),
        o_prcEDU(folder + string("pctEDU+.txt")),
        o_prcEDUpBRDUp(folder + string("pctEDU+BRDU+.txt")),
        o_prcEDUpBRDUn(folder + string("pctEDU+BRDU-.txt")),
        o_prcEDUnBRDUp(folder + string("pctEDU-BRDU+.txt")),
        o_prcEDUnBRDUn(folder + string("pctEDU-BRDU-.txt")),
        o_avgGen(folder + string("AvgGen.txt")),
        o_nbG1(folder + string("nbG1.txt")),
        o_nbS(folder + string("nbS.txt")),
        o_nbG2M(folder + string("nbG2M.txt")),
        o_nbG0(folder + string("nbG0.txt")),
        o_nbNewcomers(folder + string("nbNew.txt")),
        o_flowOut(folder + string("flowOut.txt")),
        o_avgGenOut(folder + string("avgGenOut.txt")),
        o_gen0(folder + string("ng0.txt")),
        o_gen1(folder + string("ng1.txt")),
        o_gen2(folder + string("ng2.txt")),
        o_gen3(folder + string("ng3.txt")),
        o_gen4(folder + string("ng4.txt")),
        o_gen5(folder + string("ng5.txt")),
        o_gen6(folder + string("ng6.txt")),
        o_gen7(folder + string("ng7.txt")),
        o_gen8(folder + string("ng8.txt")),
        o_gen9(folder + string("ng9.txt")),
        o_gen10(folder + string("ng10.txt")),
        o_apopt(folder + string("apopt.txt"))
    {}

    observer o_doseEDU;
    observer o_doseBRDU;
    observer o_popSize;
    observer o_prcBRDU;
    observer o_prcEDU;
    observer o_prcEDUpBRDUp;
    observer o_prcEDUpBRDUn;
    observer o_prcEDUnBRDUp;
    observer o_prcEDUnBRDUn;
    observer o_avgGen;
    observer o_nbG1;
    observer o_nbS;
    observer o_nbG2M;
    observer o_nbG0;
    observer o_nbNewcomers;
    observer o_flowOut;
    observer o_avgGenOut;
    observer o_gen0;
    observer o_gen1;
    observer o_gen2;
    observer o_gen3;
    observer o_gen4;
    observer o_gen5;
    observer o_gen6;
    observer o_gen7;
    observer o_gen8;
    observer o_gen9;
    observer o_gen10;
    observer o_apopt;

    void clear(){
        o_doseEDU.clear();
        o_doseBRDU.clear();
        o_popSize.clear();
        o_prcBRDU.clear();
        o_prcEDU.clear();
        o_prcEDUpBRDUp.clear();
        o_prcEDUpBRDUn.clear();
        o_prcEDUnBRDUp.clear();
        o_prcEDUnBRDUn.clear();
        o_avgGen.clear();
        o_nbG1.clear();
        o_nbS.clear();
        o_nbG2M.clear();
        o_nbG0.clear();
        o_nbNewcomers.clear();
        o_flowOut.clear();
        o_avgGenOut.clear();
        o_gen0.clear();
        o_gen1.clear();
        o_gen2.clear();
        o_gen3.clear();
        o_gen4.clear();
        o_gen5.clear();
        o_gen6.clear();
        o_gen7.clear();
        o_gen8.clear();
        o_gen9.clear();
        o_gen10.clear();
        o_apopt.clear();
    }
    void writeFiles(){
        o_doseEDU.writeToFile();
        o_doseBRDU.writeToFile();
        o_popSize.writeToFile();
        o_prcBRDU.writeToFile();
        o_prcEDU.writeToFile();
        o_prcEDUpBRDUp.writeToFile();
        o_prcEDUpBRDUn.writeToFile();
        o_prcEDUnBRDUp.writeToFile();
        o_prcEDUnBRDUn.writeToFile();
        o_avgGen.writeToFile();
        o_nbG1.writeToFile();
        o_nbS.writeToFile();
        o_nbG2M.writeToFile();
        o_nbG0.writeToFile();
        o_nbNewcomers.writeToFile();
        o_flowOut.writeToFile();
        o_avgGenOut.writeToFile();
        o_gen0.writeToFile();
        o_gen1.writeToFile();
        o_gen2.writeToFile();
        o_gen3.writeToFile();
        o_gen4.writeToFile();
        o_gen5.writeToFile();
        o_gen6.writeToFile();
        o_gen7.writeToFile();
        o_gen8.writeToFile();
        o_gen9.writeToFile();
        o_gen10.writeToFile();
        o_apopt.writeToFile();
    }
};

class masterObserver {
public:
    masterObserver(string folder = string("")) :
        o_doseEDU(folder + string("doseEDU.txt")),
        o_doseBRDU(folder + string("doseBRDU.txt")),

        o_popDN1(folder + string("nbDN1.txt")),
        o_popDN2(folder + string("nbDN2.txt")),
        o_popDN3(folder + string("nbDN3.txt")),
        o_popDN4(folder + string("nbDN4.txt")),
        o_popDNTot(folder + string("nbDNTot.txt")),
        o_popeDP(folder + string("nbeDP.txt")),
        o_poplDP(folder + string("nblDP.txt")),
        o_popSP4(folder + string("nbSP4.txt")),
        o_popSP8(folder + string("nbSP8.txt")),
        o_popTreg(folder + string("nbTreg.txt")),

        o_prcBRDUDN1(folder + string("%BRDU+ DN1.txt")),
        o_prcBRDUDN2(folder + string("%BRDU+ DN2.txt")),
        o_prcBRDUDN3(folder + string("%BRDU+ DN3.txt")),
        o_prcBRDUDN4(folder + string("%BRDU+ DN4.txt")),
        o_prcBRDUDNTot(folder + string("%BRDU+ DNTot.txt")),
        o_prcBRDUeDP(folder + string("%BRDU+ eDP.txt")),
        o_prcBRDUlDP(folder + string("%BRDU+ lDP.txt")),
        o_prcBRDUSP4(folder + string("%BRDU+ SP4.txt")),
        o_prcBRDUSP8(folder + string("%BRDU+ SP8.txt")),
        o_prcBRDUTreg(folder + string("%BRDU+ Treg.txt")),

        o_prcEDUDN1(folder + string("%EDU+ DN1.txt")),
        o_prcEDUDN2(folder + string("%EDU+ DN2.txt")),
        o_prcEDUDN3(folder + string("%EDU+ DN3.txt")),
        o_prcEDUDN4(folder + string("%EDU+ DN4.txt")),
        o_prcEDUDNTot(folder + string("%EDU+ DNTot.txt")),
        o_prcEDUeDP(folder + string("%EDU+ eDP.txt")),
        o_prcEDUlDP(folder + string("%EDU+ lDP.txt")),
        o_prcEDUSP4(folder + string("%EDU+ SP4.txt")),
        o_prcEDUSP8(folder + string("%EDU+ SP8.txt")),
        o_prcEDUTreg(folder + string("%EDU+ Treg.txt"))
   {}

    observer o_doseEDU;
    observer o_doseBRDU;

    observer o_popDN1;//
    observer o_popDN2;//
    observer o_popDN3;//
    observer o_popDN4;//
    observer o_popDNTot;//
    observer o_popeDP;//
    observer o_poplDP;//
    observer o_popSP4;//
    observer o_popSP8;//
    observer o_popTreg;//

    observer o_prcBRDUDN1;//
    observer o_prcBRDUDN2;//
    observer o_prcBRDUDN3;//
    observer o_prcBRDUDN4;//
    observer o_prcBRDUDNTot;//
    observer o_prcBRDUeDP;//
    observer o_prcBRDUlDP;//
    observer o_prcBRDUSP4;//
    observer o_prcBRDUSP8;//
    observer o_prcBRDUTreg;//

    observer o_prcEDUDN1;//
    observer o_prcEDUDN2;//
    observer o_prcEDUDN3;//
    observer o_prcEDUDN4;//
    observer o_prcEDUDNTot;//
    observer o_prcEDUeDP;//
    observer o_prcEDUlDP;//
    observer o_prcEDUSP4;//
    observer o_prcEDUSP8;//
    observer o_prcEDUTreg;//


    //observerTable o_multi_IDStrain_tmut_decreaseReco;

    void clear(){
        o_doseEDU.clear();
        o_doseBRDU.clear();

        o_popDN1.clear();
        o_popDN2.clear();
        o_popDN3.clear();
        o_popDN4.clear();
        o_popDNTot.clear();
        o_popeDP.clear();
        o_poplDP.clear();
        o_popSP4.clear();
        o_popSP8.clear();
        o_popTreg.clear();

        o_prcBRDUDN1.clear();
        o_prcBRDUDN2.clear();
        o_prcBRDUDN3.clear();
        o_prcBRDUDN4.clear();
        o_prcBRDUDNTot.clear();
        o_prcBRDUeDP.clear();
        o_prcBRDUlDP.clear();
        o_prcBRDUSP4.clear();
        o_prcBRDUSP8.clear();
        o_prcBRDUTreg.clear();

        o_prcEDUDN1.clear();
        o_prcEDUDN2.clear();
        o_prcEDUDN3.clear();
        o_prcEDUDN4.clear();
        o_prcEDUDNTot.clear();
        o_prcEDUeDP.clear();
        o_prcEDUlDP.clear();
        o_prcEDUSP4.clear();
        o_prcEDUSP8.clear();
        o_prcEDUTreg.clear();

        // funny plots
        //o_multi_IDStrain_tmut_decreaseReco.clear();
        //o_multi_IDStrain_tStart_tEnd_nbKilled_nbCleared_PopInit_PopEnd_tPeakInf_nbPeakInf.clear();
    }
    void writeFiles(){
        o_doseEDU.writeToFile();
        o_doseBRDU.writeToFile();

        o_popDN1.writeToFile();
        o_popDN2.writeToFile();
        o_popDN3.writeToFile();
        o_popDN4.writeToFile();
        o_popDNTot.writeToFile();
        o_popeDP.writeToFile();
        o_poplDP.writeToFile();
        o_popSP4.writeToFile();
        o_popSP8.writeToFile();
        o_popTreg.writeToFile();

        o_prcBRDUDN1.writeToFile();
        o_prcBRDUDN2.writeToFile();
        o_prcBRDUDN3.writeToFile();
        o_prcBRDUDN4.writeToFile();
        o_prcBRDUDNTot.writeToFile();
        o_prcBRDUeDP.writeToFile();
        o_prcBRDUlDP.writeToFile();
        o_prcBRDUSP4.writeToFile();
        o_prcBRDUSP8.writeToFile();
        o_prcBRDUTreg.writeToFile();

        o_prcEDUDN1.writeToFile();
        o_prcEDUDN2.writeToFile();
        o_prcEDUDN3.writeToFile();
        o_prcEDUDN4.writeToFile();
        o_prcEDUDNTot.writeToFile();
        o_prcEDUeDP.writeToFile();
        o_prcEDUlDP.writeToFile();
        o_prcEDUSP4.writeToFile();
        o_prcEDUSP8.writeToFile();
        o_prcEDUTreg.writeToFile();

       // funny plots
       //o_multi_IDStrain_tmut_decreaseReco.writeToFile();
       //o_multi_IDStrain_tStart_tEnd_nbKilled_nbCleared_PopInit_PopEnd_tPeakInf_nbPeakInf.writeToFile();
    }
};

struct miniStats {
    int nbDataPt;
    double currentMin;
    double currentMax;
    double currentSum;
    double currentSumStdSquares;
    double getMin(){if(nbDataPt == 0) return 0.0; else return currentMin;}
    double getMax(){if(nbDataPt == 0) return 0.0; else return currentMax;}
    double getAverage(){if(nbDataPt == 0) return 0.0; else return currentSum / (double) nbDataPt;}
    double getStdDev(){if(nbDataPt == 0) return 0.0; else return (currentSumStdSquares / (double) nbDataPt) - getAverage() * getAverage();}
    void addData(double newX){
        nbDataPt++;
        currentMin = min(currentMin, newX);
        currentMax = max(currentMax, newX);
        currentSum += newX;
        currentSumStdSquares += newX * newX;
    }
    miniStats(){
        nbDataPt=0;
        currentMin = 1e12;
        currentMax = -1e12;
        currentSum = 0.0;
        currentSumStdSquares = 0.0;
    }
    int getN(){return nbDataPt;}
};

#endif // STATISTIQUES_H
