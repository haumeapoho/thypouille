#ifndef WHOLETHYMUS_H
#define WHOLETHYMUS_H

// This is now the piece of code to simulate multiple populations, even a full thymus
#include "cytonSimu.h"

enum stages{DN1, DN2, DN3, DN4, eDP, lDP, SP4, SP8, Tregs, Periph, NbStages};
//// I made sure the enum POPS and Stages are the same, because the simulations with POP enums call initializeStageFromThymus(..., int)
////enum POPS{expDN1, expDN2, expDN3, expDN4, expeDP, explDP, expSP4, expSP8, expTreg, expPeriph, NB_POP};
string nameStage(int stage);

void initializeStageFromThymus(stage* currentStage, int IDthymicPopulation);



struct multiPopSimulation : public onePopulation{

    multiPopSimulation();
    ~multiPopSimulation() {reset();}
    void reset();


    // to comply with the profile of onepopulation, should be able to run by only initializing
    // with an observer for one population
    void initialize(observerOneSim *OOS){
        // if an observer all pops already exists, reuses it, else creates a new one.
        if(currentObserverAllPops){
            initialize(OOS, currentObserverAllPops);
        } else {
            masterObserver* MO = new masterObserver("");
            initialize(OOS, MO);
        }
    }

    // But in the general case, we want to observe all populations + still one population of interest
    void initialize(observerOneSim *OOS, masterObserver* MO);


    bool timeStep();
    void finalize();

    // inherited:
    //    double dt;
    //    double preSimTime;
    //    double timeBRDU;
    //    double durationBRDU;
    //    double timeEDU;
    //    double durationEDU;
    //    double timeSimulation;
    //    vector<double> AnalysisPoints;
    //    double time;
    //    bool synchronizeInflow;

    // This will observe only one population, the one of interest if doing fitting
    // observerOneSim* currentObserver;

    // inherited useful parameters (specific to onePopulation)
    // stage* pop;
    // int initialNumbers;
    // double inflowRate;
    // double scalingRatio;

    // to be removed
    double apoptoticTime;

    // New parameters for more than 1 population
    // These are now vectors
    vector<double> initialNumbersPerPop; // size NbStages;
    vector<double> additionalInflow; // size NbStages
    vector<bool> simulatedStages;
    vector<double> rescalingFactors;

    double ratiolDPtoSP4;
    double ratiolDPtoSP8;
    double ratioOfSP4InflowToTregs;

    vector<stage*> WholePop;
    stage* getStage(int IDstage);
    int getIDNextStage(int IDstage);// will probabilistically decide SP4, SP8 or Tregs

    masterObserver* currentObserverAllPops;

    string printParameters();
    string printPops();
};















////vector<double> times;
////vector<double> tabDNA;
////vector<double> tabBRDU;
////vector<double> tabEDU;
////vector<double> tabVOL;
//// this define might be necessary for the GUI
//// #define folder string("C:/Users/Philippe/Desktop/Work/2015-GC/2016-06-14 CytonLeishmania/")
//// #define folder string("")














#endif // WHOLETHYMUS_H
