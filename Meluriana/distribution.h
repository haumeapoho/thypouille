
// Careful: modified on 25-nov-2018 because different files/classes using stdrandoms caused seg fault. I think it is not good to use the create function separately when there are static fields.
// for now on, use the random static class. It's slow because redefines a new class each time. Can be improved
#ifndef i_distribution
#define i_distribution
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include "random.h"
#include <sstream>
using namespace std;


enum{Fixed, Normal, LogNormal, FromData, BiModal, Exponential, NBDIstribs};

double LogNormParamAverageToMu(double E, double S);
double LogNormParamStdDevToSigma1(double E, double S);
std::pair<double, double> LogNormParameters(double wantedE, double wantedS);
void testLogNorm();
/** @Brief Class to generate random numbers from an arbitrary distribution,
 *  defined by a liste of values (classes) and their densities
 *  THE VALUES OF THE CLASSES SHOULD BE ORDERED INCREASING */
struct probaLawFromTable{
    int size;
    vector<double> Xs;
    vector<double> NormalizedDensities;     // to probabilities
    vector<double> cumulatedValuesHigh;      // cumulated probabilities
    double normalizingCoeff;                // just for remembering the normalisation

    bool uniformizeInsideClasses;
    vector<double> lowBoundsXs;
    vector<double> highBoundsXs;

    static double IndependentRandRealGen(); // If later someone wants to separate generators for seeding.
    virtual double getRandValue();
    int getRandClass();
    probaLawFromTable(vector<double> &_Xs, vector<double> &densities, bool _uniformizeInsideClasses);
    probaLawFromTable(string filename, bool _uniformizeInsideClasses);
    void init(vector<double> &_Xs, vector<double> &densities, bool _uniformizeInsideClasses);

    double getAverage(){cout << "Function 'probaLawFromTable::getAverage() not programmed yet\n"; return -1;}
    virtual string print();
    virtual ~probaLawFromTable(){}
};




/** @Brief Class t  o test/control the behavior of probaLawFromTable.
 *  by being daughter cell, reimplements the getRandomValue function and
 *  does statistics on how the mother class behaves */
struct probaLawStoringResults : public probaLawFromTable
{
    vector<int> nbOfTimesPerClass;
    int totalNbEvents;
    probaLawStoringResults(vector<double> & _Xs, vector<double> & densities, bool _uniformizeInsideClasses);
    double getRandValue();
    double getFrequency(int classIndex);
    int fitDoubleInAClass(double val);
    void clearRecord();
    string print();
    string TestprobaLawFromTable();
    virtual ~probaLawStoringResults(){}
};

// automatic
struct histogramFromDistrib{
    vector<double> lowBoundsXs;
    vector<double> highBoundsXs;
    vector<double> averageXs;
    vector<double> densities;
    double vmin;
    double vmax;
    double maxDens;
    string print(){
        stringstream ss;
        size_t S = lowBoundsXs.size();
        if(highBoundsXs.size() != S) cerr << "Wrong sizes in histogramFromDistrib " << endl;
        if(averageXs.size() != S) cerr << "Wrong sizes in histogramFromDistrib " << endl;
        if(densities.size() != S) cerr << "Wrong sizes in histogramFromDistrib " << endl;
        for(size_t i = 0; i < S; ++i){
            ss << averageXs[i] << "\t" << densities[i] << endl;
        }
        return ss.str();
    }
    histogramFromDistrib(vector<double> listValues, size_t nbBins){
        size_t S = listValues.size();
        if(S == 0) {cerr << "Histogram: empty data < 1\n"; return;}
        if(nbBins < 1) {cerr << "Histogram: nbBins < 1\n"; return;}
        lowBoundsXs.resize(nbBins, 0.);
        highBoundsXs.resize(nbBins, 0.);
        averageXs.resize(nbBins, 0.);
        densities.resize(nbBins, 0.);

        maxDens = 0;
        vmin = listValues[0];
        vmax = listValues[0];
        for(size_t i = 0; i < S; ++i){
            vmin = min(vmin, listValues[i]);
            vmax = max(vmax, listValues[i]);
        }
        double interval = (vmax - vmin) / (static_cast<double>(nbBins) - 1);
        if(fabs(interval) < 1e-9) {
            lowBoundsXs.clear();
            lowBoundsXs.resize(1, vmin);
            highBoundsXs.clear();
            highBoundsXs.resize(1, vmax);
            averageXs.clear();
            averageXs.resize(1, 0.5*vmin + 0.5 * vmax );
            densities.clear();
            densities.resize(1, 1.0);
            cerr << "WRN : Histogram: too few stddev\n"; return;}
        for(size_t i = 0; i < nbBins; ++i){
            lowBoundsXs[i] = vmin + interval * (static_cast<double>(i));
            highBoundsXs[i] = vmin + interval * (static_cast<double>(i + 1));
            averageXs[i] = 0.5*lowBoundsXs[i] + 0.5*highBoundsXs[i];
        }
        for(size_t i = 0; i < S; ++i){
            double val = listValues[i];
            int bin = static_cast<int>((val - vmin) / interval); // here, it should not be casted to size_t
            if(bin < 0) bin = 0;
            if(bin >= nbBins) bin = nbBins-1;
            densities[bin] += 1.0 / (double) S;
        }
        for(size_t i = 0; i < densities.size(); ++i){
            maxDens = max(maxDens, densities[i]);
        }
    }
};

struct Law{
    Law(){
        type = Fixed;
        constantVal = 0.0;
        //currentGenerator = NULL;
        currentDistribFromFile = nullptr;
        recordedValues.clear();
        record = false;
        // The law should additionally remember how it was created, even though everything is already loaded into the currentGenerator.
        _mu1=0; _sigma1 = 0; _weight = 0; _mu2 = 0; _sigma2 = 0;
    }
    ~Law(){cerr << "Law gets deleted !" << endl;}

    vector<double> recordedValues;
    bool record;
    int type;
    double _mu1; double _sigma1; double _weight; double _mu2 ; double _sigma2;

    string print(){
        stringstream res;
        res << type << ", mu1=" << _mu1 << ", sig1=" << _sigma1 << ", w=" << _weight << ", mu2=" << _mu2  << ", sig2=" << _sigma2 << endl;
        return res.str();
    }

    // three cases (only one will be used)
    double constantVal;
    //stdRandoms* currentGenerator;
    probaLawFromTable* currentDistribFromFile;

    // two ways to initiate a distribution
    void set(string fileName){
        if(currentDistribFromFile) delete currentDistribFromFile;
        currentDistribFromFile = new probaLawFromTable(fileName, true);
        type = FromData;
        resetRecords();
    }

    void set(int _type, double mu1, double sigma1 = 0, double weight = 0, double mu2 = 0, double sigma2 = 0){
        //if(!currentGenerator) currentGenerator = new stdRandoms();
        _mu1 = mu1;_sigma1 = sigma1; _weight = weight; _mu2 = mu2; _sigma2 = sigma2;
        type = _type;
        switch(type){
        case Fixed:{constantVal = mu1; break;}
        case FromData:{cerr << "ERR: Law::, for file-generated distributions, call Law(fileName) instead of Law(int type, ...)\n"; break;}
        default: break;
        /*case Normal:{currentGenerator->CreateNormal(mu1, sigma1); break;}
        case LogNormal:{currentGenerator->CreateLog_normal(mu1, sigma1);break;}
        case BiModal:{currentGenerator->CreateBiModal(mu1, sigma1, weight, mu2, sigma2);break;}
        case Exponential:{currentGenerator->CreateExponential(mu1); break;}*/
        }
        resetRecords();
    }

    void setRecord(bool _record){
        record = _record;
        resetRecords();
    }
    void resetRecords(){
        recordedValues.clear();
    }

    // Philippe 2019-03-25, will need to check these are right
    double getAverage(){
        switch(type){
        case Fixed:{return constantVal; break;}
        case Normal:{return _mu1; break;}
        case LogNormal:{return exp(_mu1 + _sigma1*0.5); break;}
        case FromData:{return (currentDistribFromFile) ? currentDistribFromFile->getAverage() : -1; break;}
        case BiModal:{return _weight * _mu1 + (1.-_weight) * _mu2; break;}
        case Exponential:{return 1.0 / (max(1e-12,fabs(_mu1))); break;}
        default:{return NAN;}
        }
    }

    // then, calling the distrib
    double getRandValue(bool shouldBePositive = false){
        //cout << "Rand " << type << endl;
        double res = -1;
        int cpt = 0;
        while((cpt == 0) || ((shouldBePositive && (res < 0)) && (cpt < 100))){
            switch(type){
            case Fixed:{res = constantVal; break;}
            case Normal:{res = random::normal(_mu1, _sigma1); break;}
            case LogNormal:{res = random::logNormal(_mu1, _sigma1); break;}
            case FromData:{res = (currentDistribFromFile) ? currentDistribFromFile->getRandValue() : -1; break;}
            case BiModal:{res = random::biModal(_mu1, _sigma1, _weight, _mu2, _sigma2); break;}
            case Exponential:{res = random::exponential(_mu1); break;}
            default:{res = -1;}
            }
            cpt++;
        }
        if(cpt > 99) cerr << "ERR: the distribution could not give a positive number as requested " << endl;

        if(record) recordedValues.push_back(res);
        //cout << "Push " << res << endl;
        return res;
    }

    //std::pair<vector<double>, vector<double>> getHistogram();
    histogramFromDistrib getHistogram(){
       return histogramFromDistrib(recordedValues, recordedValues.size() / 50);
    }
};

#endif
