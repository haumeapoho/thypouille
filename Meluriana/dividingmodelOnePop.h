#ifndef DIVIDINGMODEL_ONEPOP_H
#define DIVIDINGMODEL_ONEPOP_H

#include "../Moonfit/moonfit.h"
#include "cytonSimu.h"
// #include "wholethymus.h"

//#define SIMULATE_ALL_THYMUS

enum Backgrounds {WT, DKO};

/* This model has  unknown parameters, +  */
/// @brife A dividing model is the wrapper around an agent-based simulation (see class cytonSimu class).
/// It contains the parameters and information to run the simulations, and will retrieve the observed variables fron
/// the agent-based simulation at the requested time-steps.
/// It implements all functions necessary to set-up a simulation, then the mother class modelAgentBased is pvoviding
/// a 'simulate()' function that call all these functions, and stops at each time-point where analysis is requested
struct dividingModel : public modelAgentBased {

    /// @brief The agent-based model class that will be used for simulation
#ifdef SIMULATE_ALL_THYMUS
    multiPopSimulation* currentSim;
#else
    onePopulation* currentSim;
#endif

    /// @brief defines names of parameters/variables and parameter boundaries
    dividingModel();
    /// @brief fills parameters values with a default (bad) set that simulations can run (for tests)
    void setBaseParameters();
    /// @brief Start a new instance of the agent-based simulation class (currentSim), gives to it the parameter values,
    /// and creates the initial population of cells.
    virtual void initialise(long long _background = 0); // will depend on the submodels

    void timeStep( const double tstart, const double tend);
    void analyzeState(const double t);
    void finalize();

    observerOneSim* currentObs;

    // parameter
    enum{aimedPopSize,
        avgG1,
        avgS,
        avgG2M,
        widthG1,
        widthS,
        widthG2M,
        avgG0,
        widthOrPercentLongG0,
        percentQuiescentG0,
        paramDiff,
        inflowRate,
        thresholdPos,
        preSimTime,
        timeBRDU,
        durationBRDU,
        timeEDU,
        durationEDU,
        timeSimulation,
        apoptoticTime,
        typePopulation,
        deathRate,
        modulatedG1KO,
        modulatedSKO,
        modulatedG2MKO,
        modulatedWidthG1KO,
        modulatedWidthSKO,
        modulatedQuiescentKO,
        NbParameters};

    // Variables we have over time
    enum{
        PreInG1,
        MiddleInG1,
        PostInG1,
        prcEDUpBRDUp,
        prcEDUpBRDUn,
        prcEDUnBRDUp,
        prcEDUnBRDUn,
        TotInG1,
        TotInS,
        TotInG2,
        AvgDNAPre,
        AvgDNAMiddle,
        AvgDNAPost,
        nCells,
        doseEDU,
        doseBRDU,
        popSize,
        prcBRDU,
        prcEDU,
        avgGen,
        nbG1,
        nbS,
        nbG2M,
        nbG0,
        nbNewcomers,
        flowOut,
        avgGenOut,
        gen0,
        gen1,
        gen2,
        gen3,
        gen4,
        gen5,
        gen6,
        gen7,
        gen8,
        gen9,
        gen10,
        apopt,
        PreInS,
        MiddleInS,
        PostInS,
        PreInG2M,
        MiddleInG2M,
        PostInG2M,
        AvgDNABRDU,
        UnstInS,
        nbEarly,
        nbMiddle,
        nbPost,
        NbVariables};



/*    cytonSimulation();
    void reset();
    void initialize(masterObserver* MO);
    bool timeStep();
    void finalize();

    vector<double> AnalysisPoints;
    vector<double> initialNumbers; // size NbStages;
    vector<double> additionalInflow; // size NbStages
    vector<bool> simulatedStages;
    vector<double> rescalingFactors;

    double ratiolDPtoSP4;
    double ratiolDPtoSP8;
    double ratioOfSP4InflowToTregs;
    bool synchronizeInflow;

    typeDifferentiation         = _typeDifferentiation;
    paramDiff                   = _paramDiff;
    waitEndMtoDifferentiate     = _waitEndMtoDifferentiate;
    distribDeath                = _distribDeath;
    distribG0                   = _distribG0;
    distribG1                   = _distribG1;
    distribS                    = _distribS;
    distribG2M                  = _distribG2M;
    distribTot                  = _distribTot;
    doNotRescaleS               = _doNotRescaleS;
    apoptoticTime               = _apoptoticTime;*/



    long long background; // for different models
    //virtual void derivatives(const vector<double> &x, vector<double> &dxdt, const double t);

    void action(string name, double parameter){
        if(!name.compare("FoldIncrease")){
            if((parameter > 1000.0) || (parameter < 0)) {cerr << "ERR: ModeleMinLatent::action(" << name << ", " << parameter << "), wrong parameter value\n"; return;}
            //val[NPop] =    parameter * val[NPop]; // example of how the hell it works
            return;
        }
    }
};



#endif // DIVIDINGMODEL_H
