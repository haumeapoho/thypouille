#ifndef VIEWDISTRIB_H
#define VIEWDISTRIB_H

#include <QDialog>
#include "distribwidget.h"
#include "grapheCustom.h"

namespace Ui {
class viewDistrib;
}

class viewDistrib : public QDialog
{
    Q_OBJECT

public:
    explicit viewDistrib(DistribWidget* _toUse, QWidget *parent = 0);
    ~viewDistrib();
    DistribWidget* toUse;
    grapheCustom* currentGraphe;


public slots:
    void replot();
    void closing();

private:
    Ui::viewDistrib *ui;
};

#endif // VIEWDISTRIB_H
