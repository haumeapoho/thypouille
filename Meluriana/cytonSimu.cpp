#include "cytonSimu.h"
#define verbose 0
#include <fstream>
#include <string>
#include <iomanip>
//#include "LambertW/LambertW.h"
#include "distribution.h"

#ifdef doubleStagePop
// Philippe Remove later, temporary trick
double passParameters::percentDeath = 0;
int passParameters::generationTremplinIncluded = 1;
double passParameters::newG1 = 6;
double passParameters::newS = 6.5;
double passParameters::newG2M = 20. / 60.;
#endif


// global variables that store 'watch' the simulation
static counter cnt;
static mem mm;
// a counter of how many thymocytes are in memory, for memory leakage test
static int cptthymo = 0;

//This variable allows to simulate a different curve for the DNA uptake. Was only for testing, keep it at 0
double thymocyte::testDelay = 0.0;
// Type of DNA incorporation curve. 0 means by default, linear increase. other values will use testDelay as parameter
#define typeDNA 0

thymocyte::thymocyte( double _tbirth, double _tstartG1,
                      double durG1, double durS, double durG2M, double _tdie,
                      double _durdisapp, int _gen,
                      int status, int cyclestatus,
                      cell* parent) :

    cell(parent),
    tbirth(_tbirth),
    tstartG1    (_tstartG1),
    tendG1      (_tstartG1 + durG1),
    tendS       (_tstartG1 + durG1 + durS),
    tendG2M     (_tstartG1 + durG1 + durS + durG2M),
    tdie        (_tdie),
    tdisapp     (_tdie + _durdisapp),
    gen(_gen),
    status(status),
    cyclestatus(cyclestatus){

    DNA = 0;
    VOL = 0;
    EDU = 0;
    BRDU = 0;
    // default values that need to be changed by the user
    markedForExit = unmarked;
    incomer = false;
    timeInThisPopulation = 0;
    texit = 1e12;

    if(verbose) {cout << "Cell of ID " << ID() << "   parent " << ID_parent << " TB=" << tbirth << " TDv=" << tstartG1 << " end of div=" << tendG2M << " TDd=" << tdie << " gen=" << gen << endl;}
}


int thymocyte::event(double time, double dt){ // note: only can return one event for a time. Die wins
    if(status == DIVREMOVED) return NOTHING;
    // Note: two event (tdie and tendS, or two phases, might be close together. In that case, the second event takes over
    if(status == DEAD){
        if(fabs(tdisapp - time) < dt) return DISAPPEAR;
    } else {
        if(fabs(tdie - time) < dt) {
            return DIE;
        } else {
            if((status == NORMAL) && (time - tstartG1 > dt) ) return DIVSTARTG1; // in case G0 duration is 0
            if(status == DIVIDING){
                // important: opposite order.
                if(fabs(tendG2M - time) < dt) return DOMITOSIS;
                if(fabs(tendS - time) < dt) return STARTG2M;
                if(fabs(tendG1 - time) < dt) return STARTS;
            }
            return NOTHING;
        }
    }
    return NOTHING;
}



double curveDNA(double currentT, double tendG1, double tendS){
        if(currentT < tendG1) return 1.0;
        if(currentT > tendS) return 2.0;

        double DNA = 0;

        if(typeDNA == 0){
            DNA = 1 + (currentT - tendG1) / (max(0.001, tendS - tendG1));
        }

        if(typeDNA == 1){
            // 1st option: add a lag in the cell cycle
            double lag = thymocyte::testDelay;
            if(currentT - tendG1 < lag){
                DNA = 1;
            } else {
                DNA = 1 + (currentT - lag - tendG1) / (max(0.001, tendS - tendG1 - lag));
            }
        }

        if(typeDNA == 2){
            // double linear
            double TsecondPart = tendG1 + min(thymocyte::testDelay, 0.5* (tendS - tendG1));
            if(currentT < TsecondPart){
                DNA = 1 + 0.1*(currentT - tendG1)/(max(0.001, TsecondPart - tendG1));
            } else {
                DNA = 1.1 + 0.9*(currentT - TsecondPart) / (max(0.001, tendS - TsecondPart));
            }
        }


//        if(typeDNA == 3){
//            // D is the parameter, alpha will follow.
//            double D = tendG1 + min(thymocyte::testDelay, 0.5* (tendS - tendG1));

//            double beta = D - tendG1;
//            double gamma = tendS - D; // normally gamma is never 0...
//            double inW = 2 * beta * exp(beta / gamma) / gamma;
//            double w = utl::LambertW<0>(inW);
//            double alpha = (gamma * w - beta) / (beta * gamma);
//            double derivative= (2 - exp( alpha * (D - tendG1))) / (tendS - D);
//            if(currentT < D){
//                DNA = exp(alpha * (currentT - tendG1));
//            } else {
//                DNA = exp(alpha*(D - tendG1)) + (currentT - D)*derivative;
//            }
//        }

        if(typeDNA == 3){
        // sigmoid, centered
//            double TsecondPart = tendG1 + min(thymocyte::testDelay, 0.5* (tendS - tendG1));
//            double fraction
            double lambda = thymocyte::testDelay;
            double valAt0 = 1. /(1 + exp(- lambda * (-0.5 + 0)));
            double valAt1 = 1. /(1 + exp(- lambda * (-0.5 + 1.0)));
            double a = 1 / (valAt1 - valAt0);
            double b = 1 - a * valAt0;
            DNA = b + a * 1. / (1 + exp(- lambda * (-0.5 + (currentT - tendG1) / (max(0.001, tendS - tendG1)))));
        }

        if(typeDNA == 4){
            // sigmoid, with delay
            double lambda = 5; //5 * (1 / (1 - thymocyte::testDelay / (tendS - tendG1)));
            double delay = min(0.5, thymocyte::testDelay / (tendS - tendG1));
            //cout << "Delay " << delay << endl;
            double valAt0 = 1. /(1 + exp(- lambda * (-0.5 - delay + 0)));
            double valAt1 = 1. /(1 + exp(- lambda * (-0.5 - delay + 1.0 / (1 - delay))));
            double a = 1 / (valAt1 - valAt0);
            double b = 1 - a * valAt0;
            DNA = b + a * 1. / (1 + exp(- lambda * (-0.5 - delay + (currentT - tendG1 ) / ((1 - delay) * max(0.001, tendS - tendG1 )))));
        }

        return DNA;
}

void showCurveDNA(){
    double tendG1 = 1;
    double tendS = 4;
    thymocyte::testDelay = 1;
     if(typeDNA == 3){
         thymocyte::testDelay = 5;
     }
    cout << "Example of DNA curve for: tendG1 = " << tendG1 << " hr, tendS = " << tendS << " hours, typeDNA " << typeDNA << " and delay " << thymocyte::testDelay << endl;
    for(double t = 0; t < 5; t = t + 0.1){
        cout << t << "\t" << curveDNA(t, tendG1, tendS) << endl;
    }
}



// this calculates the next amount of EDU or BRDU according to the current concentration
void thymocyte::updateVariables(double currentT, double currentDoseEDU, double currentDoseBRDU, double dt){//h = dt
    timeInThisPopulation += dt;
    if(status == NORMAL){
        DNA = 1;
        VOL = 1;
    }
    if(status == DIVIDING){
        if(cyclestatus == S){
            DNA = curveDNA(currentT, tendG1, tendS);
            BRDU = BRDU + currentDoseBRDU * dt / (tendS - tendG1);
            EDU = EDU + currentDoseEDU * dt / (tendS - tendG1);
        } else {
            if(cyclestatus == G2M){
                DNA = 2;
            } else {
                DNA = 1;
            }
        }
        VOL = 1.0 + (currentT - tendG2M) / (max(0.001, tendG2M - tstartG1));
    }
}

cell* thymocyte::me(){return (cell*) this;}
double thymocyte::currentDNA(){return DNA;}  //if(DNA.size() > 0)  DNA[DNA.size() - 1]; else return 1.0;}
double thymocyte::currentBRDU(){ return BRDU;} //if(BRDU.size() > 0) BRDU[BRDU.size() - 1]; else return 0;}
double thymocyte::currentEDU(){ return EDU;} //if(EDU.size() > 0) EDU[EDU.size() - 1]; else return 0;}
double thymocyte::currentVOL(){ return VOL;} //if(VOL.size() > 0) VOL[VOL.size() - 1]; else return 1.0;}

string thymocyte::print(){
    stringstream res;
    res << nameStatus(status) << "\t" << nameCycle(cyclestatus) << "\tgen=\t" << gen << "\tbirth=\t" << tbirth << "\tPhases: G0\t" << tstartG1 << "\tG1\t" << tendG1 << "\tS\t" << tendS << "\tG2M\t" << tendG2M << "\tDiv - will die:\t" << tdie << "\tand disapp\t" << tdisapp << "\there till " << timeInThisPopulation << " gen=" << gen << " vol=" << currentVOL() << " DNA=" << currentDNA() << " BRDU=" << currentBRDU() << " EDU=" << currentEDU();
    return res.str();
}

thymocyte::~thymocyte(){
    //cerr << "For info: Thymocyte deleted" << endl;
}







stage::stage(string _name) {
    name = _name;
    insiders.clear();
    insiders.reserve(10000);     // to avoid reallocations in memory when size increases
}
stage::~stage(){
    delete distribDeath;
    delete distribG0;
    delete distribG1;
    delete distribS;
    delete distribG2M;
    delete distribTot;        // distribution of total cell cycle
    cerr << "ERRRRR: stage gets deleted !" << endl;
}

// each stage has its own parameters. Use this function to set them.
void stage::setParams(int _typeDifferentiation, double _paramDiff, Law* _distribDeath, Law* _distribG0, Law* _distribG1, Law* _distribS, Law* _distribG2M, Law* _distribTot, bool _waitEndMtoDifferentiate, bool _doNotRescaleS, double _apoptoticTime, double _percentBystanderG0){
    typeDifferentiation         = _typeDifferentiation;
    paramDiff                   = _paramDiff;
    waitEndMtoDifferentiate     = _waitEndMtoDifferentiate;
    distribDeath                = _distribDeath;
    distribG0                   = _distribG0;
    distribG1                   = _distribG1;
    distribS                    = _distribS;
    distribG2M                  = _distribG2M;
    distribTot                  = _distribTot;
    doNotRescaleS               = _doNotRescaleS;
    apoptoticTime               = _apoptoticTime;
    percentBystanderG0          = _percentBystanderG0;
}

// does this contain apoptotic cells ? => yes.
size_t stage::nbInsiders(){return insiders.size();}

void stage::addCell(thymocyte* t, bool resetStates){
    insiders.push_back(t);
    if(resetStates){
        t->gen = 0;
        t->timeInThisPopulation = 0;
        t->markedForExit = 0;
        t->incomer = false;
    }
}

void stage::removeCell(int IDpos){
    if((IDpos < 0) || (IDpos >= (int) insiders.size())) {cerr << "removeCell(ID=" << IDpos << ", out of bounds, size " << insiders.size() << endl; return;}
    insiders[IDpos] = nullptr;
    insiders[IDpos] = insiders.back();
    insiders.pop_back();
}

void stage::clear(){
    size_t NS = insiders.size();
    //cout << "clear " << insiders.size() << " cells " << endl;
    for(size_t i = 0; i < NS; ++i){
        if(insiders[i]) delete insiders[i];
        cptthymo--;
        insiders[i] = nullptr;
    }
    insiders.clear();
}



thymocyte* stage::generateQuiescent(double currentT){
    // says G0 will last 1e8 (whatever unit, should be long enough).
    thymocyte* newT = new thymocyte(currentT, 1e8, 0, 0, 0, 2e8, 0, 0,NORMAL, G0, nullptr); // no parent defined yet
    cptthymo++;
    // Now initialize VOL and DNA. Say dose EDU and BRDU to 0 for initialization.
    newT->updateVariables(currentT, 0, 0, 0);
    if(fabs(newT->DNA) < 1e-9) cerr << "ERR: Quiescent cell with inappropriate DNA level, see stage::generateQuiescen" << endl;
    // This is the time inside this population. Normally it is added
    newT->timeInThisPopulation = 0;
    return newT;
}


// This function creates a new cell according to the population parameters and the current time.
// it doesn't add it to the population yet. use addCell(generateCell(...)) for that.
// By default, cells will be generated at G0, except if saying synchroStartG0 = false, then random phase.
thymocyte* stage::generateCell(double currentT, bool synchroStartG0, int generation, thymocyte* parent){

    // 1- generating all times of events to happen
    double tbirth = currentT;
    double tdie = currentT + distribDeath->getRandValue(true);

    double tStartG1 = tbirth + distribG0->getRandValue(true);
//     if(tStartG1 > tbirth) {
//        cout << "G" << tStartG1 << "-" << tbirth << "\t";
//        cout << distribG0->print() << endl;
//     }
    double durG1 = distribG1->getRandValue(true);
    double durS = distribS->getRandValue(true);
    double durG2M = distribG2M->getRandValue(true);
    if(parent != nullptr){
        durG1 = parent->tendG1 - parent->tbirth;
        durS = parent->tendS - parent->tendG1;
        durG2M = parent->tendG2M - parent->tendS;
    }

    ///// DANGEROUS PIECE OF CODE; JUST FOR TESTING EFFECT OF ONE DIV ONLY
    //if(generation >= 1){
    //    durG1 = 500;
    //}

    #ifdef doubleStagePop
    /// Philippe remove!!
    if(generation >= passParameters::generationTremplinIncluded) {
        durG1 = passParameters::newG1;
        durS = passParameters::newS;
        durG2M = passParameters::newG2M;
        if((generation == passParameters::generationTremplinIncluded) && (random::uniformDouble(0,1) < passParameters::percentDeath)){
            tdie = tbirth + 0.2;
        }
    }
    #endif


    // ======== <option> Standard deviation of the full cell cycle ======
    // Now, there is the possibility to give a width to the full cycle (not each phase separately)
    // It is therefore recommended to use width either for the phase, or to put 0 for each phase and play with cycle width
    // The distribution distribTot is a lognormal with average the total cycle (already defined in the other laws),
    // and with a standard deviation. Since LogNormal distributions can not be rescaled easily, simulate a
    // new total cycle duration, compares it to the average it should be, and rescales all phases higher or lower, accordingly.
    double avgCycle = distribTot->getAverage();
    double totalCycleWidthCoefficient = 1;
    if(fabs(avgCycle) < 1e-6) cerr << "ERR: the total cell cycle duration distribution can not be so small!! " << avgCycle << endl;
    totalCycleWidthCoefficient = distribTot->getRandValue(true) / avgCycle;
    //cout << "Coeff " << totalCycleWidthCoefficient << endl;
    durG1 *= totalCycleWidthCoefficient;
    durS *= totalCycleWidthCoefficient;
    durG2M *= totalCycleWidthCoefficient;


    double durdisapp = apoptoticTime;
    int cyclestatus = G0;
    int status = NORMAL;
    //int generation = 0;
    //cout << "New cell: " << tStartG1 << ",\t" << durG1  << ",\t" <<  durS  << ",\t" <<  durG2M  << ",\t" <<  tdie  << ",\t" <<  durdisapp << endl;

    // Normally, a new cell would start at G0. In case one wants to add cells at a random stage or cell cycle phase:
    double rescale = 0;
    if(!synchroStartG0){
        double lifeSpan = min(tStartG1 + durG1 + durS + durG2M, tdie) - currentT; // note: only creates alive cells, not apoptotic
        rescale = random::uniformDouble(0,1) * lifeSpan;
        if(rescale < tStartG1 - currentT + durG1 + durS + durG2M) cyclestatus = G2M;
        if(rescale < tStartG1 - currentT + durG1 + durS) cyclestatus = S;
        if(rescale < tStartG1 - currentT + durG1) cyclestatus = G1;
        if(rescale < tStartG1 - currentT) cyclestatus = G0;
        if(cyclestatus != G0) status = DIVIDING;
        // note: the case (rescale > tdie) should not happen
    }

    // Create a new cell with all these timings
    thymocyte* newT = new thymocyte(tbirth-rescale, tStartG1-rescale, durG1, durS, durG2M, tdie-rescale, durdisapp,
                                    generation,status, cyclestatus, nullptr); // no parent defined yet
    cptthymo++;

    // Now initialize VOL and DNA. Say dose EDU and BRDU to 0 for initialization.
    newT->updateVariables(currentT, 0, 0, 0);

    // This is the time inside this population. Normally it is added
    newT->timeInThisPopulation = rescale;

    // Note: the parent and time in this population are given as default values, and should
    // be updated by the population, for instance when making a division.

    return newT;
}
// Not sure G0 should be included
double stage::getInterCycleTime(){
    //cout << "Total cycle: S " << distribS->getAverage() << "G1 " << distribG1->getAverage() << " G2M " << distribG2M->getAverage() << " G0 " << distribG0->getAverage() << endl;
    return distribS->getAverage() + distribG1->getAverage() + distribG2M->getAverage() + distribG0->getAverage();
}


vector<double> stage::getSteadyStateGenerations(double Ntot){
    // note: this only applies to typeDifferentiation == finiteNrDiv
    size_t    N = static_cast<size_t>(paramDiff ); // in case it's integer, discontinuity
    double T = getInterCycleTime();
    double delta = 1 / (max(1e-10, distribDeath->getAverage()));                // note: an exponential distribution is defined by a rate, but still gives a time ...
    double X = 2*(1 - T * delta);
    vector<double> res(N+1,0); // expected cumulated probability to be in each generation,
    double Tot = 0;
    for(size_t i = 0; i <= N; ++i){
        double toAdd = std::pow(X, (double) i);
        if(i == N) toAdd *= (paramDiff - (double) N);
        res[i] = toAdd;
        Tot += toAdd;
    }
    for(size_t i = 0; i <= N; ++i){
        res[i] = res[i] * Ntot / max(1e-10,Tot);
    }
    return res;
}

// Generates toCreate cells randomly accordingly to the
void stage::initializeRandom(double initN, double time){

    double nbBystander = percentBystanderG0 * static_cast<double>(initN);
    int nStayG0 = static_cast<int>(nbBystander);
    if(random::uniformDouble(0,1) < (nbBystander - (double)nStayG0)) nStayG0++;
    //cout << "Creating " << nStayG0 << " quiescent cells " << endl;
    for(int i = 0; i < nStayG0; ++i){
        thymocyte* t = generateQuiescent(time);
        addCell(t);
    }

    int numberToCreate = (int) initN - nStayG0;
    if(random::uniformDouble(0,1) < (initN - (double)(int(initN)))) numberToCreate++; // maybe I should look at numberToCreate floating values and not initN. This is a detail.
    //cout << "Creating " << numberToCreate << " active cells " << endl;

    if(typeDifferentiation == finiteNrDiv){
        // In case cells stay for a finite number of divisions (generations), the generation
        // distribution at equilibriul is not uniform (depends on proliferation and death rates)
        int    N = (int) (paramDiff ); // in case it's integer, discontinuity
        double T = getInterCycleTime();
        double delta = 1 / (max(1e-10, distribDeath->getAverage()));                // note: an exponential distribution is defined by a rate, but still gives a time ...
        double X = 2*(1 - T * delta);
        vector<double> cumulatedGenProba(N+3,0); // expected cumulated probability to be in each generation,
        double Sum = 0;
        //cout << "Ndiv = " << paramDiff << "  N = " << N << "  delta = " << delta << " T = " << T << " , X = " << X << endl;
        for(int i = 0; i <= N; ++i){
            double toAdd = std::pow(X, (double) i);
            if(i == N) toAdd *= (paramDiff - (double) N);
            Sum += toAdd;
            cumulatedGenProba[i] = Sum;
        }
        for(int i = 0; i <= N; ++i){
            cumulatedGenProba[i] /= max(Sum, 1e-10);
            //cout << "Sum " << i << " = " << cumulatedGenProba[i] << endl;
        }
        cumulatedGenProba[N+1] = 1.0;


        // Now creates the cells with this distribution
        vector<double> attributedGenerations;
        //cout << "finiteNrDiv -> init " << numberToCreate << endl;
        for(int i = 0; i < numberToCreate; ++i){

            double U = random::uniformDouble(0,1);
            int j = 0;
            while((j < N+1) && (U > cumulatedGenProba[j])){
                ++j;
            }
             //I think in theory that n+1 should not be reached
            thymocyte* t = generateCell(time, false, j);
            if(t->gen != j) cerr << "Choucroute garnie!" << endl;

            // be careful that all cells at last division should not been allowed to divide!!
            if(j == N) t->markedForExit = thymocyte::mark_stay_till_end_div;
            attributedGenerations.push_back((double) j);
            //cout << j << "\t";
            //double randomGenInside = random::uniformDouble(0,paramDiff);
            //double add = randomGenInside - (double) (int) randomGenInside;
            //t->gen = (int) randomGenInside + ((random::uniformDouble(0,1) < add) ? 1 : 0);
            addCell(t);
        }
        //histogramFromDistrib p(attributedGenerations, N+1);
        //cout << "Generations given : " << p.print() <<endl;
    }


    if(typeDifferentiation == laminarFiniteTime){
        // cout << "laminarFiniteTime -> init " << numberToCreate << endl;
        // Note: here, we do not assign a generation because cells behave similar at each generation
        // => do not analyze the generation, it is not started at equilibrium!
        for(int i = 0; i < numberToCreate; ++i){
            int generation = 0;
            thymocyte* t = generateCell(time, false, generation);
            double randomTimeInside = random::uniformDouble(0,paramDiff);
            t->timeInThisPopulation = randomTimeInside;
            addCell(t);
        }
    }

    if(typeDifferentiation == constantDiffRate){
        // cout << "constantDiffRate -> init " << numberToCreate << endl;
        // Note: here, we do not assign a generation because cells behave similar at each generation
        // => do not analyze the generation, it is not started at equilibrium!
        for(int i = 0; i < numberToCreate; ++i){
            int generation = 0;
            thymocyte* t = generateCell(time, false, generation);
            double randomTimeInside = random::uniformDouble(0, 1./max(1e-9, paramDiff));
            t->timeInThisPopulation = randomTimeInside;
            addCell(t);
        }
    }
}

void stage::addMultipleCells(vector<thymocyte*> cellsToAdd){
    size_t N = cellsToAdd.size();
    for(size_t i = 0; i < N; ++i){
        if(cellsToAdd[i]) addCell(cellsToAdd[i]);
    }
}

void stage::addInflow(double nCells, bool CellsComeInG0, double time){
    if((nCells < 0 ) || (nCells > 1e4)) {
        cerr << "ERR: stage::addInflow(nCells = " << nCells << "...), out of bounds" << endl;
        nCells = 1e4;
    }
    int intNbToAdd = static_cast<int>(nCells);
    if(random::uniformDouble(0,1) < (nCells - static_cast<double>(intNbToAdd)))
        {intNbToAdd++;}

    for(int i = 0; i < intNbToAdd; ++i){
        int generation = 0; // by default, start at generation 0 when it arrives!
        thymocyte* t = generateCell(time, CellsComeInG0, generation);
        addCell(t);
    }
}

string stage::print(){
    stringstream res;
    size_t NS = static_cast<size_t>(nbInsiders());
    res << NS << " cells" << endl;
    for(size_t i = 0; i < NS; ++i){
        res << insiders[i]->print() << endl;
    }
    return res.str();
}


// Reminder for states and events
//enum {NOTHING=0, DIVSTARTG1=1, STARTS=2, STARTG2M=3, DOMITOSIS=4, DIE=5, DISAPPEAR=6, NBEVENTS=7};
//status enum {NORMAL, DIVIDING, DEAD, REMOVED, DIVREMOVED, NBSTATUS};
//enum {G0, G1, S, G2M, NBCycleStates};


// Update the population and returns the list of cells (pointers) that left the population
vector<thymocyte*> stage::timeStep(double time, double dt, double currentEDUdose, double currentBRDUdose){

    vector<int> IDsToRemove; // list of cells to remove from the population: Will be removed at the end to not change the vector insiders in the middle of the loop
    vector<thymocyte*> listLeavingCells; // note: can only leave if alive

    // For each cell of the population:
    size_t sizeStage = static_cast<size_t>(nbInsiders());
    for(size_t iL = 0; iL < sizeStage; ++iL){
        thymocyte* currentInsider = insiders[iL];

        // 1- update the state according to the event, and perform division
        int event = currentInsider->event(time, dt);

        if(event != NOTHING) {
            if(event == DIE){ //die
                if((currentInsider->status != NORMAL) && (currentInsider->status != DIVIDING)) cerr << "ERR :" << time <<" die : cell  " << currentInsider->ID() << " already dead/divided :" << currentInsider->status << "\n" << currentInsider->print();
                currentInsider->setStatus(DEAD);
                if(verbose) cerr << "t=" << time << ",    cell " << currentInsider->ID() << " Dies... !\n";
            }
            if(event == DISAPPEAR){ //disappear
                if(currentInsider->status != DEAD) cerr << "ERR :" << time <<" disappear : cell " << currentInsider->ID() << " not yet dead:" << currentInsider->status << endl << currentInsider->print();
                currentInsider->setStatus(REMOVED);
                if(verbose) cerr << "t=" << time << ",    cell " << currentInsider->ID() << " Is Removed... !\n";
            }
            if(event == DOMITOSIS){ //div
                if((currentInsider->status != NORMAL) && (currentInsider->status != DIVIDING)) cerr << "ERR" << time <<" : div : cell  " << currentInsider->ID() << " already dead/divided :" << currentInsider->status << endl << currentInsider->print();

                // A code to add a punctual death if wanted: gen is the current before division, and starts at 0
                //if((currentInsider->gen == 1) && (random::uniformDouble(0,1) < rateDeathNewParameterToSet)){
                //    currentInsider->setStatus(DEAD);
                //} else {
                    currentInsider->setStatus(DIVREMOVED);
                    if(verbose) cerr << "t=" << time << ",    cell " << currentInsider->ID() << " Divides... !\n";

                    //create two daughters (the same here)
                    int newGen = currentInsider->gen + 1;


                    for(int nOffspr = 1; nOffspr <= 2; ++nOffspr){
                        thymocyte* l = generateCell(time, true, newGen);
                        ////////////////// If one wants to study inheritance, the following code will 
						/// take the mother durations and apply the same ones to the daughter:
						/// thymocyte* l = generateCell(time, true, newGen, currentInsider); 
                        if(l->gen != newGen) cerr << "Choucroute Gargantueste, dirait Hercule Poirot!" << endl;
                        if((l->cyclestatus != G0) && (l->tstartG1 - l->tbirth > 1e-6)) cerr << "ERR: synchronized generated cell is not at G0" << endl;
                        l->EDU = currentInsider->currentEDU() / 2.0;
                        l->BRDU = currentInsider->currentBRDU() / 2.0;

                        l->timeInThisPopulation = currentInsider->timeInThisPopulation;

                        // If finite number of divisions, decision to leave is made at division, each of the two daughters can stay or leave
                        // If waited for end of mitosis, time to leave as well
                        switch(currentInsider->markedForExit){
                            // 1 - transfer the exit state to daughter (if waited for end mitosis)
                            case thymocyte::mark_exit:{l->markedForExit = thymocyte::mark_exit; break;}
                            // 2 - if stayed for one last division, time to exit
                            case thymocyte::mark_stay_till_end_div:{l->markedForExit = thymocyte::mark_exit; break;}
                            // 3 - If reach max number of divisions, time to decide for the daughter whether it divides once more or not
                            case thymocyte::unmarked:{
                                 if((typeDifferentiation == finiteNrDiv) && (l->gen >= static_cast<int>(paramDiff))){   // normammy should only happen when it's ==
                                     if(!waitEndMtoDifferentiate){ // in this case we can only do uniform exit during the last generation so the paramDiff is X.5 for any X
                                         l->texit = random::uniformDouble(time, min(l->tendG2M, l->tdie));
                                     } else {
                                         double residual = paramDiff - static_cast<double>(static_cast<int>(paramDiff));
                                         if(random::uniformDouble(0,1) < residual){
                                             l->markedForExit = thymocyte::mark_stay_till_end_div;
                                         } else {
                                             l->markedForExit = thymocyte::mark_exit;
                                         }
                                     }
                                 }
                                 break;
                            }
                        }

                        addCell(l);
                    }
                //}
                // just for analysis
                //flowProlif[iSt]++;
            }
            if(event == DIVSTARTG1) {
                currentInsider->cyclestatus = G1;
                currentInsider->setStatus(DIVIDING);
            }
            if(event == STARTS) {
                currentInsider->cyclestatus = S;
            }
            if(event == STARTG2M) currentInsider->cyclestatus = G2M;

        } // end event if


        // this is a short code to allow exiting at a random phase of the cycle (especially for 2-populations simulation)
        // I could have created an event, but just took the minimum code added (in the mitosis
        if(!waitEndMtoDifferentiate && typeDifferentiation == finiteNrDiv && fabs(currentInsider->texit - time) < dt){ //
            currentInsider->markedForExit = thymocyte::mark_exit;
        }

        ///////////////// VERY DANGEROUS CODE; SHOULD NOT BE COMMITED
        // if(currentInsider->timeInThisPopulation > 72){
        //     currentInsider->markedForExit = thymocyte::mark_exit;
        // }

        // 2 - Updates the variables inside the cell:
        currentInsider->updateVariables(time, currentEDUdose, currentBRDUdose, dt); // careful, he might be dead
        //if(currentInsider->BRDU > thresholdBRDU) nrBRDUPerStage[iSt]++;
        //if(currentInsider->EDU > thresholdEDU) nrEDUPerStage[iSt]++;
    }

    // the loop has to be processed again, because currentInsider has lead to 2 daughter cells.     // it's also possible to keep inside the same loop, then the daughter cells are treated next time-point.
    sizeStage = insiders.size(); // need to be updated
    for(size_t iL = 0; iL < sizeStage; ++iL){
        thymocyte* currentInsider = insiders[iL];
        // 3 - Now remove dead cells after apoptotic time, better to do it after dividing, but not important
        // and returns the list of alive cells that left the population

        if((currentInsider->status == DIVREMOVED) || (currentInsider->status == REMOVED)){
            insiders[iL] = nullptr;
            delete currentInsider;
            cptthymo--;
            IDsToRemove.push_back(static_cast<int>(iL));
        } else if (currentInsider->status != DEAD){
            // Decision to exit. note: for finiteNrDiv, decision at division
            if(currentInsider->markedForExit == thymocyte::unmarked){
                if(((typeDifferentiation == laminarFiniteTime) && (currentInsider->timeInThisPopulation > paramDiff))
                     || ((typeDifferentiation == constantDiffRate) && (random::uniformDouble(0,1) < paramDiff * dt))){
                    if((waitEndMtoDifferentiate) && (currentInsider->status == DIVIDING)){
                        currentInsider->markedForExit = thymocyte::mark_stay_till_end_div;
                    } else {
                        currentInsider->markedForExit = thymocyte::mark_exit;
                    }
                }
            }
            if(currentInsider->markedForExit == thymocyte::mark_exit){
                IDsToRemove.push_back(static_cast<int>(iL));
                listLeavingCells.push_back(currentInsider);
            }
        }
    } // end loop on each insider
    std::sort(IDsToRemove.begin(), IDsToRemove.end(), std::greater<int>());
    for(size_t i = 0; i < IDsToRemove.size(); ++i){
        removeCell(IDsToRemove[i]); // it removes the pointer from the insider list, but does not destroy the content
    }
    return listLeavingCells;
}

// tool functions
string nameStatus(int i){
    vector<string> out = {"NORMAL", "DIVIDING", "DEAD", "REMOVED", "DIVREMOVED", "NBSTATUS"};
    if((i < 0) || (i >= NBSTATUS)) cerr << "Unknown stage " << i << endl;
    return out.at(static_cast<size_t>(i));
}

string nameCycle(int i){
    vector<string> out = {"G0", "G1", "S", "G2M", "NBCycleStates"};
    if((i < 0) || (i >= NBCycleStates)) cerr << "Unknown stage " << i << endl;
    return out.at(static_cast<size_t>(i));
}

void thymocyte::setStatus(int z){status = z;}







onePopulation::onePopulation() {

    //cout << "creating a onePopulation, memory has " << cptthymo << " thymocytes" << endl;

    // 1- initialize variables
    dt = 0.05;
    time = 0;
    currentBRDUdose = 0;
    currentEDUdose = 0;
    pop = new stage("OnePop");

    // 2- put basic parameters that can make a simulation run.
    Law* distribDeath = new Law();
    Law* distribG0 = new Law();
    Law* distribG1 = new Law();
    Law* distribS = new Law();
    Law* distribG2M = new Law();
    Law* distribTot = new Law();

    int typeDifferentiation = stage::finiteNrDiv;
    double paramDiff = 5.5;           // either limited time, nr divs, or rate diff

    double deathRate = 0.05; //0.1 / 24.;   // per day, right ??

    if(deathRate == 0) distribDeath->set(Fixed, deathRate);
    else distribDeath->set(Exponential, deathRate);

    distribG0->set(BiModal,0, 0.001, 0.8, 0.7, 0.2); // example: 80% cells do not have G0, 20% take around 0.7 days, std = 0.2

    double totalCycle = 16; // not G0
    double ratioG1 = 0.25, ratioS = 0.65, ratioG2M = 0.1;
    distribG1->set(Normal, ratioG1 * totalCycle, ratioG1 * totalCycle*0.2);
    distribS->set(Normal, ratioS * totalCycle, ratioS * totalCycle*0.2);
    distribG2M->set(Normal, ratioG2M * totalCycle, ratioG2M * totalCycle*0.2);
    distribTot->set(Fixed, 1.0); //Normal, 1.0, 0.2);
    bool waitEndMtoDifferentiate = true;
    bool doNotRescaleS = false;
    double apoptoticTime = 0.1;
    double percentBystanderG0 = 0.5;

    pop->setParams(typeDifferentiation, paramDiff, distribDeath, distribG0, distribG1,distribS,distribG2M,distribTot,waitEndMtoDifferentiate,doNotRescaleS,apoptoticTime, percentBystanderG0);

    initialNumbers = 20000;
    inflowRate = 200;       // cells per hr
    synchronizeInflow = true; // all additional are in beginning og G0
    scalingRatio = 1;

    preSimTime = 10;
    timeSimulation = 1000;    // hours
    timeEDU = 1;
    durationEDU = 1;
    timeBRDU = 0;
    durationBRDU = 1;
    AnalysisPoints.resize(10, 0);
    AnalysisPoints = {2,3,4,5,6,8,10,12,16,20};

    thresholdEDU = 0.1;
    thresholdBRDU = 0.1;

    currentObserver = nullptr;
    inflowDeficit = 0;
}


void onePopulation::initialize(observerOneSim *MO){
    currentObserver = MO;
    if(currentObserver) currentObserver->clear();

    //cout << "creating a onePopulation, memory has " << cptthymo << " thymocytes" << endl;

    // here, need to calculate death rate to be at equilibrium with inflow (??)

    time = -preSimTime;
    currentEDUdose = 0;
    currentBRDUdose = 0;
    inflowDeficit = 0;

    pop->clear(); // that will also delete the cells
    double initN = static_cast<double>(initialNumbers);

    //cout << "Generating " << initN << endl;
    pop->initializeRandom(initN, time); // note: this is not synchronized (generate as approx of steady state)
    ofstream f("InitPops.txt");
    f << pop->print() << endl;
    f.close();
}

string onePopulation::print(){
    stringstream res;
    res << "Population: simulating stage " << pop->name << ", contains " << pop->nbInsiders() << "\n";
    return res.str();
}

void onePopulation::reset(){
    pop->clear();
}

#define DRUGtype 0
double drugFunction(double time, double timeStart, double duration, double dt){
    if(DRUGtype == 0){
        if((time >= timeStart) && (time < timeStart + duration + dt)){
            return 1.0;
        } else {
            return 0.;
        }
    }
    if(DRUGtype == 1){
        double ke = 2;
        double ka = 10;
         if(time <= timeStart) return 0.;
         return 1*(exp(-ke*(time - timeStart)) - exp(-ka*(time - timeStart)));
    }

    if(DRUGtype == 2){
        double ke = 2;
        double ka = 10;
         if(time <= timeStart) return 0.;
         return exp(-ke*0.5*(time - timeStart)) - exp(-ka*0.5*(time - timeStart));
    }
}

// Note, the populationToTakeCellsFrom is also updated.
bool onePopulation::timeStep(onePopulation* populationToTakeCellsFrom){

    // when end of simulation is reached
    if(time >= timeSimulation) {time += dt; return false;}

    // cout << "t=" << time << endl;

    // 1 - update the dose of BRDU at that time
    currentBRDUdose = drugFunction(time, timeBRDU, durationBRDU, dt);
    currentEDUdose  = drugFunction(time, timeEDU,  durationEDU,  dt);

    // 2 - one time-step, get the list of leaving cells
    vector<thymocyte*> listLeavingCells = pop->timeStep(time, dt, currentEDUdose, currentBRDUdose);

    // we will do nothing from those cells, so delete them
    for(size_t iT = 0; iT < listLeavingCells.size(); ++iT){
        delete listLeavingCells[iT];
        cptthymo--;
    }

    if(populationToTakeCellsFrom == nullptr || populationToTakeCellsFrom->pop == nullptr){
        // 3 - Add cells according to additional influx (useful when simulating only one population)
        // Note: we add after time-step to avoid a cells enter and leaves the same time-step.
        pop->addInflow(static_cast<double>(inflowRate * dt / scalingRatio), synchronizeInflow, time);
    } else {
        // the outflow of the parent populations gives available cells for inflow
        // note: here, we make the parent population update in time, so don't do it elswehere
        vector<thymocyte*> listLeavingCells = populationToTakeCellsFrom->pop->timeStep(time, dt, currentEDUdose, currentBRDUdose);
        size_t n_available = listLeavingCells.size();

        // In theory, this is the amount of cells that should enter per dt
        double requestedInflow = static_cast<double>(inflowRate * dt / scalingRatio);
        // for transforming into an integer, with probability the decimal part, adds 1
        if(random::uniformDouble(0,1) <  requestedInflow - static_cast<int>(requestedInflow)){
            requestedInflow = requestedInflow+1;
        }
        // Now writing as integer what is needed + potential inflow deficit from previous steps
        // (i.e. if there were not enough cells for inflow at previous steps)
        // and recalculates the new inflow deficit (depending if could catch up with the new available cells)
        size_t tryingToAdd = static_cast<size_t>(requestedInflow) + inflowDeficit;
        // only show deficit if more than 5 cells missing
        if(inflowDeficit > 5){
            cout << time << ", " << n_available << " Available, " << static_cast<int>(requestedInflow) << " requested per timestep + " << inflowDeficit << " previous inflow deficit " << endl;
        }
        if(tryingToAdd > n_available){
            inflowDeficit = tryingToAdd - n_available;
            tryingToAdd = n_available;
        } else {
            inflowDeficit = 0;
        }


        // Now, will take a random selection of tryingToAdd cells
        // We restart the cell cycle at G0 upon entry (this matches the "waitEndMtoDivide" assumption)
        // it would also be possible to start at a random phase too.
        random::shuffle<thymocyte*>(listLeavingCells);
        for(size_t i = 0; i < tryingToAdd; ++i){

            thymocyte* outsiderFromParentPop = listLeavingCells[i];
            // either it was a newly generated thymocyte after mitosis, then we need to resample its life according to the new population
            if(fabs(outsiderFromParentPop->tbirth - time) < 2*dt){
                thymocyte* newThymocyteWithResampledPhases = pop->generateCell(time, false, 0);
                this->pop->addCell(newThymocyteWithResampledPhases, true);
                newThymocyteWithResampledPhases->incomer = true;
                newThymocyteWithResampledPhases->BRDU = outsiderFromParentPop->currentBRDU();
                newThymocyteWithResampledPhases->EDU = outsiderFromParentPop->currentEDU();
                newThymocyteWithResampledPhases->gen = 0;
                newThymocyteWithResampledPhases->timeInThisPopulation = 0;
                delete outsiderFromParentPop;
            } else {
            // or it was differentiating without mitosis and we just keep it (not sure what to do about the duration of remaining phases)
                this->pop->addCell(outsiderFromParentPop, true);
                outsiderFromParentPop->incomer = true;
                //cout << ".";
            }

        }
        for(size_t iT = tryingToAdd; iT < listLeavingCells.size(); ++iT){
            delete listLeavingCells[iT];
        }

        // now we can delete them
        populationToTakeCellsFrom->pop->addInflow(static_cast<double>(populationToTakeCellsFrom->inflowRate * dt / populationToTakeCellsFrom->scalingRatio), populationToTakeCellsFrom->synchronizeInflow, time);
        populationToTakeCellsFrom->analyzeTimePoint(populationToTakeCellsFrom->pop, populationToTakeCellsFrom->currentObserver);
    }

    // This will analyze the populations and make plots (only for the present population, not for the parent population)
    analyzeTimePoint(pop, currentObserver);


    time += dt;
    return true; // means to continue
}




// this function is only using time and threshold EDU/BRDU, could be defined outside onePopulation
void onePopulation::analyzeTimePoint(stage* popOfInterest, observerOneSim* observerThisPop, bool onlyIncomers){
    // Now stores kinetic informations into the observers
    //static int cpt2 = 0;
    //cpt2++;
    //if(true) if((cpt2 % 5) == 0){
        double nBRDU = 0;
        double nEDU = 0;
        double nEDUpBRDUp = 0;
        double nEDUnBRDUn = 0;
        double avgGen = 0;
        double nG1 = 0;
        double nS = 0;
        double nG2M = 0;
        double nG0 = 0;
        double nNew = 0;
        double nDead = 0; // apoptotic
        vector<int> histGen(100,0);
        double nTot = popOfInterest->nbInsiders();
        double nTotAnalyzed = 0;
        for(size_t i = 0; i < static_cast<size_t>(nTot + 1e-6); ++i){
            thymocyte* tcell = popOfInterest->insiders.at(i);
            if(tcell && (!onlyIncomers || tcell->incomer == true)){
                nTotAnalyzed++;
                if(tcell && ((tcell->status == NORMAL) || (tcell->status == DIVIDING))){
                    if(tcell->currentEDU() > thresholdEDU){
                        nEDU++;
                        if(tcell->currentBRDU() > thresholdBRDU){
                            nEDUpBRDUp++;
                        }
                    }
                    if(tcell->currentBRDU() > thresholdBRDU){
                        nBRDU++;
                    } else if (tcell->currentEDU() < thresholdEDU){
                        nEDUnBRDUn++;
                    }
                    avgGen += tcell->gen;
                    if(tcell->cyclestatus == G0) nG0++;
                    if(tcell->cyclestatus == G1) nG1++;
                    if(tcell->cyclestatus == S) nS++;
                    if(tcell->cyclestatus == G2M) nG2M++;
                    if(tcell->incomer) nNew++;
                    if(tcell->gen < 0) cerr << "Negative Generation!!" << endl;
                    if(tcell->gen < 100) histGen[static_cast<size_t>(tcell->gen)]++;
                    if(tcell->markedForExit == thymocyte::mark_exit)
                        cerr << "MARK not exited" << endl;
                }
                if(tcell && (tcell->status == DEAD)) nDead++;
            }
        }
        nTot = nTotAnalyzed;
        avgGen /= (max(nTot, 1.));

        if(observerThisPop){
            observerThisPop->o_doseBRDU.pushData(currentBRDUdose, time);
            observerThisPop->o_doseEDU.pushData(currentEDUdose, time);
            observerThisPop->o_popSize.pushData(pop->nbInsiders(), time);
            observerThisPop->o_prcBRDU.pushData(100.* (double) nBRDU / (max(1.,nTot)), time);
            observerThisPop->o_prcEDU.pushData(100.* (double) nEDU / (max(1.,nTot)), time);
            observerThisPop->o_prcEDUpBRDUp.pushData(100.* (double) nEDUpBRDUp / (max(1.,nTot)), time);
            observerThisPop->o_prcEDUpBRDUn.pushData(100.* (double) (nEDU - nEDUpBRDUp) / (max(1.,nTot)), time);
            observerThisPop->o_prcEDUnBRDUp.pushData(100.* (double) (nBRDU - nEDUpBRDUp) / (max(1.,nTot)), time);
            observerThisPop->o_prcEDUnBRDUn.pushData(100.* (double) nEDUnBRDUn / (max(1.,nTot)), time);
            observerThisPop->o_avgGen.pushData(avgGen, time);
            observerThisPop->o_nbG1.pushData(nG1, time);
            observerThisPop->o_nbS.pushData(nS, time);
            observerThisPop->o_nbG2M.pushData(nG2M, time);
            observerThisPop->o_nbG0.pushData(nG0, time);
            observerThisPop->o_nbNewcomers.pushData(nNew, time);
            /////currentObserver->o_flowOut.pushData(listLeavingCells.size(), time);
            observerThisPop->o_avgGenOut.pushData(0, time); // need to think how to do it
            observerThisPop->o_gen0.pushData(histGen[0], time);
            observerThisPop->o_gen1.pushData(histGen[1], time);
            observerThisPop->o_gen2.pushData(histGen[2], time);
            observerThisPop->o_gen3.pushData(histGen[3], time);
            observerThisPop->o_gen4.pushData(histGen[4], time);
            observerThisPop->o_gen5.pushData(histGen[5], time);
            observerThisPop->o_gen6.pushData(histGen[6], time);
            observerThisPop->o_gen7.pushData(histGen[7], time);
            observerThisPop->o_gen8.pushData(histGen[8], time);
            observerThisPop->o_gen9.pushData(histGen[9], time);
            observerThisPop->o_gen10.pushData(histGen[10], time);
            observerThisPop->o_apopt.pushData(nDead, time);
        }
    //}
}

void onePopulation::finalize(){
    //if(currentObserver) currentObserver->writeFiles();
}










/*  --------------------------------  Counter  -------------------------------- */

counter::counter(){clear();}
void counter::clear(){cnt = 0;}
int counter::gen(){return(++cnt);}

/*  --------------------------------  Cell ---------------------------------    */

cell::cell(cell* _parent){
    genealogy.clear();		// just a precaution

    // New ID for this cell
    IDcell = cnt.gen();

    if(!useLowMemory){
        // Check the ID is not taken (it should not, just test)
        if(mm.check_exists(IDcell)){
            cerr << "ERR : trying to create a cell whose ID (" << IDcell << ")already exists (cell : cell())\n";
        }
        // Uses itself as parent if added cell (orphan !)
        if(_parent == nullptr){
            ID_parent = IDcell;
            genealogy.push_back(IDcell);
        }
        else {
            ID_parent = _parent->ID();
            if(! mm.check_exists(ID_parent)){
                cerr << "ERR : The parent (cell* type), given to create a cell has an ID that is not known ("<< ID_parent <<")\n";
            }
            vector<int>* gen = _parent->getGenealogy();
            if(gen == nullptr) {
                cerr << "ERR : the parent (ID " << ID_parent << ") has no genealogy !! (null vector got from parent->getGenealogy()). --> Warning, No parent is taken\n";
                ID_parent = IDcell;
                genealogy.push_back(IDcell);
            }
            else {
                size_t S = gen->size();
                for(size_t i = 0; i < S; ++i){
                    genealogy.push_back((*gen)[i]);
                }
            }
        }

        mm.add(IDcell, this);
    }
}

int cell::par(){
    return ID_parent;
}

int cell::ID(){
    return IDcell;
}

vector<int>* cell::getGenealogy(){
    return &genealogy;
}

/*   --------------------------------  Mem  --------------------------------  */

mem::mem(){
    clear();
}

void mem::clear(){
    size = all.size();
    if(size > 0) cerr << "Warning : all the cells will be killed, no track of them will remain. Be afraid of seg faults !";
    for(size_t i = 0; i < size; ++i){
        if(all[i] != nullptr){
            delete all[i];
            cptthymo--; // weird
        }
    }
    all.clear();
    size = 0;
}

bool mem::check_exists(int ID){
    if((ID < 0) || (ID >= size)) return false;
    if(all[ID] == nullptr) return false; else return true;
}

void mem::add(int ID, cell* c){
    if(ID < 0) cerr << "ERR : mem::add, wrong ID (" << ID << ")" << endl;
    if(check_exists(ID)){
        cerr << "ERR : mem::add : trying to add a cell with an already allocated ID (" << ID << ")" << endl;
    }
    if(c == nullptr){
        cerr << "ERR : NULL cell given to add in mem::ADD, with ID (" << ID << ")" << endl;
    }
    if(ID >= size) all.resize(ID+1);	// vérifier qu'il rajoute des null
    // this check can be removed, just a precaution.
    size_t newsize = all.size();
    for(size_t i = size; i < newsize; ++i){
        if(all[i] != nullptr) cerr << "Warning : the resizing (upsizing) should have created NULL !!!" << endl;
    }
    size = newsize;
    all[ID] = c;
}

void mem::print(){
    cout << "Liste ID known:";
    for(size_t i = 0; i < size; ++i){
        if(all[i] != nullptr) cout << " " << i;
    }
    cout << endl;
}



