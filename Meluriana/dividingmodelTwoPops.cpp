#include "dividingmodelTwoPops.h"
#include "wholethymus.h"

dividingModelWithLabelledInflow::dividingModelWithLabelledInflow() : modelAgentBased(NbVariables, NbParameters), background(0) {

    currentSim = nullptr; // gets deleted and a new one created at each simulation.
    ancestors = nullptr;

    currentObs = new observerOneSim();
    currentObsAncestors = new observerOneSim();
    currentObsIncomers = new observerOneSim();

    name = string("Population A->B following BRDU/EDU staining");

    // Now we are in hours
    dt = 0.02; //0.001; // initial time step -> then it is adaptive around this value
    print_every_dt = 0.1; //every how many seconds it is plotting

    // Name of variables
    names[prcEDUpBRDUp] = "prcEDU+BRDU+";
    names[prcEDUpBRDUn] = "prcEDU+BRDU-";
    names[prcEDUnBRDUp] = "prcEDU-BRDU+";
    names[prcEDUnBRDUn] = "prcEDU-BRDU-";
    names[PreInG1] = "PreInG1";
    names[MiddleInG1] = "MiddleInG1";
    names[PostInG1] = "PostInG1";
    names[PreInS] = "PreInS";
    names[MiddleInS] = "MiddleInS";
    names[PostInS] = "PostInS";
    names[PreInG2M] = "PreInG2M";
    names[MiddleInG2M] = "MiddleInG2M";
    names[PostInG2M] = "PostInG2M";
    names[UnstInG1] = "UnstInG1";
    names[UnstInS] = "UnstInS";
    names[UnstInG2M] = "UnstInG2M";
    names[TotInG1] = "TotInG1";
    names[TotInS] = "TotInS";
    names[TotInG2] = "TotInG2";
    names[AvgDNAPre] = "AvgDNAPre";
    names[AvgDNAMiddle] = "AvgDNAMiddle";
    names[AvgDNAPost] = "AvgDNAPost";
    names[AvgDNABRDU] = "AvgDNABRDU";
    names[AvgDNAUNeg] = "AvgDNAUNeg";
    names[AvgDNAEDU] = "AvgDNAEDU";
    names[prcBRDU] = "prcBRDU";
    names[prcEDU] = "prcEDU";
    names[doseEDU] = "doseEDU";
    names[doseBRDU] = "doseBRDU";
    names[nCells] = "nCells";
    names[popSize] = "popSize";
    names[avgGen] = "avgGen";
    names[nbG1] = "nbG1";
    names[nbS] = "nbS";
    names[nbG2M] = "nbG2M";
    names[nbG0] = "nbG0";
    names[nbEarly] = "nbEarly";
    names[nbMiddle] = "nbMiddle";
    names[nbPost] = "nbPost";
    names[nbNewcomers] = "nbNewcomers";
    names[flowOut] = "flowOut";
    names[avgGenOut] = "avgGenOut";
    names[gen0] = "gen0";
    names[gen1] = "gen1";
    names[gen2] = "gen2";
    names[gen3] = "gen3";
    names[gen4] = "gen4";
    names[gen5] = "gen5";
    names[gen6] = "gen6";
    names[gen7] = "gen7";
    names[gen8] = "gen8";
    names[gen9] = "gen9";
    names[gen10] = "gen10";
    names[apopt] = "apopt";


    // the names of variables that can be accessed by outside (global name-space)
    extNames[prcBRDU] = "prcBRDUpos";
    extNames[prcEDU] = "prcEDUpos";
    extNames[prcEDUpBRDUp] = "prcEDUposBRDUpos";
    extNames[prcEDUpBRDUn] = "prcEDUposBRDUneg";
    extNames[prcEDUnBRDUp] = "prcEDUnegBRDUpos";
    extNames[prcEDUnBRDUn] = "prcEDUnegBRDUneg";
    extNames[AvgDNAPre] = "AvgDNAPre";
    extNames[AvgDNAMiddle] = "AvgDNAMiddle";
    extNames[AvgDNAPost] = "AvgDNAPost";
    extNames[PreInG1] = "PreInG1";
    extNames[MiddleInG1] = "MiddleInG1";
    extNames[PostInG1] = "PostInG1";
    extNames[PreInS] = "PreInS";
    extNames[MiddleInS] = "MiddleInS";
    extNames[PostInS] = "PostInS";
    extNames[PreInG2M] = "PreInG2M";
    extNames[MiddleInG2M] = "MiddleInG2M";
    extNames[PostInG2M] = "PostInG2M";
    extNames[AvgDNABRDU] = "AvgDNABRDU";
    extNames[AvgDNAUNeg] = "AvgDNAUNeg";
    extNames[AvgDNAEDU] = "AvgDNAEDU";
    extNames[UnstInG1] = "UnstInG1";
    extNames[UnstInS] = "UnstInS";
    extNames[UnstInG2M] = "UnstInG2M";
    extNames[TotInG1] = "percentG0G1";
    extNames[TotInS] = "percentS";
    extNames[TotInG2] = "percentG2M";
    extNames[nCells] = "nCells";
    // to extend if we have data

    // Name of parameters
    paramNames[aimedPopSize] = "aimedPopSize";
    paramNames[avgG1] = "avgG1";
    paramNames[avgS] = "avgS";
    paramNames[avgG2M] = "avgG2M";
    paramNames[widthG1] = "widthG1";
    paramNames[widthS] = "widthS";
    paramNames[widthG2M] = "widthG2M";
    paramNames[paramDiff] = "paramDiff";
    paramNames[deathRate] = "deathRate";
    paramNames[initialG1] = "initialG1";
    paramNames[initialS] = "initialS";
    paramNames[initialG2M] = "initialG2M";
    paramNames[initialWidthG1] = "initialWidthG1";
    paramNames[initialWidthS] = "initialWidthS";
    paramNames[initialWidthG2M] = "initialWidthG2M";
    paramNames[initialParamDiff] = "initialParamDiff";
    paramNames[initialDeathRate] = "initialDeathRate";
    paramNames[percentQuiescentG0] = "percentQuiescentG0";
    paramNames[typePopulation] = "pop:lam0,nDiv1,rate2,+3LogNorm";
    paramNames[waitDivideToDiff] = "waitDivideToDiff:0_or_1";
    paramNames[initialWaitDivideToDiff] = "initialWaitDivideToDifferentiate:0_or_1";
    paramNames[thresholdPos] = "thresholdPos";
    paramNames[timeBRDU] = "timeBRDU";
    paramNames[durationBRDU] = "durationBRDU";
    paramNames[timeEDU] = "timeEDU";
    paramNames[durationEDU] = "durationEDU";
    paramNames[preSimTime] = "preSimTime";
    paramNames[timeSimulation] = "timeSimulation";
    paramNames[apoptoticTime] = "apoptoticTime";
    paramNames[inflowRate] = "inflowRate";
    paramNames[showAncestors] = "show0Main_1Ances_2Inflo";
    paramNames[wacthing_window_pop] = "wacthing_window_pop";


    // default large boundaries for the parameters
    paramLowBounds[aimedPopSize] = 10;
    paramLowBounds[avgG1] = 1;
    paramLowBounds[avgS] = 1;
    paramLowBounds[avgG2M] = 0.1;
    paramLowBounds[widthG1] = 0.1;
    paramLowBounds[widthS] = 0.1;
    paramLowBounds[widthG2M] = 0.1;
    paramLowBounds[paramDiff] = 1.1;
    paramLowBounds[deathRate] = 0.01;
    paramLowBounds[initialG1] = 1;
    paramLowBounds[initialS] = 1;
    paramLowBounds[initialG2M] = 0.1;
    paramLowBounds[initialWidthG1] = 0.1;
    paramLowBounds[initialWidthS] = 0.1;
    paramLowBounds[initialWidthG2M] = 0.1;
    paramLowBounds[initialParamDiff] = 1.1;
    paramLowBounds[initialDeathRate] = 0.01;
    paramLowBounds[percentQuiescentG0] = 0.01;
    paramLowBounds[typePopulation] = 4;
    paramLowBounds[thresholdPos] = 0.01;
    paramLowBounds[waitDivideToDiff] = 0;
    paramLowBounds[initialWaitDivideToDiff] = 0;
    paramLowBounds[timeBRDU] = 1;
    paramLowBounds[durationBRDU] = 0.1;
    paramLowBounds[timeEDU] = 0;
    paramLowBounds[durationEDU] = 0.1;
    paramLowBounds[preSimTime] = 0;
    paramLowBounds[timeSimulation] = 1;
    paramLowBounds[apoptoticTime] = 0.01;
    paramLowBounds[inflowRate] = 0.01;
    paramLowBounds[showAncestors] = 0;
    paramLowBounds[wacthing_window_pop] = 0.5;



    paramUpBounds[aimedPopSize] = 100000;
    paramUpBounds[avgG1] = 25;
    paramUpBounds[avgS] = 10;
    paramUpBounds[avgG2M] = 5;
    paramUpBounds[widthG1] = 10;
    paramUpBounds[widthS] = 10;
    paramUpBounds[widthG2M] = 5;
    paramUpBounds[paramDiff] = 8;
    paramUpBounds[deathRate] = 0.1;
    paramUpBounds[initialG1] = 25;
    paramUpBounds[initialS] = 10;
    paramUpBounds[initialG2M] = 5;
    paramUpBounds[initialWidthG1] = 10;
    paramUpBounds[initialWidthS] = 10;
    paramUpBounds[initialWidthG2M] = 5;
    paramUpBounds[initialParamDiff] = 8;
    paramUpBounds[initialDeathRate] = 0.1;
    paramUpBounds[percentQuiescentG0] = 0.99;
    paramUpBounds[typePopulation] = 4;
    paramUpBounds[thresholdPos] = 0.01;
    paramUpBounds[waitDivideToDiff] = 1;
    paramUpBounds[initialWaitDivideToDiff] = 1;
    paramUpBounds[timeBRDU] = 1;
    paramUpBounds[durationBRDU] = 1;
    paramUpBounds[timeEDU] = 0;
    paramUpBounds[durationEDU] = 1;
    paramUpBounds[preSimTime] = 0;
    paramUpBounds[timeSimulation] = 1;
    paramUpBounds[apoptoticTime] = 10;
    paramUpBounds[inflowRate] = 10;
    paramUpBounds[showAncestors] = 2;
    paramUpBounds[wacthing_window_pop] = 1000;

}

void dividingModelWithLabelledInflow::setBaseParameters(){
    background = 0;
    params.clear();     // to make sure they are all put to zero
    params.resize(NbParameters, 0.0);

    params[aimedPopSize] = 1000;
    params[avgG1] = 10;
    params[avgS] = 7;
    params[avgG2M] = 2;
    params[widthG1] = 1;
    params[widthS] = 1;
    params[widthG2M] = 0.5;
    params[paramDiff] = 5.3;
    params[deathRate] = 0.05;
    params[initialG1] = 5.0;
    params[initialS] = 4.0;
    params[initialG2M] = 1.0;
    params[initialWidthG1] = 1.0;
    params[initialWidthS] = 1.0;
    params[initialWidthG2M] = 0.2;
    params[initialParamDiff] = 2.5;
    params[initialDeathRate] = 0.05;
    params[percentQuiescentG0] = 0.05;
    params[typePopulation] = 4;
    params[thresholdPos] = 0.1;
    params[waitDivideToDiff] = 1;
    params[initialWaitDivideToDiff] = 1;
    params[timeBRDU] = 0;
    params[durationBRDU] = 1;
    params[timeEDU] = 1;
    params[durationEDU] = 1;
    params[preSimTime] = 0;
    params[timeSimulation] = 200;
    params[apoptoticTime] = 0.1;
    params[inflowRate] = 10;
    params[showAncestors] = 0;
    params[wacthing_window_pop] = 1000;

    setBaseParametersDone();
}

void dividingModelWithLabelledInflow::finalize(){

}

static int cptErrors = 0;
void dividingModelWithLabelledInflow::initialise(long long _background){ // don't touch to parameters !
    background = _background;
    val.clear();    val.resize(NbVariables, 0.0);
    init.clear();   init.resize(NbVariables, 0.0);

    if(!currentSim) currentSim = new onePopulation();   // always reuse the existe simulation if exists
    if(!ancestors) ancestors = new onePopulation();

    currentSim->dt = dt;
    currentSim->preSimTime = params[preSimTime];
    currentSim->timeSimulation = params[timeSimulation];
    currentSim->timeBRDU = params[timeBRDU];
    currentSim->durationBRDU = params[durationBRDU];
    currentSim->timeEDU = params[timeEDU];
    currentSim->durationEDU = params[durationEDU];
    currentSim->thresholdEDU = params[thresholdPos];
    currentSim->thresholdBRDU = params[thresholdPos];
    // currentSim->inflowRate = params[inflowRate]; // this parameter will be derived from other parameters to ensure population size at steady state.
    currentSim->initialNumbers = static_cast<int>(params[aimedPopSize]);
    currentSim->scalingRatio = 1.0;          // this options allows to simulate N-fold less cells but to show results as N-fold more.
    currentSim->synchronizeInflow = true;     // true means newcoming cells enter at G0(directly G1)
                                              // for the cells quiescent at G0, they are bystander, no new cell and no new exit


    ancestors->dt = dt;
    ancestors->preSimTime = params[preSimTime];
    ancestors->timeSimulation = params[timeSimulation];
    ancestors->timeBRDU = params[timeBRDU];
    ancestors->durationBRDU = params[durationBRDU];
    ancestors->timeEDU = params[timeEDU];
    ancestors->durationEDU = params[durationEDU];
    ancestors->thresholdEDU = params[thresholdPos];
    ancestors->thresholdBRDU = params[thresholdPos];
    ancestors->initialNumbers = static_cast<int>(5*params[aimedPopSize]); // Not sure how to do this one, will take more cells to make sure there is enough outflow
    ancestors->scalingRatio = 1.0;          // this options allows to simulate N-fold less cells but to show results as N-fold more.
    ancestors->synchronizeInflow = true;     // true means newcoming cells enter at G0.
                                              // for the cells quiescent at G0, we do not add new cells, maybe we should not make them exit... difficult



    stage* simMain = currentSim->pop;
    stage* simAnces = ancestors->pop;
    if(!simMain) cerr << "ERR: the main population has empty field pop)" << endl;
    if(!simAnces) cerr << "ERR: the ancestor population has empty field pop)" << endl;

    // population types are defined in distribution.h, enum: laminarFiniteTime=0, finiteNrDiv=1, constantDiffRate=2
    int choicePop = static_cast<int>(params[typePopulation] + 0.01);
    switch(choicePop){
        case 0: case 3:    {
            simMain->typeDifferentiation = stage::laminarFiniteTime;
            simAnces->typeDifferentiation = stage::laminarFiniteTime;
        break;}
        case 1: case 4:  {
            simMain->typeDifferentiation = stage::finiteNrDiv;
            simAnces->typeDifferentiation = stage::finiteNrDiv;
        break;}
        case 2: case 5:  {
            simMain->typeDifferentiation = stage::constantDiffRate;
            simAnces->typeDifferentiation = stage::constantDiffRate;
        break;}
    }

    int typeDistributions = Normal;
    switch(choicePop){
        case 0: case 1: case 2:       {typeDistributions = Normal; break;}
        case 3: case 4: case 5:     {typeDistributions = LogNormal; break;}
    }

    {
        double G1_mu1 = params[avgG1];
        double S_mu1 = params[avgS];
        double G2M_mu1 = params[avgG2M];
        double G1_sigma1 = params[widthG1];
        double S_sigma1 = params[widthS];
        double G2M_sigma1 = params[widthG2M];
        double cycleTotMu = params[avgG1] + params[avgS] + params[avgG2M];
        double cycleTotSigma = 0;

        // the Mu and Sigma of a LogNormal are NOT the average and standard deviation => Need to convert
        if(typeDistributions == LogNormal){

            std::pair<double,double> rescaleG1 = LogNormParameters(params[avgG1], params[widthG1]);
            G1_mu1 = rescaleG1.first;
            G1_sigma1 = rescaleG1.second;

            std::pair<double,double> rescaleS = LogNormParameters(params[avgS], params[widthS]);
            S_mu1 = rescaleS.first;
            S_sigma1 = rescaleS.second;

            std::pair<double,double> rescaleG2M = LogNormParameters(params[avgG2M], params[widthG2M]);
            G2M_mu1 = rescaleG2M.first;
            G2M_sigma1 = rescaleG2M.second;

            std::pair<double,double> rescaleTot = LogNormParameters(cycleTotMu, cycleTotSigma);
            cycleTotMu = rescaleTot.first;
            cycleTotSigma = rescaleTot.second;
        }

        simMain->distribG1->set(typeDistributions, G1_mu1, G1_sigma1);
        simMain->distribS->set(typeDistributions, S_mu1, S_sigma1);
        simMain->distribG2M->set(typeDistributions, G2M_mu1, G2M_sigma1);
        simMain->distribTot->set(typeDistributions, cycleTotMu, cycleTotSigma);
        simMain->distribG0->set(Fixed, 0, 0);

        simMain->paramDiff = params[paramDiff];
        simMain->waitEndMtoDifferentiate = true;
        if(params[waitDivideToDiff] < 1e-5){ // should be 0 or 1
            simMain->waitEndMtoDifferentiate = false;
            params[waitDivideToDiff] = 0;
        } else {
            params[waitDivideToDiff] = 1;
        }
        simMain->apoptoticTime = params[apoptoticTime];
    }

    // For death, we define a RATE, which is also the lambda of the distribution => The average will be 1/lambda
    // can not be zero => take a minimum
    if(params[deathRate] <= 0) simMain->distribDeath->set(Fixed, 1e-9);
    else simMain->distribDeath->set(Exponential, params[deathRate]);

    simMain->percentBystanderG0 = params[percentQuiescentG0];


    // do it after death distribution is defined
    if(simMain->typeDifferentiation == stage::finiteNrDiv){
        vector<double> steady = simMain->getSteadyStateGenerations(currentSim->initialNumbers);
        if(steady.size() > 0){
            currentSim->inflowRate = steady[0] * min(1.0, max(0.0, 1.0 - params[percentQuiescentG0]))/ (max(1e-10, simMain->getInterCycleTime()));
            cout << "Inflow of main population for gen 0 steady state=" << steady[0] << " and total cycle " << simMain->getInterCycleTime() << endl;
            cout << "Inflow: " << currentSim->inflowRate << endl;
        } else {
            currentSim->inflowRate = 0;
        }
    }

    // Final step: calculates parameters that depend on each-other and initiates the population of cells.
    params[inflowRate] = currentSim->inflowRate; // derived
    currentSim->initialize(currentObs);
    t = currentSim->time; // important, might start negative
    cout << "Initialization done main Pop, now t=" << t << endl;



    {
        double G1_mu1 = params[initialG1];
        double S_mu1 = params[initialS];
        double G2M_mu1 = params[initialG2M];
        double G1_sigma1 = params[initialWidthG1];
        double S_sigma1 = params[initialWidthS];
        double G2M_sigma1 = params[initialWidthG2M];
        double cycleTotMu = params[initialG1] + params[initialS] + params[initialG2M];
        double cycleTotSigma = 0;

        // the Mu and Sigma of a LogNormal are NOT the average and standard deviation => Need to convert
        if(typeDistributions == LogNormal){

            std::pair<double,double> rescaleG1 = LogNormParameters(params[initialG1], params[initialWidthG1]);
            G1_mu1 = rescaleG1.first;
            G1_sigma1 = rescaleG1.second;

            std::pair<double,double> rescaleS = LogNormParameters(params[initialS], params[initialWidthS]);
            S_mu1 = rescaleS.first;
            S_sigma1 = rescaleS.second;

            std::pair<double,double> rescaleG2M = LogNormParameters(params[initialG2M], params[initialWidthG2M]);
            G2M_mu1 = rescaleG2M.first;
            G2M_sigma1 = rescaleG2M.second;

            std::pair<double,double> rescaleTot = LogNormParameters(cycleTotMu, cycleTotSigma);
            cycleTotMu = rescaleTot.first;
            cycleTotSigma = rescaleTot.second;
        }

        simAnces->distribG1->set(typeDistributions, G1_mu1, G1_sigma1);
        simAnces->distribS->set(typeDistributions, S_mu1, S_sigma1);
        simAnces->distribG2M->set(typeDistributions, G2M_mu1, G2M_sigma1);
        simAnces->distribTot->set(typeDistributions, cycleTotMu, cycleTotSigma);
        simAnces->distribG0->set(Fixed, 0, 0);

        simAnces->paramDiff = params[initialParamDiff];
        simAnces->waitEndMtoDifferentiate = true;
        if(params[initialWaitDivideToDiff] < 1e-5){ // should be 0 or 1
            simAnces->waitEndMtoDifferentiate = false;
            params[initialWaitDivideToDiff] = 0;
        } else {
            params[initialWaitDivideToDiff] = 1;
        }
        simAnces->apoptoticTime = params[apoptoticTime];
    }

    // For death, we define a RATE, which is also the lambda of the distribution => The average will be 1/lambda
    // can not be zero => take a minimum
    if(params[initialDeathRate] <= 0) simAnces->distribDeath->set(Fixed, 1e-9);
    else simAnces->distribDeath->set(Exponential, params[initialDeathRate]);

    // the initial population has no G0 (useless)
    simAnces->percentBystanderG0 = 0;


    // do it after death distribution is defined
    if(simAnces->typeDifferentiation == stage::finiteNrDiv){
        vector<double> steady = simAnces->getSteadyStateGenerations(ancestors->initialNumbers);
        cout << "Ancestor inflow to have steady state of ancestor gen 0 =" << steady[0] << " and total cycle " << simAnces->getInterCycleTime() << endl;
        if(steady.size() > 0){
            ancestors->inflowRate = steady[0] / (max(1e-10, simAnces->getInterCycleTime()));
            cout << "Inflow should be " << steady[0] / max(1e-10, simAnces->getInterCycleTime()) << endl;
        } else {
            ancestors->inflowRate = 0;
        }
    }

    // Final step: calculates parameters that depend on each-other and initiates the population of cells.
    ancestors->initialize(currentObsAncestors);
    //t = ancestors->time; // important, might start negative
    cout << "Initialization done main Pop, now t=" << ancestors->time << endl;


    /// Philippe: should be a better way to do this...
    if(t < 0) timeStep(t, -1e-6);

    cptErrors = 0;
    initialiseDone();
}

void dividingModelWithLabelledInflow::timeStep( const double tstart, const double tend){
    // does not need to adapt val[]

    if((cptErrors < 20) && (fabs(currentSim->time - tstart) > 1e-5)){
        cptErrors++; cerr << "Does not understand, the model seems to have been simulated on its own " << currentSim->time << " vs start requested" << tstart << endl;
    }
    for(t = tstart; t < tend; t = t + dt){
        currentSim->dt = min(dt, tend - t);
        if(params[initialParamDiff] < 1.001){
            currentSim->timeStep(); // This will add inflow but from new cells without label, as in the original simulation
        } else {
            currentSim->timeStep(ancestors);
            // we re-analyze only the incomers and save in another ovserver.
            ancestors->analyzeTimePoint(currentSim->pop, currentObsIncomers, true);
        }
    }
        // val[nCells] = I think this has no effect here (only recorded with print_sec)

}

void dividingModelWithLabelledInflow::analyzeState(const double t){
    // for experiments 0 (labelled inflow) and 3 (unlabelled inflow) we show the main population
    onePopulation* popToAnalyze = currentSim;
    observerOneSim* observerToAnalyze = currentSim->currentObserver;
    bool onlyIncomers = false;

    if(params[showAncestors] == 1){
        popToAnalyze = ancestors;
        observerToAnalyze = ancestors->currentObserver;
    }
    if(params[showAncestors] == 2){
        popToAnalyze = currentSim;
        observerToAnalyze = currentObsIncomers;
        onlyIncomers = true;
    }
    //in case there is no preSim time the first time-point is analyzed before actually simulating, so before the currentObserver has a last position (back)
    if(observerToAnalyze){
        val[doseEDU] = observerToAnalyze->o_doseEDU.last();
        val[doseBRDU] = observerToAnalyze->o_doseBRDU.last();
        val[popSize]  = observerToAnalyze->o_popSize.last();
        val[prcBRDU] = observerToAnalyze->o_prcBRDU.last();
        val[prcEDU] = observerToAnalyze->o_prcEDU.last();
        val[prcEDUpBRDUp] = observerToAnalyze->o_prcEDUpBRDUp.last();
        val[prcEDUpBRDUn] = observerToAnalyze->o_prcEDUpBRDUn.last();
        val[prcEDUnBRDUp] = observerToAnalyze->o_prcEDUnBRDUp.last();
        val[prcEDUnBRDUn] = observerToAnalyze->o_prcEDUnBRDUn.last();
        val[avgGen] = observerToAnalyze->o_avgGen.last();
        val[nbG1] = observerToAnalyze->o_nbG1.last();
        val[nbS] = observerToAnalyze->o_nbS.last();
        val[nbG2M] = observerToAnalyze->o_nbG2M.last();
        val[nbG0] = observerToAnalyze->o_nbG0.last();
        val[nbNewcomers] = observerToAnalyze-> o_nbNewcomers.last();
        val[flowOut] = observerToAnalyze->o_flowOut.last();
        val[avgGenOut] = observerToAnalyze->o_avgGenOut.last();
        val[gen0] = observerToAnalyze->o_gen0.last();
        val[gen1] = observerToAnalyze->o_gen1.last();
        val[gen2] = observerToAnalyze->o_gen2.last();
        val[gen3] = observerToAnalyze->o_gen3.last();
        val[gen4] = observerToAnalyze->o_gen4.last();
        val[gen5] = observerToAnalyze->o_gen5.last();
        val[gen6] = observerToAnalyze->o_gen6.last();
        val[gen7] = observerToAnalyze->o_gen7.last();
        val[gen8] = observerToAnalyze->o_gen8.last();
        val[gen9] = observerToAnalyze->o_gen9.last();
        val[gen10] = observerToAnalyze->o_gen10.last();
        val[apopt] = observerToAnalyze->o_apopt.last();
    } else {
        cerr << "Observer missing for recording the simulation" << endl;
        for(size_t i = 0; i < val.size(); ++i){
            val[i] = 0;
        }
    }

    // needs to fill val[]
    //cout << "Analyze " << t << endl;
    // this is copy-pasted from cytonwidget. Might be good to make a function
    vector<double> levelsDNAall;
    vector<double> levelsDNAPre;
    vector<double> levelsDNAPost;
    vector<double> levelsDNAMixed;
    vector<double> levelsDNAUnst;

    vector<double> fractionCycleAll = vector<double>(NBCycleStates, 0);
    vector<double> fractionCyclePre = vector<double>(NBCycleStates, 0);
    vector<double> fractionCyclePost = vector<double>(NBCycleStates, 0);
    vector<double> fractionCycleMixed = vector<double>(NBCycleStates, 0);
    vector<double> fractionCycleUnstained = vector<double>(NBCycleStates, 0);

    miniStats avgDNAAll;
    miniStats avgDNABRDUOnly;
    miniStats avgDNAUNeg;
    miniStats avgDNAPre;
    miniStats avgDNAPost;
    miniStats avgDNAMixed;


    stage* thisStage = popToAnalyze->pop;
    double threshold = params[thresholdPos]; //0.025;
    size_t NS = thisStage->insiders.size();
    double nTot = 0;
    for(size_t i = 0; i < NS; ++i){
        thymocyte* currentInsider = thisStage->insiders[i];
        if(!onlyIncomers || currentInsider->incomer == true){
            nTot++;
            if(currentInsider->status == DIVREMOVED) cerr << "Mayday" << endl;
            if(true){ //currentInsider->incomer != nullptr){
                if((currentInsider->status != DEAD) && (currentInsider->DNA > 0)){ // don't get why some have 0 as DNA. Maybe in disappear state but not yet removed
                    double D = currentInsider->currentDNA();
                    double B = currentInsider->currentBRDU();
                    double E = currentInsider->currentEDU();

                    // for FACS plot cout << D << "\t" << B << "\t" << E << endl;
                    levelsDNAall.push_back(D-1.);
                    fractionCycleAll[currentInsider->cyclestatus]++;

                    if((currentInsider->cyclestatus == S) || (currentInsider->cyclestatus == G2M)) avgDNAAll.addData(D-1.);

                    if((E <= threshold) && (B <= threshold)){
                        levelsDNAUnst.push_back(D);
                        if((currentInsider->cyclestatus == S) || (currentInsider->cyclestatus == G2M))  avgDNAUNeg.addData(D-1.);
                        fractionCycleUnstained[currentInsider -> cyclestatus]++;
                    }
                    if((E > threshold) && (B <= threshold)) {
                        levelsDNAPost.push_back(D); // important to calculate frequencies
                        if ((currentInsider->cyclestatus == S) || (currentInsider->cyclestatus == G2M))  avgDNAPost.addData(D-1.);
                        fractionCyclePost[currentInsider->cyclestatus]++;
                    }
                    if((E > threshold) && (B > threshold)) {
                        levelsDNAMixed.push_back(D);
                        if ((currentInsider->cyclestatus == S) || (currentInsider->cyclestatus == G2M))  {
                            avgDNAMixed.addData(D-1.);
                            avgDNABRDUOnly.addData(D-1.);
                        }
                        fractionCycleMixed[currentInsider->cyclestatus]++;
                    }
                    if((E <= threshold) && (B > threshold)) {
                        levelsDNAPre.push_back(D);
                        if ((currentInsider->cyclestatus == S) || (currentInsider->cyclestatus == G2M)) {
                            avgDNAPre.addData(D-1.);
                            avgDNABRDUOnly.addData(D-1.);
                        }
                        fractionCyclePre[currentInsider->cyclestatus]++;
                    }
                }
            }
        }
    }

    /* if(levelsDNAall.size() > 0){
        histogramFromDistrib hAll = histogramFromDistrib(levelsDNAall, 50);
    }
    if(levelsDNAPre.size() > 0){
        histogramFromDistrib hPre = histogramFromDistrib(levelsDNAPre, 50);
    }
    if(levelsDNAPost.size() > 0){
        histogramFromDistrib hPost = histogramFromDistrib(levelsDNAPost, 50);
    }
    if(levelsDNAMixed.size() > 0){
        histogramFromDistrib hMix = histogramFromDistrib(levelsDNAMixed, 50);
    }*/
    //cpt++;

    for(size_t i = 0; i < NBCycleStates; ++i){
        fractionCycleAll[i] /= 0.01 * max(1., static_cast<double>(levelsDNAall.size()));
        fractionCyclePre[i] /= 0.01 * max(1., static_cast<double>(levelsDNAPre.size()));
        fractionCyclePost[i] /= 0.01 * max(1., static_cast<double>(levelsDNAPost.size()));
        fractionCycleMixed[i] /= 0.01 * max(1., static_cast<double>(levelsDNAMixed.size()));
        fractionCycleUnstained[i] /= 0.01 * max(1., static_cast<double>(levelsDNAUnst.size()));
    }

    //cout << "Fill val[] at time " << t << endl;
    val[AvgDNAPre] =  avgDNAPre.getAverage();
    val[AvgDNAMiddle] =  avgDNAMixed.getAverage();
    val[AvgDNAPost] =  avgDNAPost.getAverage();
    val[PreInG1] = fractionCyclePre[G1] + fractionCyclePre[G0];
    val[MiddleInG1] = fractionCycleMixed[G1] + fractionCycleMixed[G0];
    val[PostInG1] = fractionCyclePost[G1] + fractionCyclePost[G0]; // attention, overestimation due to stop of cycling !!!
    val[PreInS] = fractionCyclePre[S];
    val[MiddleInS] = fractionCycleMixed[S];
    val[PostInS] = fractionCyclePost[S];
    val[PreInG2M] = fractionCyclePre[G2M];
    val[MiddleInG2M] = fractionCycleMixed[G2M];
    val[PostInG2M] = fractionCyclePost[G2M];
    val[AvgDNABRDU] =  avgDNABRDUOnly.getAverage();// avgDNAAll.getAverage();
    val[UnstInS] = fractionCycleUnstained[S];        //val[AvgDNAUNeg] = avgDNAUNeg.getAverage();
    val[TotInG1] = fractionCycleAll[G1] + fractionCycleAll[G0];
    val[TotInS] = fractionCycleAll[S];
    val[TotInG2] = fractionCycleAll[G2M];
    val[nbEarly] = avgDNAPre.getN();
    val[nbMiddle] = avgDNAMixed.getN();
    val[nbPost] = avgDNAPost.getN();
    val[nCells] = nTot;
}







