#include "cytonwidget.h"
#include "ui_cytonwidget.h"
#include "distribwidget.h"
#include <QStringList>
#include "cytonSimu.h"
#include "common.h"
#include "wholethymus.h"

enum{ hypConstant, hypPerLSH, hypPerMPH, hypCrowdness, NBHyps};

void CytonWidget::analyze(double time){

    vector<double> levelsDNAall;
    vector<double> levelsDNAPre;
    vector<double> levelsDNAPost;
    vector<double> levelsDNAMixed;

    vector<double> fractionCycleAll = vector<double>(NbStages, 0);
    vector<double> fractionCyclePre = vector<double>(NbStages, 0);
    vector<double> fractionCyclePost = vector<double>(NbStages, 0);
    vector<double> fractionCycleMixed = vector<double>(NbStages, 0);

    miniStats avgDNAAll;
    miniStats avgDNAPre;
    miniStats avgDNAPost;
    miniStats avgDNAMixed;

    double threshold = 0.025;
    int NS = thisStage->insiders.size();
    for(int i = 0; i < NS; ++i){
        thymocyte* currentInsider = thisStage->insiders[i];
        if(true){ //currentInsider->incomer){
            if((currentInsider->status != DEAD) && (currentInsider->DNA > 0)){ // don't get why some have 0 as DNA. Maybe in disappear state but not yet removed
                double D = currentInsider->currentDNA();
                double B = currentInsider->currentBRDU();
                double E = currentInsider->currentEDU();
                levelsDNAall.push_back(D);
                fractionCycleAll[currentInsider->cyclestatus]++;
                avgDNAAll.addData(D);
                if((B <= threshold) && (E > threshold)) {
                    levelsDNAPost.push_back(D);
                    avgDNAPost.addData(D);
                    fractionCyclePost[currentInsider->cyclestatus]++;
                }
                if((B > threshold) && (E > threshold)) {
                    levelsDNAMixed.push_back(D);
                    avgDNAMixed.addData(D);
                    fractionCycleMixed[currentInsider->cyclestatus]++;
                }
                if((B > threshold) && (E <= threshold)) {
                    levelsDNAPre.push_back(D);
                    avgDNAPre.addData(D);
                    fractionCyclePre[currentInsider->cyclestatus]++;
                }
            }
        }
    }

    if(levelsDNAall.size() > 0){
        histogramFromDistrib hAll = histogramFromDistrib(levelsDNAall, 50);
        grapheEDU_BRDU->Plot(cpt,hAll.densities, hAll.averageXs, QString::number(time),grapheEDU_BRDU->baseList(cpt));
    }
    if(levelsDNAPre.size() > 0){
        histogramFromDistrib hPre = histogramFromDistrib(levelsDNAPre, 50);
        grapheCyclePre->Plot(cpt,hPre.densities, hPre.averageXs, QString::number(time),grapheCyclePre->baseList(cpt));
    }
    if(levelsDNAPost.size() > 0){
        histogramFromDistrib hPost = histogramFromDistrib(levelsDNAPost, 50);
        grapheCyclePost->Plot(cpt,hPost.densities, hPost.averageXs, QString::number(time),grapheCyclePost->baseList(cpt));
    }
    if(levelsDNAMixed.size() > 0){
        histogramFromDistrib hMix = histogramFromDistrib(levelsDNAMixed, 50);
        grapheCycleMixed->Plot(cpt,hMix.densities, hMix.averageXs, QString::number(time),grapheCycleMixed->baseList(cpt));
    }
    cpt++;

    stringstream res;
    res << "Stage " << thisStage->name << ", ------- At time " << time << ", Analysis yielded: " << levelsDNAall.size() << " cells, Pre=" << levelsDNAPre.size() << "  Mixed=" << levelsDNAMixed.size() << "  Post=" << levelsDNAPost.size() << "--------" << endl;
    for(int i = 0; i < NBCycleStates; ++i){
        res << "\t" << nameCycle(i);
    }
    res << "\nAllAlive\tAvgDNA=\t" << avgDNAAll.getAverage();
    for(int i = 0; i < NBCycleStates; ++i){
        fractionCycleAll[i] /= max(1., (double) levelsDNAall.size());
        res <<  "\t" << fractionCycleAll[i];
    }
    res << "\nPre\tAvgDNA=\t" << avgDNAPre.getAverage();
    for(int i = 0; i < NBCycleStates; ++i){
        fractionCyclePre[i] /= max(1., (double) levelsDNAPre.size());
        res <<  "\t" << fractionCyclePre[i];
    }
    res << "\nPost\tAvgDNA=\t" << avgDNAPost.getAverage();
    for(int i = 0; i < NBCycleStates; ++i){
        fractionCyclePost[i] /= max(1., (double) levelsDNAPost.size());
        res <<  "\t" << fractionCyclePost[i];
    }
    res << "\nMixed\tAvgDNA=\t" << avgDNAMixed.getAverage();
    for(int i = 0; i < NBCycleStates; ++i){
        fractionCycleMixed[i] /= max(1., (double) levelsDNAMixed.size());
        res <<  "\t" << fractionCycleMixed[i];
    }
    res << endl;
    cout << res.str() << endl;
    ui->textEdit->setPlainText(ui->textEdit->toPlainText() + QString(res.str().c_str()));
}

CytonWidget::CytonWidget(stage *st, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CytonWidget)
{
    ui->setupUi(this);

    grapheEDU_BRDU = new grapheCustom(ui->widgetPlotPopNb);
    grapheCyclePost = new grapheCustom(ui->widgetPlotGFP);
    grapheCyclePre = new grapheCustom(ui->widgetPlotRFP);
    grapheCycleMixed = new grapheCustom(ui->widgetPlotRatio);

    // can show different analysis times ;-)
    grapheEDU_BRDU->setNbCurves(10);
    grapheCyclePost->setNbCurves(10);
    grapheCyclePre->setNbCurves(10);
    grapheCycleMixed->setNbCurves(10);

    grapheEDU_BRDU->setTitle(QString("BRDU/eDU"));
    grapheCyclePost->setTitle(QString("Post(leaving S between brdu and edu)"));
    grapheCyclePre->setTitle(QString("Pre(Entering S between brdu and edu)"));
    grapheCycleMixed->setTitle(QString("Mixed (inside S at brdu and EDU)"));

    currentDistribWidgetDeath = new DistribWidget(ui->widgetDistribDeath);
    currentDistribWidgetG0 = new DistribWidget(ui->widgetDistribG0);
    currentDistribWidgetG1 = new DistribWidget(ui->widgetDistribG1);
    currentDistribWidgetS = new DistribWidget(ui->widgetDistribS);
    currentDistribWidgetG2 = new DistribWidget(ui->widgetDistribG2);
    currentDistribWidgetTot = new DistribWidget(ui->widgetDistribtTot);

    ui->spinBoxMaxNrDivThisStage->setValue(0);
    ui->doubleSpinBoxConstantDiff->setValue(0);
    ui->doubleSpinBoxFiniteTimeInPop->setValue(0);

    //currentDistribWidgetG1->setMu1(0.6);
    //currentDistribWidgetG1->setSigma1(0.2);
    //currentDistribWidgetProliferation->setMu1(10.0);

    //QStringList hyps = {QString("Constant"), QString("Distribution per Lsh"), QString("Distribution per Mph"), QString("Function of crowdness")};
    //ui->comboBoxProliferation->addItems(hyps);
    //ui->comboBoxDeath->addItems(hyps);

    // keep that afterwards creating the combos ! (don't know why, it makes seg fault)
    //QObject::connect(ui->checkBoxSameGFPandRFP, SIGNAL(stateChanged(int)), this, SLOT(GFPcheckChanged()));
    //QObject::connect(ui->comboBoxDeath, SIGNAL(activated(int)), this, SLOT(hypDeathChanged()));
    //QObject::connect(ui->comboBoxProliferation, SIGNAL(activated(int)), this, SLOT(hypProlifChanged()));
    //QObject::connect(ui->pushButtonRun, SIGNAL(released()), this, SLOT(run()));
    //hypDeathChanged();
    //hypProlifChanged();

    reset(st);
}
void CytonWidget::reset(stage* st){

    cpt = 0;
    running = false;
    thisStage = st;
    applyParamsFromStage();

    grapheEDU_BRDU->clear();
    grapheCyclePost->clear();
    grapheCyclePre->clear();
    grapheCycleMixed->clear();
}
void CytonWidget::enable(bool enable){
    /*
    currentDistribWidgetDeath->setEnabled(enable);
    currentDistribWidgetG0->setEnabled(enable);
    currentDistribWidgetG1->setEnabled(enable);
    currentDistribWidgetS->setEnabled(enable);
    currentDistribWidgetG2->setEnabled(enable);
    currentDistribWidgetTot->setEnabled(enable);

    ui->checkBoxNoRescaleS->setEnabled(enable);
    ui->checkBoxWaitForEndM->setEnabled(enable);

    ui->radioButtonFiniteTime->setEnabled(enable);
    ui->spinBoxMaxNrDivThisStage->setEnabled(enable);

    ui->radioButtonFiniteDivs->setEnabled(enable);
    ui->doubleSpinBoxConstantDiff->setEnabled(enable);

    ui->radioButtonFiniteDiff->setEnabled(enable);
    ui->doubleSpinBoxFiniteTimeInPop->setEnabled(enable);
    */
}

void CytonWidget::applyParamsFromStage(){
    currentDistribWidgetDeath->setLaw(thisStage->distribDeath);
    currentDistribWidgetG0->setLaw(thisStage->distribG0);
    currentDistribWidgetG1->setLaw(thisStage->distribG1);
    currentDistribWidgetS->setLaw(thisStage->distribS);
    currentDistribWidgetG2->setLaw(thisStage->distribG2M);
    currentDistribWidgetTot->setLaw(thisStage->distribTot);

    ui->checkBoxNoRescaleS->setChecked(thisStage->doNotRescaleS);
    ui->checkBoxWaitForEndM->setChecked(thisStage->waitEndMtoDifferentiate);

    switch(thisStage->typeDifferentiation){
        case stage::laminarFiniteTime: {
            ui->radioButtonFiniteTime->setChecked(true);
            ui->doubleSpinBoxFiniteTimeInPop->setValue(thisStage->paramDiff);
            break;
        }
        case stage::finiteNrDiv: {
            ui->radioButtonFiniteDivs->setChecked(true);
            ui->spinBoxMaxNrDivThisStage->setValue(thisStage->paramDiff);
            break;
        }
        case stage::constantDiffRate:{
            ui->radioButtonFiniteDiff->setChecked(true);
            ui->doubleSpinBoxConstantDiff->setValue(thisStage->paramDiff);
            break;
        }
    }
}

void CytonWidget::applyParamsToStage(){
    thisStage->doNotRescaleS = ui->checkBoxNoRescaleS->isChecked();
    thisStage->waitEndMtoDifferentiate = ui->checkBoxWaitForEndM->isChecked();

    if(ui->radioButtonFiniteTime->isChecked()){
        thisStage->typeDifferentiation = stage::laminarFiniteTime;
        thisStage->paramDiff = ui->doubleSpinBoxFiniteTimeInPop->value();
    } else if (ui->radioButtonFiniteDivs->isChecked()){
        thisStage->typeDifferentiation = stage::finiteNrDiv;
        thisStage->paramDiff = ui->spinBoxMaxNrDivThisStage->value();
    } else if (ui->radioButtonFiniteDiff->isChecked()){
        thisStage->typeDifferentiation = stage::constantDiffRate;
        thisStage->paramDiff = ui->doubleSpinBoxConstantDiff->value();
    } else {
        cerr << "ERR: stage " << thisStage->name << ", on the interface, no type of differentiation is decided. Take constant rate" << endl;
        thisStage->typeDifferentiation = stage::constantDiffRate;
        thisStage->paramDiff = ui->doubleSpinBoxConstantDiff->value();
    }
}



// the run button is common for all windows => Inside the mainWindow,
/*
void CytonWidget::run(){
    if(running) {running = false;
        ui->pushButtonRun->setText(QString("Run !"));
        return;}
    running = true;
    ui->pushButtonRun->setText(QString("Stop"));
    multiPopSimulation* sim = new multiPopSimulation();
    sim->nbMacrophages = ui->spinBoxNbMacrophages->value();
    sim->nbLeishPerMacrophage = this->currentDistribWidgetS->getLaw();
    sim->prolifPerMacrophage = (ui->comboBoxProliferation->currentIndex() == hypPerMPH);
    sim->lawForProlif = this->currentDistribWidgetG2->getLaw();
    sim->deathPerMacrophage = (ui->comboBoxDeath->currentIndex() == hypPerMPH);
    sim->lawForDeath = this->currentDistribWidgetDeath->getLaw();
    sim->timeBleaching = ui->doubleSpinBoxTimeBleaching->value();
    sim->lawForBleaching = this->currentDistribWidgetG1->getLaw();
    sim->degradGFP = ui->doubleSpinBoxDegradGFP->value();
    sim->degradRFP = ui->doubleSpinBoxDegradRed->value();
    sim->GFPMax = ui->doubleSpinBoxGFPMax->value();
    sim->timeStayDead = ui->doubleSpinBoxTimeStayDead->value();
    sim->timeSimulation = ui->doubleSpinBoxTimeSimulation->value();
    masterObserver* newMO = new masterObserver(currentDir());
    sim->initialize(newMO);
    int counts = 0;
    while((running) && (sim->timeStep(sim->timeSimulation / 5000.))) {
        counts++;
        if((counts % 10) == 0){
            grapheEDU_BRDU->Plot(0, newMO->o_popSize.variable, newMO->o_popSize.time, QString("PopNb"), Qt::blue);
            grapheCyclePost->Plot(0, newMO->o_popGFP.variable, newMO->o_popGFP.time, QString("[GFP]"), Qt::darkGreen);
            grapheCyclePre->Plot(0, newMO->o_popRED.variable, newMO->o_popRED.time, QString("[RED]"), Qt::darkRed);
            grapheCyclePost->Plot(1, newMO->o_popAbsGFP.variable, newMO->o_popAbsGFP.time, QString("Abs GFP"), Qt::green);
            grapheCyclePre->Plot(1, newMO->o_popAbsRED.variable, newMO->o_popAbsRED.time, QString("Abs Red"), Qt::red);
            grapheCycleMixed->Plot(0, newMO->o_popRatio.variable, newMO->o_popRatio.time, QString("Ratio"), Qt::darkGray);

            //grapheGFP->rescaleY(0., 5.0);
            //grapheRFP->rescaleY(0, 5.0);
            //grapheRatio->rescaleY(0, 1.0);
        }
        //>Plot(IDrepeat, listObservers[i]->variable, listObservers[i]->time, QString("Curve ") + QString::number(i+1), plots[i]->baseList(IDrepeat));
    }
    sim->finalize();
}*/

/*void CytonWidget::hypDeathChanged(){
    switch(ui->comboBoxDeath->currentIndex()){
    case hypConstant: case hypCrowdness: {
        currentDistribWidgetDeath->enableCombo(Fixed, true);
        currentDistribWidgetDeath->enableCombo(Normal, false);
        currentDistribWidgetDeath->enableCombo(LogNormal, false);
        currentDistribWidgetDeath->enableCombo(FromData, false);
        currentDistribWidgetDeath->enableCombo(BiModal, false);
        currentDistribWidgetDeath->enableCombo(Exponential, false);
        break;}
    case hypPerLSH: case hypPerMPH: {
        currentDistribWidgetDeath->enableCombo(Fixed, false);
        currentDistribWidgetDeath->enableCombo(Normal, true);
        currentDistribWidgetDeath->enableCombo(LogNormal, true);
        currentDistribWidgetDeath->enableCombo(FromData, true);
        currentDistribWidgetDeath->enableCombo(BiModal, true);
        currentDistribWidgetDeath->enableCombo(Exponential, true);
        break;}
    }
}*/


CytonWidget::~CytonWidget()
{
    delete ui;
}
