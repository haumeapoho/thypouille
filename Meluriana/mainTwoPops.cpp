#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <set>
using namespace std;

#include "../Moonfit/moonfit.h"

#include "random.h"
#include "bootstrap.h"
#include "dividingmodelTwoPops.h"
#include "expLabelling.h"

static string folder = "C:/Users/pprobert/Desktop/fromHP/Softwares/NewArchaeropteryx/Sources/Meluriana/";
static string folderBaseResults = "C:/Users/pprobert/Desktop/fromHP/Softwares/NewArchaeropteryx/Sources/Meluriana/Results/";

// to call it: ./Meluriana twoPops DN2 WT VarG1 FixedG0 4P_D1D2D3 none
int mainTwoPops(int argc, char *argv[]){

    int NBootstrap = 10;
    size_t nRep = 10;
    size_t nbPointsIdentifiability = 12;

    if(!dirExists(folder)) cerr << "Could not find the project folder for finding data and configurations: " << folder << endl;
    if(!dirExists(folderBaseResults.c_str())) createFolder(folderBaseResults);

    // Filtering input in a list of authorized options
    string command           = string(argv[1]);
    string population        = string(argv[2]);
    string condition         = (argc >= 4) ? string(argv[3]) : "WT";
    string cycle             = (argc >= 5) ? string(argv[4]) : "VarG1";
    string allowingQuiescent = (argc >= 6) ? string(argv[5]) : "FixedG0";
    string dataset           = (argc >= 7) ? string(argv[6]) : "6P_D1D2D3";
    string addingBootstrap   = (argc >= 8) ? string(argv[7]) : "none";

    set<string> commands = {"twoPops", "twoPopsFit"};
    set<string> populations = {"DN1", "DN2", "DN3A", "DN3B", "DN4", "preSel", "postSel", "CD4SP", "CD8SP"};
    set<string> conditions = {"WT", "KO", "CTR", "IRR", "WT_KO", "CTR_IRR"};
    set<string> cycles = {"VarG1", "VarS", "VarG2M", "VarAll", "NoVar"};
    set<string> allowingQuiescents = {"FixedG0", "VariableG0"};
    set<string> datasets = {"6P_D1D2D3", "4P_D1", "4P_D1D2", "4P_D1D2D3", "4P_D1D2D3D4", "6P_D1D2D3D4"};
    set<string> addingBootstraps = {"none", "bootstrap"};

    bool success = true;
    if(commands.find(command)                  == commands.end()) {success = false; cerr << "option " << command << " not understood" << endl;}
    if(populations.find(population)            == populations.end()) {success = false; cerr << "option " << population << " not understood" << endl;}
    if(conditions.find(condition)              == conditions.end()) {success = false; cerr << "option " << condition << " not understood" << endl;}
    if(cycles.find(cycle)                      == cycles.end()) {success = false; cerr << "option " << cycle << " not understood" << endl;}
    if(allowingQuiescents.find(allowingQuiescent) == allowingQuiescents.end()) {success = false; cerr << "option " << allowingQuiescent << " not understood" << endl;}
    if(datasets.find(dataset)                  == datasets.end()) {success = false; cerr << "option " << dataset << " not understood" << endl;}
    if(addingBootstraps.find(addingBootstrap)  == addingBootstraps.end()) {success = false; cerr << "option " << addingBootstrap << " not understood" << endl;}
    if(!success) {return 0;}


    // Chosing the data files based on input
    string folderDataset = folder + "DATA/";
    string shortName = "";
    if(!dataset.compare("6P_D1D2D3"))     {folderDataset += "6TP_Data1Data2Data3/"; shortName = "6+D123";}
    if(!dataset.compare("4P_D1"))         {folderDataset += "4TP_Data1_EduBrdu/"; shortName = "4+D1";}
    if(!dataset.compare("4P_D1D2"))       {folderDataset += "4TP_Data1Data2_EduBrdu_Return/";  shortName = "4+D12";}
    if(!dataset.compare("4P_D1D2D3"))     {folderDataset += "4TP_Data1Data2Data3_EduBrdu_Return_Steady/";  shortName = "4+D123";}
    if(!dataset.compare("4P_D1D2D3D4"))   {folderDataset += "4TP_Data1Data2Data3Data4_EduBrdu_Return_Steady_DNA/";  shortName = "4+D1234";}
    if(!dataset.compare("6P_D1D2D3D4"))   {folderDataset += "6TP_Data1Data2Data3Data4/"; shortName = "6+D1234";}

    string dataFileAVG = folderDataset;
    string dataFileSTD = folderDataset;
    if(!condition.compare("WT")){
        dataFileAVG += population + "_WT_POOL_AVG.txt";
        dataFileSTD += population + "_WT_POOL_STD.txt";
    }
    if(!condition.compare("KO")){
        dataFileAVG += population + "_KO_POOL_AVG.txt";
        dataFileSTD += population + "_KO_POOL_STD.txt";
    }
    if(!condition.compare("CTR")){
        dataFileAVG += "Reg" + population + "_WT_AVG.txt";
        dataFileSTD += "Reg" + population + "_WT_STD.txt";
    }
    if(!condition.compare("IRR")){
        dataFileAVG += "Reg" + population + "_IRR_AVG.txt";
        dataFileSTD += "Reg" + population + "_IRR_STD.txt";
    }

    // Loading the data and creating the models
    TableCourse* Data_AVG = new TableCourse(dataFileAVG);
    cout << Data_AVG->print() << endl;
    TableCourse* Data_STD = new TableCourse(dataFileSTD);
    cout << Data_STD->print() << endl;

    if(!addingBootstrap.compare("bootstrap")){
        TableCourse Bootstrapped = bootstrap::randomize(*Data_AVG, *Data_STD, NBootstrap);
        Data_AVG = new TableCourse(Bootstrapped);
        cout << "Data has been boostrapped for average, keeping the standard deviation as in data" << endl;
        cout << Data_AVG->print() << endl;
    }

    Model* currentModel = new dividingModelWithLabelledInflow();
    Experiment* currentExperiment = nullptr;
    string configFile = "";
    int IDcombination = 0;

    //if(!command.compare("twoPops")){
    //     currentExperiment = new twoPopsExpDoubleLabel(currentModel);
    // } else  if(!command.compare("twoPopsFit")){
        currentExperiment = new twoPopsExpDoubleLabelSlim(currentModel);
    // }

    currentExperiment->giveData(Data_AVG, 0, Data_STD); // 0 is the index of the experiment
    currentExperiment->loadEvaluators();
    cout << "   -> Using model : " << currentModel->name << " with experiment " << currentExperiment->Identification << "\n";

    // Creating a code for these settings, and creating an output result folder
    currentExperiment->Identification = population + "+" + condition + "+" + cycle + "+" + shortName + "+" + allowingQuiescent + "+" + addingBootstrap;
    createFolder(folderBaseResults + currentExperiment->Identification);
    cout << "Will save results in " << folderBaseResults + currentExperiment->Identification << endl;

    // Now deciding the settings for optimization based on the options (which parameters to optimize)
    configFile = folder + string("GeneralConfigInflow2024.txt");
    if(!population.compare("DN3A")){configFile = folder + string("GeneralConfigInflow2024_DN2_to_DN3A.txt");}
    if(!population.compare("DN3B")){configFile = folder + string("GeneralConfigInflow2024_DN3A_to_DN3B.txt");}
    if(!population.compare("DN4")){configFile = folder + string("GeneralConfigInflow2024_DN3B_to_DN4.txt");}
    if(!population.compare("preSel")){configFile = folder + string("GeneralConfigInflow2024_DN4_to_preSel.txt");}

    if(!cycle.compare("VarG1")) {IDcombination = 1;}
    if(!cycle.compare("VarS")) {IDcombination = 2;}
    if(!cycle.compare("VarG2M")) {IDcombination = 3;}
    if(!cycle.compare("VarAll")) {IDcombination = 4;}
    if(!cycle.compare("NoVar")) {IDcombination = 0;}
    if(!allowingQuiescent.compare("VariableG0")){IDcombination += 5;}

    cout << "   -> Using model : " << currentModel->name << " with experiment " << currentExperiment->Identification << "\n";
    cout << "   -> Using configuration file: " << configFile << " with combination " << IDcombination << endl;

    // Creating a code for these settings, and creating an output result folder
    currentExperiment->Identification = population + "+" + condition + "+" + cycle + "+" + shortName + "+" + allowingQuiescent + "+" + addingBootstrap;
    createFolder(folderBaseResults + currentExperiment->Identification);
    cout << "Will save results in " << folderBaseResults + currentExperiment->Identification << endl;


    // Deciding the type of cost
    setTypeCost(SQUARE_COST);
    //    SQUARE_COST: "RSS"
    //    SQUARE_COST_STD: "RSS + StdDev"
    //    LOG_COST: "Log"
    //    PROPORTION_COST: "Ratios"

    setTypeNorm(NORM_AVERAGE);
    //    NO_NORM: " (No Norm)"
    //    NORM_AVERAGE: " Norm/Avg vars"
    //    NORM_NB_PTS: " Norm/Nb Points"
    //    NORM_AVG_AND_NB_PTS: " Norm/Avg and Nb Pts "
    //    NORM_MAX: " Norm/Max vars"
    //    NORM_MAX_AND_NB_PTS: " Norm/Max and Nb Pts"

    // Set the costs before calling the graphical interface, because it will not update once it's called
    // (although you would change the cost, the GUI would not realize and keep showing the old one)

    if(!command.compare("twoPops")){

        #ifndef WITHOUT_QT
        QApplication b(argc, argv);             // Starts the Qt application

        cout << "Launching Graphical Interface ..." << endl;
        // Step 7a: Launch the graphical interface from an experiment (containing the model inside)
        simuWin* p = new simuWin(currentExperiment);

        // Step 7b: optionally give a config file
        p->loadConfig(configFile);

        // Step 7c: show the graphical interface!
        p->show();

        p->useComb(IDcombination);


        // Step 7d: leave the control to Qt instead of finishing the program
        b.exec();

        #else
        cerr << "You have compiled Meluriana without the graphical libraries, so the 'gui' option doesn't run. Check that #WITHOUT_QT is not defined, then recompile" << endl;
        #endif
    }

    if(!command.compare("twoPopsFit")){

        currentExperiment->m->setBaseParameters();
        manageSims* MSim = new manageSims(currentExperiment);

        MSim->loadConfig(configFile);
        int nbCombinations = MSim->nbCombs;
        if(nbCombinations != 10){
            cerr << "The default configuration should have 10 combinations, should be GeneralConfigAllOptions.txt" << endl;
        }

        string optimizerName = folderBaseResults + currentExperiment->Identification + "/" + "Opt.txt";
        string contentOpt = MSim->motherCreateOptimizerFile(IDcombination,optFileHeader(Genetic25k));
        ofstream optim(optimizerName.c_str());
        optim << contentOpt;
        optim.close();
        cout << "The selected options for fitting are:" << endl;
        cout << contentOpt << endl;

        for(int j = 0; j < nRep; ++j){
            stringstream codeSim; codeSim << "Comb" << IDcombination << "_Rep" << j;
            createFolder(folderBaseResults + currentExperiment->Identification + "/" + codeSim.str());

            cout << codeSim.str() << endl;
            MSim->resetParamSetFromConfig(configFile);
            MSim->initialize();
            MSim->motherOptimize(optimizerName, 10000);
            MSim->saveHistory(folderBaseResults + currentExperiment->Identification + "/" + codeSim.str() + "/TestHistory.txt");                         // SAVES all the best parameter sets. by default, 10 000, can be modified by             msi->history.resize(max_nb_sets_to_record);
            MSim->useParamSetFromHistory(0);                                                     // takes the first set of parameters (the best), also possible to use msi->useParamSetFromHistory(0, i); for overriding only parameters from this combination,
            MSim->simulate();
            cout << "Sim OK, cost " << MSim->getCost() << endl;
            MSim->makeTextReportParamSet(folderBaseResults + currentExperiment->Identification + "/" + codeSim.str() + "/", IDcombination);
        }
    }

    if(!command.substr(0, 7).compare("identif")){

        int idParam = dividingModelWithLabelledInflow::avgG1;
        if(!command.compare("identifG1")){idParam = dividingModelWithLabelledInflow::avgG1;}
        if(!command.compare("identifS")){idParam = dividingModelWithLabelledInflow::avgS;}
        if(!command.compare("identifG2M")){idParam = dividingModelWithLabelledInflow::avgG2M;}
        if(!command.compare("identifQuiescent")){idParam = dividingModelWithLabelledInflow::percentQuiescentG0;}
        if(!command.compare("identifWG1")){idParam = dividingModelWithLabelledInflow::widthG1;}
        if(!command.compare("identifWS")){idParam = dividingModelWithLabelledInflow::widthS;}
        if(!command.compare("identifWG2M")){idParam = dividingModelWithLabelledInflow::widthG2M;}
        if(!command.compare("identifDeath")){idParam = dividingModelWithLabelledInflow::deathRate;}
        if(!command.compare("identifnDiv")){idParam = dividingModelWithLabelledInflow::paramDiff;}

        stringstream codeIdentif; codeIdentif << folderBaseResults << currentExperiment->Identification << "/" << "_Identif" << idParam << "/";
        createFolder(codeIdentif.str());

        currentExperiment->m->setBaseParameters();
        manageSims* MSim = new manageSims(currentExperiment);

        MSim->loadConfig(configFile);
        int nbCombinations = MSim->nbCombs;
        if(nbCombinations != 10){
            cerr << "The default configuration should have 10 combinations, should be GeneralConfigAllOptions.txt" << endl;
        }

        MSim->resetParamSetFromConfig(configFile);
        vector<double> paramsRef = currentModel->getParameters();
        MSim->prepareOptFilesForIdentifibiality(codeIdentif.str(),idParam, IDcombination, optFileHeader(GeneticFast));
        MSim->motherIdentifiability(paramsRef, idParam, nbPointsIdentifiability);
        MSim->makeTextReportParamSet(codeIdentif.str(), IDcombination);
    }

    cout << " ===== The End! ==== " << endl;
    return 0;
}



