include("../Moonfit/moonfit.pri")

#name of the executable file generated
TARGET = Urania4.03


#put += console to run in a separate terminal
#CONFIG += console

#bundles might be required for MAC OS ??
#CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
    bootstrap.h \
    cytonSimu.h \
    distribution.h \
    dividingmodelOnePop.h \
    dividingmodelTwoPops.h \
    mainOldScripts.h \
    mainSynchro.h \
    mainTwoPops.h \
    random.h \
    statistiques.h \
    expLabelling.h \
    wholethymus.h

SOURCES += \
    cytonSimu.cpp \
    distribution.cpp \
    dividingmodelOnePop.cpp \
    dividingmodelTwoPops.cpp \
    main.cpp \
    mainSynchro.cpp \
    mainTwoPops.cpp \
    mainoOldScripts.cpp \
    random.cpp \
    statistiques.cpp \
    wholethymus.cpp \
    explabelling.cpp

