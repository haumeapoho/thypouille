# Meluriana
Simulation software for the manuscript:
**High-resolution mapping of cell-cycle dynamics during steady-state T-cell development and regeneration in vivo** by 
Heike Kunze-Schumacher, Nikita A. Verheyden, Zoe Grewers, Michael Meyer-Hermann, Victor Greiff, Philippe A. Robert, Andreas Krueger
bioRxiv 2023.06.14.544919; doi: https://doi.org/10.1101/2023.06.14.544919


**Meluriana software** 

- simulates a population of cell going through the phases of the cell cycle and following a dual-pulse labelling with EdU, BrdU and DNA measurement at different time-points.
- finds the most likely duration of cell cycle phases to explain experimental dual-pulse datasets and allow to compare cycle heterogeneity hypotheses.
- simulates two conditions (WT and perturbed) and compare scenarios of differences in cell cycle phases to find the most likely explanation for the perturbated labelling dynamics.

Contact for issues / questions: philippe dot robert at ens minus lyon dot org
Please inform us with the above e-mail when creating an issue request in this github, we do not get automatic notification.

![Meluriana overview](Meluriana/Doc/Scheme0.png?raw=true)

## Download / clone

```bash
git clone https://gitlab.com/haumeapoho/thypouille.git
```

## Usage and reproduction of the results

```bash
			 action             population                                         condition            heterogeneous_phases
./Meluriana  gui/fit/identifXX  DN1/DN2/DN3A/DN3B/DN4/preSel/postSel/CD4SP/CD8SP   WT/CTR/IRR/CTR_IRR   [VarG1/VarS/VarG2M/VarAll/NoVar]

allowingQuiescent      dataset                                                      addingBootstrap     differences_CTR/IRR          
[FixedG0/VariableG0]   [6P_D1D2D3/4P_D1/4P_D1D2/4P_D1D2D3/4P_D1D2D3D4/6P_D1D2D3D4]  [none/bootstrap]    [none/diffG1/diffS/diffG2M/diffAll]

Notes: 
- Possible identifiability commands (identifXX=identifG1/identifS/identifG2M/identifQuiescent/identifWG1/identifWS/identifWG2M/identifDeath/identifnDiv
- [] arguments are optional if using Meluriana with the graphical interface (first argument = gui)
- dataset refers to subselection of datapoints. 4P/=6P  4/6 time-points. D1: steady state EdU/BrdU. D2: Return of Post and Middle in G1, Unstained in S and Post in S. 
  D3: steady percents of cells in G1/S/G2M. D4: Avg DNA levels in Pre and Middle
```

# Simulation of one population at a time (one condition: WT, CTR or IRR)
Examples: minimal executions using the graphical interface
```bash
./Meluriana gui DN1 WT
./Meluriana gui DN3A IRR
./Meluriana gui DN3A IRR
```
Starting the interface and running one simulation with certain cell cycle durations should take a few seconds.

Execution giving all parameters for a parameter estimation
```bash
./Meluriana gui DN3B WT VarG1 FixedG0 6P_D1D2D3D4 none 
./Meluriana gui DN3A IRR VarS VariableG0 4P_D1D2 bootstrap
```
Parameter estimation for a population of 10 000 cells takes 5 to 10 hours on a personal computer using one CPU.

Which is identical to the following commands, that will run by themselves in command line without gui (graphical interface)
```bash
./Meluriana fit DN3B WT VarG1 FixedG0 6P_D1D2D3D4 none 
./Meluriana fit DN3A IRR VarS VariableG0 4P_D1D2 bootstrap
```

To explore the likelihood of each phase duration (profile likelyhood). Do not use bootstrap.
```bash
./Meluriana identifS DN3B WT VarG1 FixedG0 6P_D1D2D3D4 none 
```

# Simulation of one population at a time (two conditions: CTR and IRR together)

```bash
./Meluriana gui DN3B CTR_IRR
```

```bash
./Meluriana fit DN3B CTR_IRR VarG1 VariableG0 6P_D1D2D3D4 none diffS
```

# Estimating cell cycle duration on new datasets

0/ Decide a name for your population of interest (for instance _"mypop"_) and the name of your dataset (example : _"Data3timepoints"_) and create a subfolder with your dataset name inside Meluriana/DATA

1/ prepare your data into two new text files (tab-separated) in your dataset subfolder (for instance _"Meluriana/DATA/Data3timepoints/"_) containing the measurements for only one population. One file should contain measurement averages, and another file the measurement standard deviations. See example files inside the dataset subfolder 6TP_Data1Data2Data3Data4, that contains all types of measurements (columns), and see the manuscript tables for the definitions of the columns. 

Make sure that the two files are named with your population name followed by "_WT_POOL_AVG.txt" and "_WT_POOL_STD.txt", (For instance, _Meluriana/DATA/Data3timepoints/mypop_WT_POOL_AVG.txt and Meluriana/DATA/Data3timepoints/mypop_WT_POOL_STD.txt_)

Any file with less columns will also work. Note that the two first numbers in the text files describe the number of lines and columns. Pay attention that percents are written between 0 and 100.


2/ Naming your population and linking your data inside main.cpp

Adding your population name in main.cpp, line 64, for instance if you name it "mypop"
```bash
set<string> populations = {"DN1", "DN2", "DN3A", "DN3B", "DN4", "preSel", "postSel", "CD4SP", "CD8SP", "mypop"};
```
Adding your dataset name in main.cpp, line 68, for instance if you name it "Data3timepoints"
```bash
set<string> datasets = {"6P_D1D2D3", "4P_D1", "4P_D1D2", "4P_D1D2D3", "4P_D1D2D3D4", "6P_D1D2D3D4", "Data3timepoints"};
```

Line 93, add a line to link the dataset name to the proper folder. You can give a shorter shortName for displaying reports on this dataset.
```bash
if(!dataset.compare("Data3timepoints"))   {folderDataset += "Data3timepoints/"; shortName = "Data3timepoints";}
```

3/ Re-compile the program (it is suggested to delete Meluriana to see if compiling regenerated the executable). See the installation paragraph since it depends on the OS. 

Example for linux, you only need to repeat the make step.
```bash
cd Meluriana/
make
```
If using QtCreator, clicking the green arrow does recompile for you. 


Then, you can run all commands as in tis example, with your population and dataset name, and using the WT keyword:
```bash
./Meluriana fit mypop WT VarG1 FixedG0 Data3timepoints none 
```

## Installation

Meluriana was written in C++ using the Moonfit framework for parameter estimation.
- requires **a C++ compiler**
- requires the **Qt framework**

Important to locate data files, please define the working folders of interest manually inside Meluriana/main.cpp, lines 21-22 and inside mainTwoPops.cpp, lines 15-16 and RECOMPILE afterwards (make, see below)

```bash
folder = "/home/... to fill .../Meluriana/";
folderBaseResults = "/home/... tofill ... /Meluriana/Results/";
```

#Installing on linux (simplest):
```bash
#sudo apt-get install git #if you don't have git for downloading the code
git clone https://gitlab.com/haumeapoho/thypouille.git

#sudo apt-get install build-essential  #if you don't have g++ and make 

#to find the available qt packages in your distribution
apt-cache search qtbase
apt-cache search libqt5

#These packages should do the job
sudo apt-get install qtbase5-dev
sudo apt-get install qtcreator
sudo apt-get install libqt5svg5
sudo apt-get install libqt5printsupport5

cd Meluriana/
qmake Meluriana.pro
make
```

#Installing on Windows:

We recommend installing the Qt framework including a C++ compiler if you don't have. Please visit qt.io/download

Compiling 

```bash
cd Meluriana/bin
qmake ../Meluriana.pro	#qmake creates a new Makefile embedding Qt libraries locations
make		#This creates the executable of Meluriana
```

Alternately, if you have installed the Qt framework with qtcreator, 
```bash
qtcreator Meluriana/Meluriana.pro
```
When qtcreator opens, select the C++ compiler, then once the project is opened, select Release instead of debug (bottom left) and use the green arrow to compile and run.
Use projects tab > Desktop > Run > command line arguments to put the desired arguments (without ./Meluriana)

#Installing on MAC:

```bash
#if no recent g++ compiler, use this command (will also install g++)
brew install gcc 
brew install qt
```

Then, from the Meluriana/bin/ folder, 
```bash
qmake ../Meluriana.pro
make
```

Some OS specific points:
Inside Meluriana.pro, your compiler might recognize only -o1 or -O1. 
Linking the libraries might require that you provide their folder. Can either add it in the Absolut.pro linker options, or inside the Makefile manually (but then do not run qmake anymore). 
Depending on your g++ compiler, there might be conflicts between the C++ language of the libraries and the C++ standard libraries provided with the compiler. 
This might be solved by adding "QMAKE_CXXFLAGS += -std=c++14 -std=c++17" inside Meluriana.pro. We didn't add these lines by default in case your C++ compiler doesn't support C++17.

*Meluriana has been run on a HPC cluster with CentOS Linux version 7, and gcc version 4.8.5, Qt version 5.12.5*  

## Detailed explanation 

# File structure

Moonfit/ contains the Moonfit GUI/simulation framework 

Meluriana/ contains the code to simulate an agent-based model of proliferating cells and their labelling, following a program design that Moonfit graphical interface can call. 

CytonSimu/ defines agents (cells), populations (stages) and functions to create new cells or evolve the cells through the cell cycle. This file is named after the Cyton model introduced by Wellard 2011, since we simulate analoguous populations with log-normal residence times. 

ExpLabelling/ defines the setup of a simulation

DividingModel/ defines simulations using CytonSimu as a Moonfit subclass, with inputs = parameters (cycle durations) and output = results of labelling experiment after simulating t=~20 hours.

# Steps of a simulation

The parameters of a simulation define how the cells will behave (cell cycle phase, among others). Labelling is performed in a yes-or-no manner to cells that are in the S phase. The population of cells is monitored like in the experimental settings. Further, a populations has entry from previous populations and exit after a fixed number of divisions. 

![Meluriana overview](Meluriana/Doc/Scheme1.png?raw=true)

This allows to simulate a flow cytometry experiment in silico:

![Meluriana overview](Meluriana/Doc/Scheme2.png?raw=true)

By chosing standard deviations cycle phase durations, one can simulate different scenario of cycle heterogeneity

![Meluriana overview](Meluriana/Doc/Scheme3.png?raw=true)

It is possible to compare such different scenario compared to experimental data

![Meluriana overview](Meluriana/Doc/Scheme4.png?raw=true)
 
Finally, by doing parameter optimization, a huge number of possible phase durations is tested and the scenario best fitting to the data can be compared depending on the heterogeneity scenario

![Meluriana overview](Meluriana/Doc/Scheme5.png?raw=true)

By doing parameter Identifiability for one parameter (for instance for the duration of S phase), each possible value of S phase is fixed and other parameter are estimated. If the cost does not depend on the fixed S value, it means S duration is not identifiable. However, if S duration shows a minimum cost, it means only this minimum can explain best the data.

![Meluriana overview](Meluriana/Doc/Scheme6.png?raw=true)

## Documentation / Reference / Reproducing

Robert, Philippe A., Henrik Jönsson, and Michael Meyer-Hermann. 2018. “MoonFit, a Minimal Interface for Fitting ODE Dynamical Models, Bridging Simulation by Experimentalists and Customization by C++ Programmers.” bioRxiv. https://doi.org/10.1101/281188.

Wellard, C., Markham, J. F., Hawkins, E. D., & Hodgkin, P. D. (2011). The cyton model for lymphocyte proliferation and differentiation. In Mathematical Models and Immune Cell Biology (pp. 107-120). New York, NY: Springer New York.
