// -------------------- Script that uses Moonfit to perform simulations / optimizations ----------------------------

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <set>
using namespace std;

#include "../Moonfit/moonfit.h"
#include "random.h"
#include "bootstrap.h"

#include "mainTwoPops.h"

#include "dividingmodelTwoPops.h"
#include "dividingmodelOnePop.h"
#include "expLabelling.h"


static string folder = "C:/Users/pprobert/Desktop/fromHP/Softwares/NewArchaeropteryx/Sources/Meluriana/";
static string folderBaseResults = "C:/Users/pprobert/Desktop/fromHP/Softwares/NewArchaeropteryx/Sources/Meluriana/Results/";

// How many mouse we draw for each time-point
#define NBootstrap 5

// Number of replicates for fitting or identifiability
#define nRep 10

// Number of tested values within the boundaries (in log 2 scale)
#define nbPointsIdentifiability 12


int main(int argc, char *argv[]){

    if(!dirExists(folder)) cerr << "Could not find the project folder for finding data and configurations: " << folder << endl;
    if(!dirExists(folderBaseResults.c_str())) createFolder(folderBaseResults);

    // Giving help if called witn less than 2 arguments
    if(argc <= 3){
        cout << "// expected syntax (arguments in [] are optional, first value is the default):\n"
                "//              action             population                                        condition                    cycle_properties"
                "// ./Meluriana  gui/fit/identifXX  DN1/DN2/DN3A/DN3B/DN4/preSel/postSel/CD4SP/CD8SP  WT/KO/CTR/IRR/WT_KO/CTR_IRR  [VarG1/VarS/VarG2M/VarAll/NoVar]"
                "//\n"
                "//              allowingQuiescent      dataset                                                      addingBootstrap     differences_WT/KO_or_CTR/IRR          parameterSet\n"
                "//              [FixedG0/VariableG0]   [6P_D1D2D3/4P_D1/4P_D1D2/4P_D1D2D3/4P_D1D2D3D4/6P_D1D2D3D4]  [none/bootstrap]    [none/diffG1/diffS/diffG2M/diffAll]   [textfile]\n"
                "//\n"
                "// Possible identifiability commands (identifXX=identifG1/identifS/identifG2M/identifQuiescent/identifWG1/identifWS/identifWG2M/identifDeath/identifnDiv\n"
                "// The parameter set(s) file should be in a History format: nrCols<tab>nrLines<newLine>data, without headers, first column being the cost";
        return 0;
    }

    // Filtering input in a list of authorized options
    string command           = string(argv[1]);
    string population        = string(argv[2]);
    string condition         = (argc >= 4) ? string(argv[3]) : "WT";
    string cycle             = (argc >= 5) ? string(argv[4]) : "VarG1";
    string allowingQuiescent = (argc >= 6) ? string(argv[5]) : "FixedG0";
    string dataset           = (argc >= 7) ? string(argv[6]) : "6P_D1D2D3";
    string addingBootstrap   = (argc >= 8) ? string(argv[7]) : "none";
    string diffHyp           = (argc >= 9) ? string(argv[8]) : "none";
    string parameterSets     = (argc >= 10) ? string(argv[9]) : "";

    set<string> commands = {"gui", "fit", "twoPops", "twoPopsFit", "identifG1", "identifS", "identifG2M", "identifQuiescent", "identifWG1", "identifWS", "identifWG2M", "identifDeath", "identifnDiv"};
    set<string> populations = {"DN1", "DN2", "DN3A", "DN3B", "DN4", "preSel", "postSel", "CD4SP", "CD8SP"};
    set<string> conditions = {"WT", "KO", "CTR", "IRR", "WT_KO", "CTR_IRR"};
    set<string> cycles = {"VarG1", "VarS", "VarG2M", "VarAll", "NoVar"};
    set<string> allowingQuiescents = {"FixedG0", "VariableG0"};
    set<string> datasets = {"6P_D1D2D3", "4P_D1", "4P_D1D2", "4P_D1D2D3", "4P_D1D2D3D4", "6P_D1D2D3D4"};
    set<string> addingBootstraps = {"none", "bootstrap"};
    set<string> diffHyps = {"none", "diffG1", "diffS", "diffG2M", "diffAll", "diffQuies", "diffAllAndLongG1", "diffG1AndLongG1"};

    bool success = true;
    if(commands.find(command)                  == commands.end()) {success = false; cerr << "option " << command << " not understood" << endl;}
    if(populations.find(population)            == populations.end()) {success = false; cerr << "option " << population << " not understood" << endl;}
    if(conditions.find(condition)              == conditions.end()) {success = false; cerr << "option " << condition << " not understood" << endl;}
    if(cycles.find(cycle)                      == cycles.end()) {success = false; cerr << "option " << cycle << " not understood" << endl;}
    if(allowingQuiescents.find(allowingQuiescent) == allowingQuiescents.end()) {success = false; cerr << "option " << allowingQuiescent << " not understood" << endl;}
    if(datasets.find(dataset)                  == datasets.end()) {success = false; cerr << "option " << dataset << " not understood" << endl;}
    if(addingBootstraps.find(addingBootstrap)  == addingBootstraps.end()) {success = false; cerr << "option " << addingBootstrap << " not understood" << endl;}
    if(diffHyps.find(diffHyp)                  == diffHyps.end()) {success = false; cerr << "option " << diffHyp << " not understood" << endl;}
    if(!success) {return 0;}

    if(!command.compare("twoPops") || !command.compare("twoPopsFit")){
        mainTwoPops(argc, argv);
    }


    // Chosing the data files based on input
    string folderDataset = folder + "DATA/";
    string shortName = "";
    if(!dataset.compare("6P_D1D2D3"))     {folderDataset += "6TP_Data1Data2Data3/"; shortName = "6+D123";}
    if(!dataset.compare("4P_D1"))         {folderDataset += "4TP_Data1_EduBrdu/"; shortName = "4+D1";}
    if(!dataset.compare("4P_D1D2"))       {folderDataset += "4TP_Data1Data2_EduBrdu_Return/";  shortName = "4+D12";}
    if(!dataset.compare("4P_D1D2D3"))     {folderDataset += "4TP_Data1Data2Data3_EduBrdu_Return_Steady/";  shortName = "4+D123";}
    if(!dataset.compare("4P_D1D2D3D4"))   {folderDataset += "4TP_Data1Data2Data3Data4_EduBrdu_Return_Steady_DNA/";  shortName = "4+D1234";}
    if(!dataset.compare("6P_D1D2D3D4"))   {folderDataset += "6TP_Data1Data2Data3Data4/"; shortName = "6+D1234";}

    string dataFileAVG = folderDataset;
    string dataFileSTD = folderDataset;
    string dataFileAVG_REF = folderDataset;
    string dataFileSTD_REF = folderDataset;
    bool doubleSimulation = false;
    if(!condition.compare("WT")){
        dataFileAVG += population + "_WT_POOL_AVG.txt";
        dataFileSTD += population + "_WT_POOL_STD.txt";
    }
    if(!condition.compare("KO")){
        dataFileAVG += population + "_KO_POOL_AVG.txt";
        dataFileSTD += population + "_KO_POOL_STD.txt";
    }
    if(!condition.compare("CTR")){
        dataFileAVG += "Reg" + population + "_WT_AVG.txt";
        dataFileSTD += "Reg" + population + "_WT_STD.txt";
    }
    if(!condition.compare("IRR")){
        dataFileAVG += "Reg" + population + "_IRR_AVG.txt";
        dataFileSTD += "Reg" + population + "_IRR_STD.txt";
    }
    if(!condition.compare("WT_KO")){
        doubleSimulation = true;
        dataFileAVG_REF += population + "_WT_POOL_AVG.txt";
        dataFileSTD_REF += population + "_WT_POOL_STD.txt";
        dataFileAVG += population + "_KO_POOL_AVG.txt";
        dataFileSTD += population + "_KO_POOL_STD.txt";
    }
    if(!condition.compare("CTR_IRR")){
        doubleSimulation = true;
        dataFileAVG_REF += "Reg" + population + "_WT_AVG.txt";
        dataFileSTD_REF += "Reg" + population + "_WT_STD.txt";
        dataFileAVG += "Reg" + population + "_IRR_AVG.txt";
        dataFileSTD += "Reg" + population + "_IRR_STD.txt";
    }

    // Loading the data and creating the models
    TableCourse* Data_AVG = new TableCourse(dataFileAVG);
    cout << Data_AVG->print() << endl;
    TableCourse* Data_STD = new TableCourse(dataFileSTD);
    cout << Data_STD->print() << endl;

    if(!addingBootstrap.compare("bootstrap")){
        TableCourse Bootstrapped = bootstrap::randomize(*Data_AVG, *Data_STD, NBootstrap);
        Data_AVG = new TableCourse(Bootstrapped);
        cout << "Data has been boostrapped for average, keeping the standard deviation as in data" << endl;
        cout << Data_AVG->print() << endl;
    }

    Model* currentModel = new dividingModel();
    Experiment* currentExperiment = nullptr;
    string configFile = "";
    int IDcombination = 0;

    // Two possible fitting experiments:
    // First case: only simulates one population at a time

    if(doubleSimulation == false){

        currentExperiment = new simplestExpDoubleLabel(currentModel);
        currentExperiment->giveData(Data_AVG, 0, Data_STD); // 0 is the index of the experiment
        currentExperiment->loadEvaluators();
        cout << "   -> Using model : " << currentModel->name << " with experiment " << currentExperiment->Identification << "\n";

        // Creating a code for these settings, and creating an output result folder
        currentExperiment->Identification = population + "+" + condition + "+" + cycle + "+" + shortName + "+" + allowingQuiescent + "+" + addingBootstrap + "+" + diffHyp;
        createFolder(folderBaseResults + currentExperiment->Identification);
        cout << "Will save results in " << folderBaseResults + currentExperiment->Identification << endl;

        // Now deciding the settings for optimization based on the options (which parameters to optimize)
        configFile = folder + string("GeneralConfigAllOptions.txt");

        if(!cycle.compare("VarG1")) {IDcombination = 1;}
        if(!cycle.compare("VarS")) {IDcombination = 2;}
        if(!cycle.compare("VarG2M")) {IDcombination = 3;}
        if(!cycle.compare("VarAll")) {IDcombination = 4;}
        if(!cycle.compare("NoVar")) {IDcombination = 0;}
        if(!allowingQuiescent.compare("VariableG0")){IDcombination += 5;}

    // second option: simulates WT and KO together, or CTR and IRR together.
    } else {

        // reads the second set of data (WT or CTR)
        TableCourse* Data_AVG_REF = new TableCourse(dataFileAVG_REF);
        cout << Data_AVG_REF->print() << endl;
        TableCourse* Data_STD_REF = new TableCourse(dataFileSTD_REF);
        cout << Data_STD_REF->print() << endl;

        if(!addingBootstrap.compare("bootstrap")){
            TableCourse Bootstrapped_REF = bootstrap::randomize(*Data_AVG_REF, *Data_STD_REF, NBootstrap);
            Data_AVG_REF = new TableCourse(Bootstrapped_REF);
            cout << "Data has been boostrapped for average, keeping the standard deviation as in data" << endl;
            cout << Data_AVG_REF->print() << endl;
        }

        currentExperiment = new expDoubleLabelWT_KO(currentModel);
        currentExperiment->giveData(Data_AVG_REF, expDoubleLabelWT_KO::WT, Data_STD_REF); // 0 is the index of the WT experiment
        currentExperiment->giveData(Data_AVG, expDoubleLabelWT_KO::DKO, Data_STD); // 1 is the index of the KO or IRR experiment
        currentExperiment->loadEvaluators();

        // naming the perturbated condition
        if(!condition.compare("WT_KO")){currentExperiment->names_exp[expDoubleLabelWT_KO::DKO] = "DKO";}
        if(!condition.compare("CTR_IRR")){currentExperiment->names_exp[expDoubleLabelWT_KO::DKO] = "IRR";}

        // Now deciding the settings for optimization based on the options (which parameters to optimize)
        configFile = folder + string("GeneralConfigTwoPopulations.txt");

        if(!cycle.compare("VarG1")) {IDcombination = 1;}
        if(!cycle.compare("VarS")) {IDcombination = 2;}
        if(!cycle.compare("VarG2M")) {IDcombination = 3;}
        if(!cycle.compare("VarAll")) {IDcombination = 4;}
        if(!cycle.compare("NoVar")) {IDcombination = 0;}

        if(!allowingQuiescent.compare("VariableG0")){IDcombination += 5;}

        if(!diffHyp.compare("none")) {IDcombination += 0;}
        if(!diffHyp.compare("diffG1")) {IDcombination += 10;}
        if(!diffHyp.compare("diffS")) {IDcombination += 20;}
        if(!diffHyp.compare("diffG2M")) {IDcombination += 30;}
        if(!diffHyp.compare("diffAll")) {IDcombination += 40;}
        if(!diffHyp.compare("diffQuies")) {IDcombination += 50;}
        if(!diffHyp.compare("diffAllAndLongG1")) {IDcombination += 60;}
        if(!diffHyp.compare("diffG1AndLongG1")) {IDcombination += 70;}
    }

    cout << "   -> Using model : " << currentModel->name << " with experiment " << currentExperiment->Identification << "\n";
    cout << "   -> Using configuration file: " << configFile << "\n   -> with combination " << IDcombination << endl;

    // Creating a code for these settings, and creating an output result folder
    currentExperiment->Identification = population + "+" + condition + "+" + cycle + "+" + shortName + "+" + allowingQuiescent + "+" + addingBootstrap;
    createFolder(folderBaseResults + currentExperiment->Identification);
    cout << "Will save results in " << folderBaseResults + currentExperiment->Identification << endl;


    // Deciding the type of cost
    setTypeCost(SQUARE_COST);
    //    SQUARE_COST: "RSS"
    //    SQUARE_COST_STD: "RSS + StdDev"
    //    LOG_COST: "Log"
    //    PROPORTION_COST: "Ratios"

    setTypeNorm(NORM_AVERAGE);
    //    NO_NORM: " (No Norm)"
    //    NORM_AVERAGE: " Norm/Avg vars"
    //    NORM_NB_PTS: " Norm/Nb Points"
    //    NORM_AVG_AND_NB_PTS: " Norm/Avg and Nb Pts "
    //    NORM_MAX: " Norm/Max vars"
    //    NORM_MAX_AND_NB_PTS: " Norm/Max and Nb Pts"

    // Set the costs before calling the graphical interface, because it will not update once it's called
    // (although you would change the cost, the GUI would not realize and keep showing the old one)

    if(!command.compare("gui")){

        #ifndef WITHOUT_QT
        QApplication b(argc, argv);             // Starts the Qt application

        cout << "Launching Graphical Interface ..." << endl;
        // Step 7a: Launch the graphical interface from an experiment (containing the model inside)
        simuWin* p = new simuWin(currentExperiment);

        // Step 7b: optionally give a config file
        p->loadConfig(configFile);
        if(parameterSets.size() > 0){p->loadHistory(QString(parameterSets.c_str()));}

        //int nbCombinations = p->nbCombs; // cold check that this is 10 or 50

        // Step 7c: show the graphical interface!
        p->show();

        p->useComb(IDcombination);


        // Step 7d: leave the control to Qt instead of finishing the program
        b.exec();

        #else
        cerr << "You have compiled Meluriana without the graphical libraries, so the 'gui' option doesn't run. Check that #WITHOUT_QT is not defined, then recompile" << endl;
        #endif
    }

    if(!command.compare("fit")){

        currentExperiment->m->setBaseParameters();
        manageSims* MSim = new manageSims(currentExperiment);

        MSim->loadConfig(configFile);
        int nbCombinations = MSim->nbCombs;
        if(nbCombinations != 10){
            cerr << "The default configuration should have 10 combinations, should be GeneralConfigAllOptions.txt" << endl;
        }

        string optimizerName = folderBaseResults + currentExperiment->Identification + "/" + "Opt.txt";
        string contentOpt = MSim->motherCreateOptimizerFile(IDcombination,optFileHeader(Genetic25k));
        ofstream optim(optimizerName.c_str());
        optim << contentOpt;
        optim.close();
        cout << "The selected options for fitting are:" << endl;
        cout << contentOpt << endl;

        for(int j = 0; j < nRep; ++j){
            stringstream codeSim; codeSim << "Comb" << IDcombination << "_Rep" << j;
            createFolder(folderBaseResults + currentExperiment->Identification + "/" + codeSim.str());

            cout << codeSim.str() << endl;
            MSim->resetParamSetFromConfig(configFile);
            MSim->initialize();
            MSim->motherOptimize(optimizerName, 10000);
            MSim->saveHistory(folderBaseResults + currentExperiment->Identification + "/" + codeSim.str() + "/TestHistory.txt");                         // SAVES all the best parameter sets. by default, 10 000, can be modified by             msi->history.resize(max_nb_sets_to_record);
            MSim->useParamSetFromHistory(0);                                                     // takes the first set of parameters (the best), also possible to use msi->useParamSetFromHistory(0, i); for overriding only parameters from this combination,
            MSim->simulate();
            cout << "Sim OK, cost " << MSim->getCost() << endl;
            MSim->makeTextReportParamSet(folderBaseResults + currentExperiment->Identification + "/" + codeSim.str() + "/", IDcombination);
        }
    }

    if(!command.substr(0, 7).compare("identif")){

        int idParam = dividingModel::avgG1;
        if(!command.compare("identifG1")){idParam = dividingModel::avgG1;}
        if(!command.compare("identifS")){idParam = dividingModel::avgS;}
        if(!command.compare("identifG2M")){idParam = dividingModel::avgG2M;}
        if(!command.compare("identifQuiescent")){idParam = dividingModel::percentQuiescentG0;}
        if(!command.compare("identifWG1")){idParam = dividingModel::widthG1;}
        if(!command.compare("identifWS")){idParam = dividingModel::widthS;}
        if(!command.compare("identifWG2M")){idParam = dividingModel::widthG2M;}
        if(!command.compare("identifDeath")){idParam = dividingModel::deathRate;}
        if(!command.compare("identifnDiv")){idParam = dividingModel::paramDiff;}

        stringstream codeIdentif; codeIdentif << folderBaseResults << currentExperiment->Identification << "/" << "_Identif" << idParam << "/";
        createFolder(codeIdentif.str());

        currentExperiment->m->setBaseParameters();
        manageSims* MSim = new manageSims(currentExperiment);

        MSim->loadConfig(configFile);
        int nbCombinations = MSim->nbCombs;
        if(nbCombinations != 10){
            cerr << "The default configuration should have 10 combinations, should be GeneralConfigAllOptions.txt" << endl;
        }

        MSim->resetParamSetFromConfig(configFile);
        vector<double> paramsRef = currentModel->getParameters();
        MSim->prepareOptFilesForIdentifibiality(codeIdentif.str(),idParam, IDcombination, optFileHeader(GeneticFast));
        MSim->motherIdentifiability(paramsRef, idParam, nbPointsIdentifiability);
        MSim->makeTextReportParamSet(codeIdentif.str(), IDcombination);
    }

    cout << " ===== The End! ==== " << endl;
    return 0;
}



