#include "mainwindow.h"
#include "ui_mainwindow.h"

//#include <QCoreApplication>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mainSimu = new multiPopSimulation();
    Board = new MainBoard(mainSimu, ui->tabBoard);
    CWDN1 = new CytonWidget(mainSimu->getStage(DN1), ui->tabDN1);
    CWDN2 = new CytonWidget(mainSimu->getStage(DN2), ui->tabDN2);
    CWDN3 = new CytonWidget(mainSimu->getStage(DN3), ui->tabDN3);
    CWDN4 = new CytonWidget(mainSimu->getStage(DN4), ui->tabDN4);
    CWeDP = new CytonWidget(mainSimu->getStage(eDP), ui->tabeDP);
    CWlDP = new CytonWidget(mainSimu->getStage(lDP), ui->tablDP);
    CWSP4 = new CytonWidget(mainSimu->getStage(SP4), ui->tabSP4);
    CWSP8 = new CytonWidget(mainSimu->getStage(SP8), ui->tabSP8);
    CWTreg= new CytonWidget(mainSimu->getStage(Tregs), ui->tabTregs);
    widgets = {CWDN1, CWDN2, CWDN3, CWDN4, CWeDP, CWlDP,CWSP4, CWSP8, CWTreg}; // size NbStages

    simRunning = false;
    QObject::connect(ui->pushButtonRun, SIGNAL(released()), this, SLOT(runSim()));

    //runSim();
}

MainWindow::~MainWindow()
{
    delete Board;
    delete CWDN1;
    delete CWDN2;
    delete CWDN3;
    delete CWDN4;
    delete CWeDP;
    delete CWlDP;
    delete CWSP4;
    delete CWSP8;
    delete CWTreg;
}

void MainWindow::runSim(){
    if(!simRunning){
        mainSimu->reset();
        // Starts simulation
        ui->pushButtonRun->setText(QString("Stop"));

        Board->applyParamsToSimu();
        for(int iSt = 0; iSt < NbStages-1; ++iSt){
            widgets[iSt]->applyParamsToStage();
        }

        masterObserver* MO = new masterObserver("");
        observerOneSim* MOS = new observerOneSim("");
        mainSimu->initialize(MOS, MO);
        simulationLoop();
    } else {
        // Stops simulation
        ui->pushButtonRun->setText(QString("Run"));
        simRunning = false;
    }
    Board->enable(!simRunning);
    for(int i = 0; i < NbStages; ++i){
        widgets[i]->enable(!simRunning);
    }

}

void MainWindow::simulationLoop(){
    if(simRunning == true) return;
    else simRunning=true;
    while((simRunning == true) && (mainSimu->timeStep())){
        Board->display(mainSimu->currentObserverAllPops);

        // This should be done more properly, not in the interface
        for(int i = 0; i < (int) mainSimu->AnalysisPoints.size(); ++i){
            if(fabs(mainSimu->time - mainSimu->AnalysisPoints[i]) < 0.5*mainSimu->dt){ // either before or after so 0.5
                cout  << "Performing analysis for time " << mainSimu->time << endl;
                for(int iSt = 0; iSt < NbStages-1; ++iSt){
                    cout << "Stage " << nameStage(iSt) << endl;
                    widgets[iSt]->analyze(mainSimu->AnalysisPoints[i]);
                }
            }
        }
        QCoreApplication::processEvents();
    }
    mainSimu->finalize();
    simRunning = false;
}
