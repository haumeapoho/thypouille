#include "dividingmodelOnePop.h"
#include "wholethymus.h"

dividingModel::dividingModel() : modelAgentBased(NbVariables, NbParameters), background(0) {

    currentSim = nullptr; // gets deleted and a new one created at each simulation.
    currentObs = new observerOneSim(); // can be reused

    name = string("Generic agent-based model BRDU/EDU");

    // Now we are in hours
    dt = 0.02; //0.001; // initial time step -> then it is adaptive around this value
    print_every_dt = 0.1; //every how many seconds it is plotting

    // Name of variables
    names[nCells] = "nCells";
    names[doseEDU] = "doseEDU";
    names[doseBRDU] = "doseBRDU";
    names[popSize] = "popSize";
    names[prcBRDU] = "prcBRDU+";
    names[prcEDU] = "prcEDU+";
    names[prcEDUpBRDUp] = "prcEDU+BRDU+";
    names[prcEDUpBRDUn] = "prcEDU+BRDU-";
    names[prcEDUnBRDUp] = "prcEDU-BRDU+";
    names[prcEDUnBRDUn] = "prcEDU-BRDU-";
    names[avgGen] = "avgGen";
    names[nbG1] = "nbG1";
    names[nbS] = "nbS";
    names[nbG2M] = "nbG2M";
    names[nbG0] = "nbG0";
    names[nbNewcomers] = "nbNewcomers";
    names[flowOut] = "flowOut";
    names[avgGenOut] = "avgGenOut";
    names[gen0] = "gen0";
    names[gen1] = "gen1";
    names[gen2] = "gen2";
    names[gen3] = "gen3";
    names[gen4] = "gen4";
    names[gen5] = "gen5";
    names[gen6] = "gen6";
    names[gen7] = "gen7";
    names[gen8] = "gen8";
    names[gen9] = "gen9";
    names[gen10] = "gen10";
    names[apopt] = "apopt";
    names[AvgDNAPre] = "AvgDNAPre";
    names[AvgDNAMiddle] = "AvgDNAMiddle";
    names[AvgDNAPost] = "AvgDNAPost";
    names[PreInG1] = "PreInG1";
    names[MiddleInG1] = "MiddleInG1";
    names[PostInG1] = "PostInG1";
    names[PreInS] = "PreInS";
    names[MiddleInS] = "MiddleInS";
    names[PostInS] = "PostInS";
    names[PreInG2M] = "PreInG2M";
    names[MiddleInG2M] = "MiddleInG2M";
    names[PostInG2M] = "PostInG2M";
    names[AvgDNABRDU] = "AvgDNABRDU";
    names[UnstInS] = "UnstInS"; //names[AvgDNAUNeg] = "AvgDNAUNeg";
    names[TotInG1] = "TotInG1";
    names[TotInS] = "TotInS";
    names[TotInG2] = "TotInG2";
    names[nbEarly] = "nbEarly";
    names[nbMiddle] = "nbMiddle";
    names[nbPost] = "nbPost";
    names[nCells] = "nCells";


    // the names of variables that can be accessed by outside (global name-space)
    extNames[prcBRDU] = "prcBRDUpos";
    extNames[prcEDU] = "prcEDUpos";
    extNames[prcEDUpBRDUp] = "prcEDUposBRDUpos";
    extNames[prcEDUpBRDUn] = "prcEDUposBRDUneg";
    extNames[prcEDUnBRDUp] = "prcEDUnegBRDUpos";
    extNames[prcEDUnBRDUn] = "prcEDUnegBRDUneg";
    extNames[AvgDNAPre] = "AvgDNAPre";
    extNames[AvgDNAMiddle] = "AvgDNAMiddle";
    extNames[AvgDNAPost] = "AvgDNAPost";
    extNames[PreInG1] = "PreInG1";
    extNames[MiddleInG1] = "MiddleInG1";
    extNames[PostInG1] = "PostInG1";
    extNames[PreInS] = "PreInS";
    extNames[MiddleInS] = "MiddleInS";
    extNames[PostInS] = "PostInS";
    extNames[PreInG2M] = "PreInG2M";
    extNames[MiddleInG2M] = "MiddleInG2M";
    extNames[PostInG2M] = "PostInG2M";
    extNames[AvgDNABRDU] = "AvgDNABRDU";
    extNames[UnstInS] = "UnstInS";
    extNames[TotInG1] = "percentG0G1";
    extNames[TotInS] = "percentS";
    extNames[TotInG2] = "percentG2M";
    extNames[nCells] = "nCells";
    // to extend if we have data

    // Name of parameters
    paramNames[aimedPopSize] = "aimedPopSize";
    paramNames[avgG1] = "avgG1";
    paramNames[avgS] = "avgS";
    paramNames[avgG2M] = "avgG2M";
    paramNames[widthG1] = "widthG1";
    paramNames[widthS] = "widthS";
    paramNames[widthG2M] = "widthG2M";
    paramNames[avgG0] = "avgG0";
    paramNames[widthOrPercentLongG0] = "widthOrPercentLongG0";
    paramNames[percentQuiescentG0] = "percentQuiescentG0";
    paramNames[paramDiff] = "paramDiff";
    paramNames[inflowRate] = "inflowRate";
    paramNames[thresholdPos] = "thresholdPos";
    paramNames[preSimTime] = "preSimTime";
    paramNames[timeBRDU] = "timeBRDU";
    paramNames[durationBRDU] = "durationBRDU";
    paramNames[timeEDU] = "timeEDU";
    paramNames[durationEDU] = "durationEDU";
    paramNames[timeSimulation] = "timeSimulation";
    paramNames[apoptoticTime] = "apoptoticTime";
    paramNames[typePopulation] = "pop:lam0,nDiv1,rate2,+3LogNorm";
    paramNames[deathRate] = "deathRate";
    paramNames[modulatedG1KO] = "modulatedG1KO";
    paramNames[modulatedSKO] = "modulatedSKO";
    paramNames[modulatedG2MKO] = "modulatedG2MKO";
    paramNames[modulatedWidthG1KO] = "modulatedWidthG1KO";
    paramNames[modulatedWidthSKO] = "modulatedWidthSKO";
    paramNames[modulatedQuiescentKO] = "modulatedQuiescentKO";


    // default large boundaries for the parameters
    paramLowBounds[aimedPopSize] = 10;
    paramLowBounds[avgG1] = 1;
    paramLowBounds[avgS] = 1;
    paramLowBounds[avgG2M] = 1;
    paramLowBounds[widthG1] = 0.1;
    paramLowBounds[widthS] = 0.1;
    paramLowBounds[widthG2M] = 0.1;
    paramLowBounds[avgG0] = 1;
    paramLowBounds[widthOrPercentLongG0] = 0.01;
    paramLowBounds[percentQuiescentG0] = 0;
    paramLowBounds[paramDiff] = 0.001;
    paramLowBounds[inflowRate] = 0.001;
    paramLowBounds[thresholdPos] = 0.01;
    paramLowBounds[preSimTime] = 0.1;
    paramLowBounds[timeBRDU] = 0;
    paramLowBounds[durationBRDU] = 0.01;
    paramLowBounds[timeEDU] = 0;
    paramLowBounds[durationEDU] = 0.01;
    paramLowBounds[timeSimulation] = 3;
    paramLowBounds[apoptoticTime] = 0.01;
    paramLowBounds[typePopulation] = 0;
    paramLowBounds[deathRate] = 0.05;
    paramLowBounds[modulatedG1KO] = 0.05;
    paramLowBounds[modulatedSKO] = 0.05;
    paramLowBounds[modulatedG2MKO] = 0.05;
    paramLowBounds[modulatedWidthG1KO] = 0.05;
    paramLowBounds[modulatedWidthSKO] = 0.05;
    paramLowBounds[modulatedQuiescentKO] = 0.05;

    paramUpBounds[aimedPopSize] = 1000000;
    paramUpBounds[avgG1] = 20;
    paramUpBounds[avgS] = 20;
    paramUpBounds[avgG2M] = 20;
    paramUpBounds[widthG1] = 10;
    paramUpBounds[widthS] = 10;
    paramUpBounds[widthG2M] = 10;
    paramUpBounds[avgG0] = 60;
    paramUpBounds[widthOrPercentLongG0] = 1;
    paramUpBounds[percentQuiescentG0] = 1;
    paramUpBounds[paramDiff] = 1;
    paramUpBounds[inflowRate] = 10;
    paramUpBounds[thresholdPos] = 0.5;
    paramUpBounds[preSimTime] = 200;
    paramUpBounds[timeBRDU] = 0;
    paramUpBounds[durationBRDU] = 1;
    paramUpBounds[timeEDU] = 1;
    paramUpBounds[durationEDU] = 1;
    paramUpBounds[timeSimulation] = 100;
    paramUpBounds[apoptoticTime] = 24;
    paramUpBounds[typePopulation] = 3;
    paramUpBounds[deathRate] = 20;
    paramUpBounds[modulatedG1KO] = 20;
    paramUpBounds[modulatedSKO] = 20;
    paramUpBounds[modulatedG2MKO] = 20;
    paramUpBounds[modulatedWidthG1KO] = 20;
    paramUpBounds[modulatedWidthSKO] = 20;
    paramUpBounds[modulatedQuiescentKO] = 1.0;
}

void dividingModel::setBaseParameters(){
    background = 0;
    params.clear();     // to make sure they are all put to zero
    params.resize(NbParameters, 0.0);

    params[aimedPopSize] = 1000;
    params[avgG1] = 5;
    params[avgS] = 5;
    params[avgG2M] = 2;
    params[widthG1] = 1;
    params[widthS] = 1;
    params[widthG2M] = 0.5;
    params[avgG0] = 5;
    params[widthOrPercentLongG0] = 0.2;
    params[percentQuiescentG0] = 0.05;
    params[paramDiff] = 5.3;
    params[inflowRate] = 10;
    params[thresholdPos] = 0.1;
    params[preSimTime] = 0;
    params[timeBRDU] = 0;
    params[durationBRDU] = 1;
    params[timeEDU] = 1;
    params[durationEDU] = 1;
    params[timeSimulation] = 200;
    params[apoptoticTime] = 0.1;
    params[typePopulation] = 0;
    params[deathRate] = 1.0;
    params[modulatedG1KO] = 1.0;
    params[modulatedSKO] = 1.0;
    params[modulatedG2MKO] = 1.0;
    params[modulatedWidthG1KO] = 1.0;
    params[modulatedWidthSKO] = 1.0;
    params[modulatedQuiescentKO] = 0.0;

    setBaseParametersDone();
}

void dividingModel::finalize(){

}

static int cptErrors = 0;
void dividingModel::initialise(long long _background){ // don't touch to parameters !
    params[timeSimulation] = 26; ///to be removed
    background = _background;

    val.clear();    val.resize(NbVariables, 0.0);
    init.clear();   init.resize(NbVariables, 0.0);

    #ifdef SIMULATE_ALL_THYMUS
    if(!currentSim) currentSim = new multiPopSimulation();
    #else
    if(!currentSim) currentSim = new onePopulation();   // always reuse the existe simulation
    #endif

    // a onePopulation has the following parameters to define, setting up the global simuation (not the cell behavior directly):
    // note: the parameters are given for the reference simulation that will be WT. Therefore, the same parameters will apply to DKO except those
    //       that are called 'modulated', so the population size of the DKO might be different than WT, this is normal, and will be part of the prediction.

    // Global simulation parameters, inside the onePopulation class
    currentSim->dt = dt;
    currentSim->preSimTime = params[preSimTime];
    currentSim->timeSimulation = params[timeSimulation];
    currentSim->timeBRDU = params[timeBRDU];
    currentSim->durationBRDU = params[durationBRDU];
    currentSim->timeEDU = params[timeEDU];
    currentSim->durationEDU = params[durationEDU];
    currentSim->thresholdEDU = params[thresholdPos];
    currentSim->thresholdBRDU = params[thresholdPos];
    // currentSim->inflowRate = params[inflowRate]; // this parameter will be derived from other parameters to ensure population size at steady state.

    currentSim->initialNumbers = static_cast<int>(params[aimedPopSize]);
    currentSim->scalingRatio = 1.0;         // this options allows to simulate N-fold less cells but to show results as N-fold more.

    // to derive from steady state
    currentSim->synchronizeInflow = true;     // true means newcoming cells enter at G0.
                                              // for the cells quiescent at G0, we do not add new cells, maybe we should not make them exit... difficult

    // Now, we define the values specific for the population of interest.
    // The structure of the populations and type of differentiation is defined into cytonsimu().
    // please just change parameter values here, not structure.
    stage* cs = currentSim->pop;

    if(!cs) cerr << "ERR: the stage has empty field pop)" << endl;

    // types are defined in distribution.h
    int typeDistributions = Normal;
    bool onlyTotalCycleIsVariable = false;

    // laminarFiniteTime=0, finiteNrDiv=1, constantDiffRate=2
    int choicePop = static_cast<int>(params[typePopulation] + 0.01);
    switch(choicePop){
        case 0: case 3: case 6: case 9:    {cs->typeDifferentiation = stage::laminarFiniteTime; break;}
        case 1: case 4: case 7: case 10:   {cs->typeDifferentiation = stage::finiteNrDiv;  break;}
        case 2: case 5: case 8: case 11:   {cs->typeDifferentiation = stage::constantDiffRate; break;}
    }
    switch(choicePop){
        case 0: case 1: case 2: case 6: case 7: case 8:      {typeDistributions = Normal; break;}
        case 3: case 4: case 5: case 9: case 10: case 11:    {typeDistributions = LogNormal; break;}
   }
    switch(choicePop){
        case 0: case 1: case 2: case 3: case 4: case 5:      {onlyTotalCycleIsVariable = false; break;}
        case 6: case 7: case 8:case  9: case 10: case 11:    {onlyTotalCycleIsVariable = true; break;}
   }

    // If you wish to use a delay in the DNA incorporation curve. Might be better to define a new parameter for it.
    // thymocyte::testDelay = params[widthG2M];

    cs->paramDiff = params[paramDiff];

    // this parameter is not used so far. Law* distribTot;
    cs->waitEndMtoDifferentiate = true;
    // this is also not used. bool doNotRescaleS;


    cs->apoptoticTime = params[apoptoticTime];

    double G1_mu1 = 0, S_mu1 = 0, G2M_mu1 = 0;
    double G1_sigma1 = 0, S_sigma1 = 0, G2M_sigma1 = 0;

    if(background == Backgrounds::WT){
        G1_mu1 = params[avgG1];
        S_mu1 = params[avgS];
        G2M_mu1 = params[avgG2M];
        G1_sigma1 = params[widthG1];
        S_sigma1 = params[widthS];
        cs->distribG2M->_sigma1 = 0; //params[widthG2M];
    }
    else if(background == Backgrounds::DKO){
//        // This is the old way
//        G1_mu1 = params[avgG1] * params[modulatedG1KO];
//        S_mu1 = params[avgS] * params[modulatedSKO];
//        G2M_mu1 = params[avgG2M] * params[modulatedG2MKO];
//        G1_sigma1 = params[widthG1] * params[modulatedWidthG1KO];
//        S_sigma1 = params[widthS] * params[modulatedWidthSKO];
//        G2M_sigma1 = 0; //params[widthG2M] * params[modulatedQuiescentKO];

        G1_mu1 = params[modulatedG1KO];
        S_mu1 = params[modulatedSKO];
        G2M_mu1 = params[modulatedG2MKO];
        G1_sigma1 = params[modulatedWidthG1KO];
        S_sigma1 = params[modulatedWidthSKO];
        G2M_sigma1 = 0;


        if(G1_mu1 == 0)     G1_mu1 = params[avgG1];
        if(S_mu1 == 0)    S_mu1 = params[avgS];
        if(G2M_mu1 == 0)    G2M_mu1 = params[avgG2M];
        if(G1_sigma1 == 0)    G1_sigma1 = params[widthG1];
        if(S_sigma1 == 0)    S_sigma1 = params[widthS];
    } else {
        cerr << "Unknown Background" << background << endl;
    }


#ifdef doubleStagePop
    /// This code is only to have a double behaving population, with some parameters before and after a certain number of divisions
    passParameters::percentDeath = params[modulatedWidthSKO];
    passParameters::generationTremplinIncluded = static_cast<int>(params[modulatedWidthG1KO]+0.001);
    passParameters::newG1 = params[modulatedG1KO];
    passParameters::newS = params[modulatedSKO];
    passParameters::newG2M = params[modulatedG2MKO];
    paramNames[modulatedWidthSKO] = "percentDeath";
    paramNames[modulatedWidthG1KO] = "generationChange";
    paramNames[modulatedG1KO] = "newG1";
    paramNames[modulatedSKO] = "newS";
    paramNames[modulatedG2MKO] = "newG2M";
#endif


    // In case the full cycle is variable, the parameter width G1 will be taken
    double cycleTotMu = G1_mu1 + S_mu1 + G2M_mu1;
    double cycleTotSigma = G1_sigma1;
    if(!onlyTotalCycleIsVariable){
         cycleTotSigma = 0;
    } else {
        G1_sigma1 = 0;
        S_sigma1 = 0;
        G2M_sigma1 = 0;
        params[widthS] = 0;
        //params[widthG2M] = 0;
    }

    // the Mu and Sigma of a LogNormal are NOT the average and standard deviation => Need to convert
    if(typeDistributions == LogNormal){
        //cout << "Init distribs G1 mu " << G1_mu1 << " sigma " << G1_sigma1 << " avg " << cs->distribG1->getAverage() << endl;
        //cout << "Init distribs S mu " << S_mu1 << " sigma " << S_sigma1 << " avg " << cs->distribS->getAverage() << endl;
        //cout << "Init distribs G2M mu " << G2M_mu1 << " sigma " << G2M_sigma1 << " avg " << cs->distribG2M->getAverage() << endl;


        std::pair<double,double> rescaleG1 = LogNormParameters(G1_mu1, G1_sigma1);
        G1_mu1 = rescaleG1.first;
        G1_sigma1 = rescaleG1.second;

        std::pair<double,double> rescaleS = LogNormParameters(S_mu1, S_sigma1);
        S_mu1 = rescaleS.first;
        S_sigma1 = rescaleS.second;

        std::pair<double,double> rescaleG2M = LogNormParameters(G2M_mu1, G2M_sigma1);
        G2M_mu1 = rescaleG2M.first;
        G2M_sigma1 = rescaleG2M.second;

        std::pair<double,double> rescaleTot = LogNormParameters(cycleTotMu, cycleTotSigma);
        cycleTotMu = rescaleTot.first;
        cycleTotSigma = rescaleTot.second;
    }

    cs->distribG1->set(typeDistributions, G1_mu1, G1_sigma1);
    cs->distribS->set(typeDistributions, S_mu1, S_sigma1);
    cs->distribG2M->set(typeDistributions, G2M_mu1, G2M_sigma1);
    cs->distribTot->set(typeDistributions, cycleTotMu, cycleTotSigma);
    //cout << "Init distribs G1 mu " << G1_mu1 << " sigma " << G1_sigma1 << " avg " << cs->distribG1->getAverage() << endl;
    //cout << "Init distribs S mu " << S_mu1 << " sigma " << S_sigma1 << " avg " << cs->distribS->getAverage() << endl;
    //cout << "Init distribs G2M mu " << G2M_mu1 << " sigma " << G2M_sigma1 << " avg " << cs->distribG2M->getAverage() << endl;

    // For death, we define a RATE, which is also the lambda of the distribution => The average will be 1/lambda
    if(params[deathRate] <= 0) cs->distribDeath->set(Fixed, 1e-9);
    else cs->distribDeath->set(Exponential, params[deathRate]);

    cs->percentBystanderG0 = params[percentQuiescentG0];
    if(background == Backgrounds::DKO){
        cs->percentBystanderG0 = params[modulatedQuiescentKO];
        if(cs->percentBystanderG0 == 0) cs->percentBystanderG0 = params[percentQuiescentG0];
        if(cs->percentBystanderG0 < 0) cs->percentBystanderG0 = 0;
        if(cs->percentBystanderG0 > 1.0) cs->percentBystanderG0 = 1.0;
    }

    if(cs->distribG0->type == BiModal){
        cs->distribG0->_mu1 = 0.0;
        cs->distribG0->_sigma1 = 0.0;
        // the weight is on the first part of the bimodal
        cs->distribG0->_weight = 1.0 - params[widthOrPercentLongG0];
        cs->distribG0->_mu2 = params[avgG0];
        cs->distribG0->_sigma2 = 0.0;
    }
    else {
        /// to solve: what to do if returns negative number
        cs->distribG0->_mu1 = params[avgG0];
        cs->distribG0->_sigma1 = params[widthOrPercentLongG0];
    }

    // do it after death distribution is defined
    if(cs->typeDifferentiation == stage::finiteNrDiv){
        vector<double> steady = cs->getSteadyStateGenerations(params[aimedPopSize]);
        if(steady.size() > 0){
            params[inflowRate] = steady[0] * min(1.0, max(0.0, 1.0 - params[percentQuiescentG0])) / (max(1e-10, cs->getInterCycleTime()));
            //cout << "Inflow from steady=" << steady[0] << " and total cycle " << cs->getInterCycleTime() << endl;
        } else {
            params[inflowRate] = 0;
        }
    }

    // Final step: calculates parameters that depend on each-other and initiates the population of cells.
    currentSim->inflowRate = params[inflowRate]; // derived
    currentSim->initialize(currentObs);

    //    for(int i = 0; i < NbVariables; ++i){
    //    val[i] = init[i];}
    t = currentSim->time; // important, might start negative
    //cout << "Initialization done, now t=" << t << endl;

    /// Philippe: should be a better way to do this...
    if(t < 0) timeStep(t, -1e-6);
    cptErrors = 0;
    initialiseDone();
}

void dividingModel::timeStep( const double tstart, const double tend){
    // does not need to adapt val[]

    if((cptErrors < 20) && (fabs(currentSim->time - tstart) > 1e-5)){
        cptErrors++; cerr << "Does not understand, the model seems to have been simulated on its own " << currentSim->time << " vs start requested" << tstart << endl;
    }
    for(t = tstart; t < tend; t = t + dt){
        currentSim->dt = min(dt, tend - t);
        currentSim->timeStep();
        // val[nCells] = I think this has no effect here (only recorded with print_sec)
    }
}
void dividingModel::analyzeState(const double t){

    //in case there is no preSim time the first time-point is analyzed before actually simulating, so before the currentObserver has a last position (back)
    if(currentSim->currentObserver){
    val[doseEDU] = currentSim->currentObserver->o_doseEDU.last();
    val[doseBRDU] = currentSim->currentObserver->o_doseBRDU.last();
    val[popSize]  = currentSim->currentObserver->o_popSize.last();
    val[prcBRDU] = currentSim->currentObserver->o_prcBRDU.last();
    val[prcEDU] = currentSim->currentObserver->o_prcEDU.last();
    val[prcEDUpBRDUp] = currentSim->currentObserver->o_prcEDUpBRDUp.last();
    val[prcEDUpBRDUn] = currentSim->currentObserver->o_prcEDUpBRDUn.last();
    val[prcEDUnBRDUp] = currentSim->currentObserver->o_prcEDUnBRDUp.last();
    val[prcEDUnBRDUn] = currentSim->currentObserver->o_prcEDUnBRDUn.last();
    val[avgGen] = currentSim->currentObserver->o_avgGen.last();
    val[nbG1] = currentSim->currentObserver->o_nbG1.last();
    val[nbS] = currentSim->currentObserver->o_nbS.last();
    val[nbG2M] = currentSim->currentObserver->o_nbG2M.last();
    val[nbG0] = currentSim->currentObserver->o_nbG0.last();
    val[nbNewcomers] = currentSim->currentObserver-> o_nbNewcomers.last();
    val[flowOut] = currentSim->currentObserver->o_flowOut.last();
    val[avgGenOut] = currentSim->currentObserver->o_avgGenOut.last();
    val[gen0] = currentSim->currentObserver->o_gen0.last();
    val[gen1] = currentSim->currentObserver->o_gen1.last();
    val[gen2] = currentSim->currentObserver->o_gen2.last();
    val[gen3] = currentSim->currentObserver->o_gen3.last();
    val[gen4] = currentSim->currentObserver->o_gen4.last();
    val[gen5] = currentSim->currentObserver->o_gen5.last();
    val[gen6] = currentSim->currentObserver->o_gen6.last();
    val[gen7] = currentSim->currentObserver->o_gen7.last();
    val[gen8] = currentSim->currentObserver->o_gen8.last();
    val[gen9] = currentSim->currentObserver->o_gen9.last();
    val[gen10] = currentSim->currentObserver->o_gen10.last();
    val[apopt] = currentSim->currentObserver->o_apopt.last();
    } else {
        for(size_t i = 0; i < val.size(); ++i){
            val[i] = 0;
        }
    }

    // needs to fill val[]
    //cout << "Analyze " << t << endl;
    // this is copy-pasted from cytonwidget. Might be good to make a function
    vector<double> levelsDNAall;
    vector<double> levelsDNAPre;
    vector<double> levelsDNAPost;
    vector<double> levelsDNAMixed;
    vector<double> levelsDNAUnst;

    vector<double> fractionCycleAll = vector<double>(NBCycleStates, 0);
    vector<double> fractionCyclePre = vector<double>(NBCycleStates, 0);
    vector<double> fractionCyclePost = vector<double>(NBCycleStates, 0);
    vector<double> fractionCycleMixed = vector<double>(NBCycleStates, 0);
    vector<double> fractionCycleUnstained = vector<double>(NBCycleStates, 0);

    miniStats avgDNAAll;
    miniStats avgDNABRDUOnly;
    miniStats avgDNAUNeg;
    miniStats avgDNAPre;
    miniStats avgDNAPost;
    miniStats avgDNAMixed;


    stage* thisStage = currentSim->pop;
    double threshold = params[thresholdPos]; // 0.025;
    size_t NS = thisStage->insiders.size();
    for(size_t i = 0; i < NS; ++i){
        thymocyte* currentInsider = thisStage->insiders[i];
        if(currentInsider->status == DIVREMOVED) cerr << "Mayday" << endl;
        if(true){ //currentInsider->incomer){
            if((currentInsider->status != DEAD) && (currentInsider->DNA > 0)){ // don't get why some have 0 as DNA. Maybe in disappear state but not yet removed
                double D = currentInsider->currentDNA();
                double B = currentInsider->currentBRDU();
                double E = currentInsider->currentEDU();

                // for FACS plot cout << D << "\t" << B << "\t" << E << endl;
                levelsDNAall.push_back(D-1.);
                fractionCycleAll[currentInsider->cyclestatus]++;

                if((currentInsider->cyclestatus == S) || (currentInsider->cyclestatus == G2M)) avgDNAAll.addData(D-1.);

                if((E <= threshold) && (B <= threshold)){
                    levelsDNAUnst.push_back(D);
                    if((currentInsider->cyclestatus == S) || (currentInsider->cyclestatus == G2M))  avgDNAUNeg.addData(D-1.);
                    fractionCycleUnstained[currentInsider -> cyclestatus]++;
                }
                if((E > threshold) && (B <= threshold)) {
                    levelsDNAPost.push_back(D); // important to calculate frequencies
                    if ((currentInsider->cyclestatus == S) || (currentInsider->cyclestatus == G2M))  avgDNAPost.addData(D-1.);
                    fractionCyclePost[currentInsider->cyclestatus]++;
                }
                if((E > threshold) && (B > threshold)) {
                    levelsDNAMixed.push_back(D);
                    if ((currentInsider->cyclestatus == S) || (currentInsider->cyclestatus == G2M))  {
                        avgDNAMixed.addData(D-1.);
                        avgDNABRDUOnly.addData(D-1.);
                    }
                    fractionCycleMixed[currentInsider->cyclestatus]++;
                }
                if((E <= threshold) && (B > threshold)) {
                    levelsDNAPre.push_back(D);
                    if ((currentInsider->cyclestatus == S) || (currentInsider->cyclestatus == G2M)) {
                        avgDNAPre.addData(D-1.);
                        avgDNABRDUOnly.addData(D-1.);
                    }
                    fractionCyclePre[currentInsider->cyclestatus]++;
                }
            }
        }
    }

    /* if(levelsDNAall.size() > 0){
        histogramFromDistrib hAll = histogramFromDistrib(levelsDNAall, 50);
    }
    if(levelsDNAPre.size() > 0){
        histogramFromDistrib hPre = histogramFromDistrib(levelsDNAPre, 50);
    }
    if(levelsDNAPost.size() > 0){
        histogramFromDistrib hPost = histogramFromDistrib(levelsDNAPost, 50);
    }
    if(levelsDNAMixed.size() > 0){
        histogramFromDistrib hMix = histogramFromDistrib(levelsDNAMixed, 50);
    }*/
    //cpt++;

    for(size_t i = 0; i < NBCycleStates; ++i){
        fractionCycleAll[i] /= 0.01 * max(1., static_cast<double>(levelsDNAall.size()));
        fractionCyclePre[i] /= 0.01 * max(1., static_cast<double>(levelsDNAPre.size()));
        fractionCyclePost[i] /= 0.01 * max(1., static_cast<double>(levelsDNAPost.size()));
        fractionCycleMixed[i] /= 0.01 * max(1., static_cast<double>(levelsDNAMixed.size()));
        fractionCycleUnstained[i] /= 0.01 * max(1., static_cast<double>(levelsDNAUnst.size()));
    }

    //cout << "Fill val[] at time " << t << endl;
    val[AvgDNAPre] =  avgDNAPre.getAverage();
    val[AvgDNAMiddle] =  avgDNAMixed.getAverage();
    val[AvgDNAPost] =  avgDNAPost.getAverage();
    val[PreInG1] = fractionCyclePre[G1] + fractionCyclePre[G0];
    val[MiddleInG1] = fractionCycleMixed[G1] + fractionCycleMixed[G0];
    val[PostInG1] = fractionCyclePost[G1] + fractionCyclePost[G0]; // attention, overestimation due to stop of cycling !!!
    val[PreInS] = fractionCyclePre[S];
    val[MiddleInS] = fractionCycleMixed[S];
    val[PostInS] = fractionCyclePost[S];
    val[PreInG2M] = fractionCyclePre[G2M];
    val[MiddleInG2M] = fractionCycleMixed[G2M];
    val[PostInG2M] = fractionCyclePost[G2M];
    val[AvgDNABRDU] =  avgDNABRDUOnly.getAverage();// avgDNAAll.getAverage();
    val[UnstInS] = fractionCycleUnstained[S];        //val[AvgDNAUNeg] = avgDNAUNeg.getAverage();
    val[TotInG1] = fractionCycleAll[G1] + fractionCycleAll[G0];
    val[TotInS] = fractionCycleAll[S];
    val[TotInG2] = fractionCycleAll[G2M];
    val[nbEarly] = avgDNAPre.getN();
    val[nbMiddle] = avgDNAMixed.getN();
    val[nbPost] = avgDNAPost.getN();
    val[nCells] = thisStage->nbInsiders();
}







