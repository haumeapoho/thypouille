#ifndef COMMON_H
#define COMMON_H


// ===================== Part 1 : common tool functions to work with files and folders (to be platform independent) =======================

#include <string>
#include <vector>
using namespace std;

void createFolder(string folderName);       // because this sh** is OS dependent, better in a function
vector<string> listSubDirectories(string dir);
string currentDir();
string getParentFolder(string dir);
vector<string> getAllResultSubFolders(string dir); // folders containing a 'History' file
vector<string> listFilesInDir(string dir, string containing = string(""));
string locateProjectDirectory();
vector<string> findAllResultFolders(string dir);
void printVector(vector<string> v);
void testDirectoryFunctions();

// ===================== Part 2 : Platform detection =======================

// The code below automatically detects the version of QT and the platform,
// and will define the proper tags (QT4, QT5, WINDOWS, UNIX, MAC) automatically, to be used by all the files of the folder
//
// NOTE : in case of platform problems in linux, this package might be required
// sudo apt-get install build-essential g++

// =========  please don't touch anything below !... ===========
#ifndef WITHOUT_QT
#include <QtGlobal>
#if QT_VERSION >= 0x050000
#define QT5
#else
#define QT4
#endif
#endif

#ifdef _WIN32
   //define something for Windows (32-bit and 64-bit, this part is common)
    #define WINDOWS
#ifdef _WIN64
      //define something for Windows (64-bit only)
   #endif
#elif __APPLE__
    #define MAC
    #include "TargetConditionals.h"
    #if TARGET_IPHONE_SIMULATOR
         // iOS Simulator
    #elif TARGET_OS_IPHONE
        // iOS device
    #elif TARGET_OS_MAC
        // Other kinds of Mac OS
    #else
    #   error "Unknown Apple platform"
    #endif
#elif __linux__
    #define UNIX
    // linux
#elif __unix__ // all unices not caught above
    #define UNIX
    // Unix
#elif defined(_POSIX_VERSION)
    // POSIX
#else
#   error "Unknown compiler"
#endif



#endif // COMMON_H
