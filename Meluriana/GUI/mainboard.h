#ifndef MAINBOARD_H
#define MAINBOARD_H

#include <QWidget>
#include "grapheCustom.h"
#include "cytonSimu.h"
#include "wholethymus.h"

namespace Ui {
class MainBoard;
}

class MainBoard : public QWidget
{
    Q_OBJECT

public:
    explicit MainBoard(multiPopSimulation* _sim, QWidget *parent = 0);
    ~MainBoard();
    void display(masterObserver* newMO);
    void applyParamsToSimu();

    multiPopSimulation* sim;
    grapheCustom* plotDynDNs;
    grapheCustom* plotDynAllPops;
    grapheCustom* plotDynBrdu;
    grapheCustom* plotDoseBrdu;


    //grapheCustom* grapheCyclePost;
    //grapheCustom* grapheCyclePre;
    //grapheCustom* grapheCycleMixed;

public slots:
        void enable(bool enable);

private:
    Ui::MainBoard *ui;
};

#endif // MAINBOARD_H
