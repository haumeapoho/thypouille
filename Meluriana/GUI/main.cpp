#include "common.h"

//#define NO_INTERFACE

#ifndef NO_INTERFACE
#ifdef QT5
#include <QMainWindow>
#include <QApplication>
#endif
#ifdef QT4
#include <QtGui/QMainWindow>
#include <QtGui/QApplication>
#endif
#endif

#include "mainwindow.h"
#include "cytonSimu.h"

int main(int argc, char** argv){
#ifdef NO_INTERFACE
    cout << "Run, " << argc << "args, " << argv[0] << endl; // to avoid warning :-)
    multiPopSimulation* sim = new multiPopSimulation();
    masterObserver* MO = new masterObserver(string(""));
    sim->initialize(MO);
    while(sim->timeStep()){};
    return 0;
#else
    QApplication b(argc, argv);
    //multiPopSimulation* sim = new multiPopSimulation();
    MainWindow* MW = new MainWindow();
    MW->show();
    return b.exec();
#endif

}
