// -------------------- Script that uses Moonfit to perform simulations / optimizations ----------------------------

#define UseMainUrania
#ifdef UseMainUrania

//first staining is EDU,
//then second staining is BRDU
//E+B- 	E+B+ 	E-B+ 	E-B-
//Post 	Middle 	Pre 	UnLabelled



// ------------- Step 1: Include Moonfit. "moonfit.h" is enough to use the full library
#include "../Moonfit/moonfit.h"

// Note: You might want to have a look at Moonfit/common.h, (same location as Moonfit.h)
// The file common.h is already included from Moonfit.h and checks the system
//  -> it creates #define WINDOWS/UNIX/MAC and #define QT4/QT5 automatically if you need to use.
//  -> You can choose to compile without graphical interface nor Qt, then define WITHOUT_QT in common.h
//  -> It also defines useful functions (all platforms) like:
//            string createFolder(string folderName)
//            string currentDir()
//            string getParentFolder(string dir)
//            vector<string> listSubDirectories(string dir)
//            string codeTime()
//            void compileLatex(string folderRes, string texFile)
//            string printVector(vector<double> &v)
//  -> It gives predefined options for optimizers (for doing optimizations manually)
//            enum typeOptimizer {GeneticFast, SRESFast, Genetic25k,  ... SRES100k, ...
//            string optFileHeader(typeOptimizer toUse)




// ------------- Step 2: Now including all your models for the project, and the experiments defining what to do with them

#include "dividingmodelTwoPops.h"
#include "dividingmodelOnePop.h"
#include "expLabelling.h"
#include <cmath>
#include "random.h"
#include "bootstrap.h"





// ------------- Step 3a: It's always good to know where to read/write files. Define global variables for that ...

#include <vector>
#include <string>
#include <iostream>
using namespace std;

static string folder;
static string folderBaseResults;

// Note: you can define them manually. But not recommended as the main function already finds folders
// automatically, so you can move the project (better)
//      #define folder string("C:/Users/...")
//      #define defaultfolderBaseResults string("/home/phr13/...")


// declares the script functions that is defined lower
void bacicScript(int nb = -1,string newConfigFile = string(""), string newParameterSet = string(""));



/// @brief Main : to get help, launch without any argument. Graphical window will open, and when quitted, all options will be displayed.
int anternativeOldMain(int argc, char *argv[]){






    // ------------------ Step 3b finding folders when starting ...
    folder = "C:/Users/pprobert/Desktop/Softwares/NewArchaeropteryx/Sources/Meluriana/";
    folderBaseResults = "C:/Users/pprobert/Desktop/Softwares/NewArchaeropteryx/Sources/Meluriana/Results/";

    cout << "Working folders now detected: \n   -> Current Project in " << folder << "\n   -> Results can be put in " << folderBaseResults << endl;
    if(!dirExists(folder)) cerr << "Could not find the project folder for finding data and configurations: " << folder << endl;
    if(!dirExists(folderBaseResults.c_str())) createFolder(folderBaseResults);
    // Note: if problems for locating the exe you can also use QCoreApplication::applicationDirPath() or applicationFilePath() but you need to
    // first create a QApplication by  QApplication b(argc, argv);

    // Parsing main arguments for different options.

    // If called without argument, starts the graphical interface
    switch(argc){
        case 0: case 1: {
            string exeName = removeFolderFromFile(string(argv[0]));
            cout << "\n   Welcome !\n" << endl;
            cout << "   -> No option chosen from command line ..." << endl;
            cout << "   -> Other command line options are :\n" << endl;
            cout << "         "  << exeName << " NumScript " << endl;
            bacicScript();
            break;}
        // If given with a number, starts the respective script
        case 2: {
            bacicScript(atoi(argv[1]));
            break;}
        //case 3: {
        //    if(!string(argv[2]).compare(string("option"))) {
        //        otherFunction(atoi(argv[1]));
        //    }
        //    ...
        //    break;}
        default:{cerr << "ERR: Too many arguments given !" << endl;}
    }
    return 0;
}

















//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///
///
///         1 - Basic Script for different thymus models + graphical interface
///
///
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void bacicScript(int nb,string newConfigFile, string newParameterSet){

    // --------- Step 4: Initializing Qt if necessary
    #ifndef WITHOUT_QT
    char *args[] = {(char*)"Moonfit!",nullptr};
    int argc = 1;
    QApplication b(argc, args);             // Starts the Qt application
    #endif

    #define includeDNA true
    string DNA = ((includeDNA) ? "+DNA" : "");

    #define includeReturnS true
    if(includeReturnS) DNA = DNA + "+RetS";

    #define includeLateTime_points true
    if(includeLateTime_points) DNA = DNA + "+LateTP";

    #define FitG0 false
    string sfx = ((FitG0) ? "G0" : "");

    #define NBootstrap 5

    #define dataplace "DATA/DataForOldScripts/"


    // A small code to choose before different options
    vector<string> options = {
        "0 : oldDN1 (no data)",
        "1 : oldDN2",
        "2 : oldDN3",
        "3 : oldDN4",
        "4 : oldDP",
        "5 : oldSP4",
        "6 : oldSP8",
        "7 : DN1_WT_noDNA",
        "8 : DN1_DKO_noDNA",
        "9 : DN2_WT" + DNA,
        "10 : DN2_DKO" + DNA,
        "11 : DN3a_WT" + DNA,
        "12 : DN3a_DKO" + DNA,
        "13 : DN3b_WT" + DNA,
        "14 : DN3b_DKO" + DNA,
        "15 : DN4_WT" + DNA,
        "16 : DN4_DKO" + DNA,
        "17 : CD4SP_WT_noDNA",
        "18 : CD4SP_DKO_noDNA",
        "19 : CD8SP_WT_noDNA",
        "20 : CD8SP_DKO_noDNA",
        "21 : PreSelDP_WT" + DNA,
        "22 : PreSelDP_DKO" + DNA,
        "23 : PostSelDP_WT" + DNA,
        "24 : PostSelDP_DKO" + DNA,
        "25 : DN1_WTandKO" + DNA,
        "26 : DN2_WTandKO" + DNA,
        "27 : DN3a_WTandKO" + DNA,
        "28 : DN3b_WTandKO" + DNA,
        "29 : DN4_WTandKO" + DNA,
        "30 : PreSelDP_WTandKO" + DNA,
        "31 : PostSelDP_WTandKO" + DNA,
        "32 : CD4SP_WTandKO" + DNA,
        "33 : CD8SP_WTandKO" + DNA,
    };

    if(nb < 0){
        cout << "Available scripts by command line:";
        for(size_t i = 0; i < options.size(); ++i){
            cout << "   -> " << options[i] << endl;
        }

        #ifndef WITHOUT_QT
        cout << "Now choosing the option by a user dialog\n";
        // Make the list of choices as a list of QStrings for Qt
        QStringList items;
        for(size_t i = 0; i < options.size(); ++i){
            items << QString(options[i].c_str());
        }

        // Open a small window to chose the option
        bool okPressed = false;
        QString chosenTextParameter = QInputDialog::getItem(nullptr, QString("Model Choice."),QString("Please choose the model you would like to simulate among the following options:"), items,7, false, &okPressed);
        if(!okPressed) return; // means Cancel

        // retrieves which text was chosen and saves ID in nb
        for(size_t i = 0; i < options.size(); ++i){
            if(!(QString(options[i].c_str())).compare(chosenTextParameter)) {nb = static_cast<int>(i);}
        }
        if(nb < 0) {cerr << "ERR: couldn't find which option was chosen - should not happen" << endl; return;}
        #else
        cout << "Tip: You need to run with an argument (nr of script) or re-compile without #define WITHOUT_QT in common.h to choose by clicking" << endl;
        return;
        #endif
    }

    cout << "Launching script nr. " << nb << " ... " << endl;





    // ----------- Step 5: Create a model and create an experiment with this model. Good to take a config file as well.
    // --- and --- Step 6: Give experimental data to the experiment.
    Model* currentModel = nullptr;
    Experiment* currentExperiment = nullptr;
    string configFile = string("");
    currentModel = new dividingModel();

    switch(nb){

    case 0: { // DN1-4 model (Manesso 2009) + Pottritt 2003 data
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN1);
        cout << "Sorry, no data for DN" << endl;
        break;
    }
    case 1: { // DN1-4 model (Manesso 2009) + Pottritt 2003 data
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN2);
        cout << "Sorry, no data for DN" << endl;
        break;
    }
    case 2: { // DN1-4 model (Manesso 2009) + Pottritt 2003 data
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN3);
        cout << "Sorry, no data for DN" << endl;
        break;
    }
    case 3: { // DN1-4 model (Manesso 2009) + Pottritt 2003 data
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN4);
        cout << "Sorry, no data for DN" << endl;
        break;
    }
   case 4: { // DP
        configFile=string("../ConfBasicOnePop.txt");

        TableCourse* Data_DP = new TableCourse(folder + dataplace + string("newpostDPavg.txt"));
        cout << Data_DP->print() << endl;
        //TableCourse* Data_DPStd = new TableCourse(folder + dataplace + string("DPdev.txt"));
        //cout << Data_DPStd->print() << endl;

        if(0){
            //currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDP);
            MultiExperiments* megaCompare = new MultiExperiments(currentModel);
            for(int i = 0; i < currentModel->getNbParams(); ++i){
                Experiment* currentExperiment2 = new expParameters(currentModel, i, 2.0);
                currentExperiment2->Identification = string("Around ") + currentModel->getParamName(i);
                currentExperiment2->giveData(Data_DP, 0); // 1 is the index of the experiment
                currentExperiment2->loadEvaluators();
                megaCompare->AddExperiment(currentExperiment2);
            }
            //cout << "   -> Using model : " << currentModel->name << " with experiment " << megaCompare->print(); //currentExperiment->Identification << "\n";

            cout << "Launching Graphical Interface ..." << endl;
            #ifndef WITHOUT_QT
            // Step 7a: Launch the graphical interface from an experiment (containing the model inside)
            simuWin* p = new simuWin(megaCompare);

            // Step 7b: optionally give a config file
            p->loadConfig(folder + configFile);

            // Step 7c: show the graphical interface!
            p->show();

            // Step 7d: leave the control to Qt instead of finishing the program
            b.exec();

            exit(-1);//*/
            #else
            cerr << "ERR: Program compiled without Qt library (see #define WITHOUT_QT in common.h" << endl;
            #endif

        }
        //currentExperiment = new expParameters(currentModel, dividingModel::avgG1, 2.0);

/*        configFile=string("Model0ManessoOnly/ConfigManessoBasic.txt");

        TableCourse* Data_DP = new TableCourse(folder + dataplace + string("DPavg.txt"));
        cout << Data_DP->print() << endl;
        TableCourse* Data_DPStd = new TableCourse(folder + dataplace + string("DPdev.txt"));
        cout << Data_DPStd->print() << endl;
*/
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expeDP);

        currentExperiment->giveData(Data_DP, 0); // 1 is the index of the experiment

        break;
    }

    case 5: { // SP4
         currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expSP4);
         configFile=string("Model0ManessoOnly/ConfigManessoBasic.txt");

         TableCourse* Data_SP4 = new TableCourse(folder + dataplace + string("SP4avg.txt"));
         cout << Data_SP4->print() << endl;
         TableCourse* Data_SP4Std = new TableCourse(folder + dataplace + string("SP4dev.txt"));
         cout << Data_SP4Std->print() << endl;

         currentExperiment->giveData(Data_SP4, 0, Data_SP4Std); // 1 is the index of the experiment
         break;
     }

    case 6: { // SP8
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expSP8);
        configFile=string("Model0ManessoOnly/ConfigManessoBasic.txt");

        TableCourse* Data_SP8 = new TableCourse(folder + dataplace + string("SP8avg.txt"));
        cout << Data_SP8->print() << endl;
        TableCourse* Data_SP8Std = new TableCourse(folder + dataplace + string("SP8dev.txt"));
        cout << Data_SP8Std->print() << endl;

        currentExperiment->giveData(Data_SP8, 0, Data_SP8Std); // 1 is the index of the experiment
        break;
    }


#define config1pop "Config1PopFiniteNrDiv.txt"
//configFile=string("ConfigOnePopulationGeneric.txt");

    case 7: case 57: { // NewDN1_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN1);
        currentExperiment->Identification += "NewDN1_WT_noDNA";
        configFile=string("Configs/DN1_WT_1Var.txt");
        if(nb == 57) configFile=string("Configs/DN1_WT_AllVar.txt");

        TableCourse* Data_DN1new = new TableCourse(folder + dataplace + string("DN1_WT_POOL_AVG.txt"));
        cout << Data_DN1new->print() << endl;
        TableCourse* Data_DN1newStd = new TableCourse(folder + dataplace + string("DN1_WT_POOL_STD.txt"));
        cout << Data_DN1newStd->print() << endl;

        currentExperiment->giveData(Data_DN1new, 0, Data_DN1newStd); // 1 is the index of the experiment
        break;
    }

    case 8: case 58: { // NewDN1_DKO
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN1);
        currentExperiment->Identification += "NewDN1_KO_noDNA";
        configFile=string("Configs/DN1_KO_1Var.txt");
        if(nb == 58) configFile=string("Configs/DN1_KO_AllVar.txt");

        TableCourse* Data_DN1new = new TableCourse(folder + dataplace + string("DN1_KO_POOL_AVG.txt"));
        cout << Data_DN1new->print() << endl;
        TableCourse* Data_DN1newStd = new TableCourse(folder + dataplace + string("DN1_KO_POOL_STD.txt"));
        cout << Data_DN1newStd->print() << endl;

        currentExperiment->giveData(Data_DN1new, 0, Data_DN1newStd); // 1 is the index of the experiment
        break;
    }













    case 9: case 59: case 89: { // NewDN2_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN2);
        currentExperiment->Identification += "NewDN2_WT" + DNA;
        configFile=string("Configs/DN2_WT_1Var") + sfx + string(".txt");
        if(nb == 59) configFile=string("Configs/DN2_WT_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN2new = new TableCourse(folder + dataplace + string("DN2_WT_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN2new->print() << endl;
        TableCourse* Data_DN2newStd = new TableCourse(folder + dataplace + string("DN2_WT_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN2newStd->print() << endl;

        if(nb == 89){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN2new, *Data_DN2newStd, NBootstrap);
            Data_DN2new = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN2new, 0, Data_DN2newStd); // 1 is the index of the experiment
        break;
    }

    case 10: case 60: case 90:{ // NewDN2_DKO
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN2);
        currentExperiment->Identification += "NewDN2_KO" + DNA;
        configFile=string("Configs/DN2_KO_1Var") + sfx + string(".txt");
        if(nb == 60) configFile=string("Configs/DN2_KO_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN2new = new TableCourse(folder + dataplace + string("DN2_KO_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN2new->print() << endl;
        TableCourse* Data_DN2newStd = new TableCourse(folder + dataplace + string("DN2_KO_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN2newStd->print() << endl;

        if(nb == 90){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN2new, *Data_DN2newStd, NBootstrap);
            Data_DN2new = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN2new, 0, Data_DN2newStd); // 1 is the index of the experiment
        break;
    }

    case 11: case 61: case 91:
    case 100: case 101: case 102: case 103: case 104: case 105: case 106: case 107: case 108: case 109: case 110: case 111: case 112: { // NewDN3a_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN3);
        currentExperiment->Identification += "NewDN3a_WT" + DNA;
        configFile=string("Configs/DN3a_WT_1Var") + sfx + string(".txt");
        if(nb == 61) configFile=string("Configs/DN3a_WT_AllVar") + sfx + string(".txt");
        if(nb == 100) configFile=string("Configs/Model100.txt");
        if(nb == 101) configFile=string("Configs/Model101.txt");
        if(nb == 102) configFile=string("Configs/Model102.txt");
        if(nb == 103) configFile=string("Configs/Model103.txt");
        if(nb == 104) configFile=string("Configs/Model104.txt");
        if(nb == 105) configFile=string("Configs/Model105.txt");
        if(nb == 106) configFile=string("Configs/Model106.txt");
        if(nb == 107) configFile=string("Configs/Model107.txt");
        if(nb == 108) configFile=string("Configs/Model108.txt");
        if(nb == 109) configFile=string("Configs/Model109.txt");
        if(nb == 110) configFile=string("Configs/Model110.txt");
        if(nb == 111) configFile=string("Configs/Model111.txt");
        if(nb == 112) configFile=string("Configs/Model112.txt");

        TableCourse* Data_DN3anew = new TableCourse(folder + dataplace + string("DN3A_WT_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN3anew->print() << endl;
        TableCourse* Data_DN3anewStd = new TableCourse(folder + dataplace + string("DN3A_WT_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN3anewStd->print() << endl;

        if(nb == 91){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN3anew, *Data_DN3anewStd, NBootstrap);
            Data_DN3anew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN3anew, 0, Data_DN3anewStd); // 1 is the index of the experiment
        break;
    }

    case 12: case 62: case 92:
    case 120: case 121: case 122: case 123: case 124: case 125: case 126: case 127: case 128: case 129: case 130: case 131: case 132:
    { // NewDN3a_DKO
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN3);
        currentExperiment->Identification += "NewDN3a_KO" + DNA;
        configFile=string("Configs/DN3a_KO_1Var") + sfx + string(".txt");
        if(nb == 62) configFile=string("Configs/DN3a_KO_AllVar") + sfx + string(".txt");

        if(nb == 120) configFile=string("Configs/Model100KO.txt");
        if(nb == 121) configFile=string("Configs/Model101KO.txt");
        if(nb == 122) configFile=string("Configs/Model102KO.txt");
        if(nb == 123) configFile=string("Configs/Model103KO.txt");
        if(nb == 124) configFile=string("Configs/Model104KO.txt");
        if(nb == 125) configFile=string("Configs/Model105KO.txt");
        if(nb == 126) configFile=string("Configs/Model106KO.txt");
        if(nb == 127) configFile=string("Configs/Model107KO.txt");
        if(nb == 128) configFile=string("Configs/Model108KO.txt");
        if(nb == 129) configFile=string("Configs/Model109KO.txt");
        if(nb == 130) configFile=string("Configs/Model110KO.txt");
        if(nb == 131) configFile=string("Configs/Model111KO.txt");
        if(nb == 132) configFile=string("Configs/Model112KO.txt");

        TableCourse* Data_DN3anew = new TableCourse(folder + dataplace + string("DN3A_KO_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN3anew->print() << endl;
        TableCourse* Data_DN3anewStd = new TableCourse(folder + dataplace + string("DN3A_KO_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN3anewStd->print() << endl;

        if(nb == 92){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN3anew, *Data_DN3anewStd, NBootstrap);
            Data_DN3anew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN3anew, 0, Data_DN3anewStd); // 1 is the index of the experiment
        break;
    }

    case 13: case 63: case 93: { // NewDN3b_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN3);
        currentExperiment->Identification += "NewDN3b_WT" + DNA;
        configFile=string("Configs/DN3b_WT_1Var") + sfx + string(".txt");
        if(nb == 63) configFile=string("Configs/DN3b_WT_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN3bnew = new TableCourse(folder + dataplace + string("DN3B_WT_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN3bnew->print() << endl;
        TableCourse* Data_DN3bnewStd = new TableCourse(folder + dataplace + string("DN3B_WT_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN3bnewStd->print() << endl;

        if(nb == 93){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN3bnew, *Data_DN3bnewStd, NBootstrap);
            Data_DN3bnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN3bnew, 0, Data_DN3bnewStd); // 1 is the index of the experiment
        break;
    }

    case 14: case 64: case 94: { // NewDN3b_DKO
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN3);
        currentExperiment->Identification += "NewDN3b_KO" + DNA;
        configFile=string("Configs/DN3b_KO_1Var") + sfx + string(".txt");
        if(nb == 64) configFile=string("Configs/DN3b_KO_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN3bnew = new TableCourse(folder + dataplace + string("DN3B_KO_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN3bnew->print() << endl;
        TableCourse* Data_DN3bnewStd = new TableCourse(folder + dataplace + string("DN3B_KO_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN3bnewStd->print() << endl;

        if(nb == 94){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN3bnew, *Data_DN3bnewStd, NBootstrap);
            Data_DN3bnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN3bnew, 0, Data_DN3bnewStd); // 1 is the index of the experiment
        break;
    }

    case 15: case 65: case 95: { // NewDN4_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN4);
        currentExperiment->Identification += "NewDN4_WT" + DNA;
        configFile=string("Configs/DN4_WT_1Var") + sfx + string(".txt");
        if(nb == 65) configFile=string("Configs/DN4_WT_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN4new = new TableCourse(folder + dataplace + string("DN4_WT_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN4new->print() << endl;
        TableCourse* Data_DN4newStd = new TableCourse(folder + dataplace + string("DN4_WT_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN4newStd->print() << endl;

        if(nb == 95){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN4new, *Data_DN4newStd, NBootstrap);
            Data_DN4new = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN4new, 0, Data_DN4newStd); // 1 is the index of the experiment
        break;
    }

    case 16: case 66: case 96: { // NewDN4_DKO
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN4);
        currentExperiment->Identification += "NewDN4_KO" + DNA;
        configFile=string("Configs/DN4_KO_1Var") + sfx + string(".txt");
        if(nb == 66) configFile=string("Configs/DN4_KO_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN4new = new TableCourse(folder + dataplace + string("DN4_KO_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN4new->print() << endl;
        TableCourse* Data_DN4newStd = new TableCourse(folder + dataplace + string("DN4_KO_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN4newStd->print() << endl;

        if(nb == 96){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN4new, *Data_DN4newStd, NBootstrap);
            Data_DN4new = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN4new, 0, Data_DN4newStd); // 1 is the index of the experiment
        break;
    }

    case 17: case 150: case 158: { // NewCD4SP_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expSP4);
        currentExperiment->Identification += "NewCD4SP_WT_noDNA";
        //configFile=string("ConfigOnePopulationGeneric.txt");
        //configFile = config1pop;
        configFile=string("Configs/GenericConfig") + sfx + string(".txt");
        if(nb == 158) configFile=string("Configs/GenericConfigNdivG0.txt");

        TableCourse* Data_CD4SPnew = new TableCourse(folder + dataplace + string("CD4SP_WT_POOL_AVG.txt"));
        cout << Data_CD4SPnew->print() << endl;
        TableCourse* Data_CD4SPnewStd = new TableCourse(folder + dataplace + string("CD4SP_WT_POOL_STD.txt"));
        cout << Data_CD4SPnewStd->print() << endl;

        if(nb == 150){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_CD4SPnew, *Data_CD4SPnewStd, NBootstrap);
            Data_CD4SPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_CD4SPnew, 0, Data_CD4SPnewStd); // 1 is the index of the experiment
        break;
    }

    case 18: case 151: case 159: { // NewCD4SP_DKO
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expSP4);
        currentExperiment->Identification += "NewCD4SP_KO_noDNA";
        //configFile=string("ConfigOnePopulationGeneric") + sfx + string(".txt");
        //configFile = config1pop;
        configFile=string("Configs/GenericConfig") + sfx + string(".txt");
        if(nb == 159) configFile=string("Configs/GenericConfigNdivG0.txt");

        TableCourse* Data_CD4SPnew = new TableCourse(folder + dataplace + string("CD4SP_KO_POOL_AVG.txt"));
        cout << Data_CD4SPnew->print() << endl;
        TableCourse* Data_CD4SPnewStd = new TableCourse(folder + dataplace + string("CD4SP_KO_POOL_STD.txt"));
        cout << Data_CD4SPnewStd->print() << endl;

        if(nb == 151){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_CD4SPnew, *Data_CD4SPnewStd, NBootstrap);
            Data_CD4SPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_CD4SPnew, 0, Data_CD4SPnewStd); // 1 is the index of the experiment
        break;
    }

    case 19: case 152: case 160:  { // NewCD8SP_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expSP8);
        currentExperiment->Identification += "NewCD8SP_WT_noDNA";
        //configFile=string("ConfigOnePopulationGeneric") + sfx + string(".txt");
        //configFile = config1pop;

        configFile=string("Configs/GenericConfig") + sfx + string(".txt");
        if(nb == 160) configFile=string("Configs/GenericConfigNdivG0.txt");

        TableCourse* Data_CD8SPnew = new TableCourse(folder + dataplace + string("CD8SP_WT_POOL_AVG.txt"));
        cout << Data_CD8SPnew->print() << endl;
        TableCourse* Data_CD8SPnewStd = new TableCourse(folder + dataplace + string("CD8SP_WT_POOL_STD.txt"));
        cout << Data_CD8SPnewStd->print() << endl;

        if(nb == 152){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_CD8SPnew, *Data_CD8SPnewStd, NBootstrap);
            Data_CD8SPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_CD8SPnew, 0, Data_CD8SPnewStd); // 1 is the index of the experiment
        break;
    }

    case 20: case 153: case 161: { // NewCD8SP_DKO
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expSP8);
        currentExperiment->Identification += "NewCD8SP_KO_noDNA";
        //configFile=string("ConfigOnePopulationGeneric") + sfx + string(".txt");
        //configFile = config1pop;
        configFile=string("Configs/GenericConfig") + sfx + string(".txt");
        if(nb == 161) configFile=string("Configs/GenericConfigNdivG0.txt");


        TableCourse* Data_CD8SPnew = new TableCourse(folder + dataplace + string("CD8SP_KO_POOL_AVG.txt"));
        cout << Data_CD8SPnew->print() << endl;
        TableCourse* Data_CD8SPnewStd = new TableCourse(folder + dataplace + string("CD8SP_KO_POOL_STD.txt"));
        cout << Data_CD8SPnewStd->print() << endl;

        if(nb == 153){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_CD8SPnew, *Data_CD8SPnewStd, NBootstrap);
            Data_CD8SPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_CD8SPnew, 0, Data_CD8SPnewStd); // 1 is the index of the experiment
        break;
    }

    case 21: case 71: case 154: case 162: { // NewpreSelDP_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expeDP);
        currentExperiment->Identification += "NewpreSelDP_WT" + DNA;
        configFile=string("Configs/PreSelDP_WT_1Var") + sfx + string(".txt");
        if(nb == 71) configFile=string("Configs/PreSelDP_WT_AllVar") + sfx + string(".txt");
        if(nb == 162) configFile=string("Configs/PreSelDP_WT_1VarNdivG0.txt");

        TableCourse* Data_preSelDPnew = new TableCourse(folder + dataplace + string("preSel_WT_POOL_AVG") + DNA + string(".txt"));
        cout << Data_preSelDPnew->print() << endl;
        TableCourse* Data_preSelDPnewStd = new TableCourse(folder + dataplace + string("preSel_WT_POOL_STD") + DNA + string(".txt"));
        cout << Data_preSelDPnewStd->print() << endl;

        if(nb == 154){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_preSelDPnew, *Data_preSelDPnewStd, NBootstrap);
            Data_preSelDPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_preSelDPnew, 0, Data_preSelDPnewStd); // 1 is the index of the experiment
        break;
    }

    case 22: case 72: case 155: case 163: { // NewpreSelDP_DKO
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expeDP);
        currentExperiment->Identification += "NewpreSelDP_KO" + DNA;
        configFile=string("Configs/PreSelDP_KO_1Var") + sfx + string(".txt");
        if(nb == 72) configFile=string("Configs/PreSelDP_KO_AllVar") + sfx + string(".txt");
        if(nb == 163) configFile=string("Configs/PreSelDP_KO_1VarNdivG0.txt");


        TableCourse* Data_preSelDPnew = new TableCourse(folder + dataplace + string("preSel_KO_POOL_AVG") + DNA + string(".txt"));
        cout << Data_preSelDPnew->print() << endl;
        TableCourse* Data_preSelDPnewStd = new TableCourse(folder + dataplace + string("preSel_KO_POOL_STD") + DNA + string(".txt"));
        cout << Data_preSelDPnewStd->print() << endl;

        if(nb == 155){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_preSelDPnew, *Data_preSelDPnewStd, NBootstrap);
            Data_preSelDPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_preSelDPnew, 0, Data_preSelDPnewStd); // 1 is the index of the experiment
        break;
    }

    case 23: case 156: case 164: { // NewpostSelDP_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::explDP);
        currentExperiment->Identification += "NewpostSelDP_WT" + DNA;
        //configFile=string("ConfigOnePopulationGeneric") + sfx + string(".txt");
        //configFile = config1pop;
        configFile=string("Configs/GenericConfig") + sfx + string(".txt");
        if(nb == 164) configFile=string("Configs/GenericConfigNdivG0.txt");

        TableCourse* Data_postSelDPnew = new TableCourse(folder + dataplace + string("postSelDP_WT_POOL_AVG") + DNA + string(".txt"));
        cout << Data_postSelDPnew->print() << endl;
        TableCourse* Data_postSelDPnewStd = new TableCourse(folder + dataplace + string("postSelDP_WT_POOL_STD") + DNA + string(".txt"));
        cout << Data_postSelDPnewStd->print() << endl;

        if(nb == 156){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_postSelDPnew, *Data_postSelDPnewStd, NBootstrap);
            Data_postSelDPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_postSelDPnew, 0, Data_postSelDPnewStd); // 1 is the index of the experiment
        break;
    }

    case 24: case 157: case 165: { // NewpostSelDP_DKO
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::explDP);
        currentExperiment->Identification += "NewpostSelDP_KO" + DNA;
        //configFile=string("ConfigOnePopulationGeneric") + sfx + string(".txt");
        //old way: configFile = config1pop;
        configFile=string("Configs/GenericConfig") + sfx + string(".txt");
        if(nb == 165) configFile=string("Configs/GenericConfigNdivG0.txt");

        TableCourse* Data_postSelDPnew = new TableCourse(folder + dataplace + string("postSelDP_KO_POOL_AVG") + DNA + string(".txt"));
        cout << Data_postSelDPnew->print() << endl;
        TableCourse* Data_postSelDPnewStd = new TableCourse(folder + dataplace + string("postSelDP_KO_POOL_STD") + DNA + string(".txt"));
        cout << Data_postSelDPnewStd->print() << endl;

        if(nb == 157){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_postSelDPnew, *Data_postSelDPnewStd, NBootstrap);
            Data_postSelDPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_postSelDPnew, 0, Data_postSelDPnewStd); // 1 is the index of the experiment
        break;
    }















    case 209: case 259: case 289: { // RegDN2_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN2);
        currentExperiment->Identification += "RegDN2_WT" + DNA;
        configFile=string("Configs/DN2_WT_1Var") + sfx + string(".txt");
        if(nb == 259) configFile=string("Configs/DN2_WT_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN2new = new TableCourse(folder + dataplace + string("RegDN2_WT_AVG.txt"));
        cout << Data_DN2new->print() << endl;
        TableCourse* Data_DN2newStd = new TableCourse(folder + dataplace + string("RegDN2_WT_STD.txt"));
        cout << Data_DN2newStd->print() << endl;

        if(nb == 289){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN2new, *Data_DN2newStd, NBootstrap);
            Data_DN2new = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN2new, 0, Data_DN2newStd); // 1 is the index of the experiment
        break;
    }

    case 210: case 260: case 290:{ // RegDN2_IRR
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN2);
        currentExperiment->Identification += " RegDN2_IRR" + DNA;
        configFile=string("Configs/DN2_KO_1Var") + sfx + string(".txt");
        if(nb == 260) configFile=string("Configs/DN2_KO_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN2new = new TableCourse(folder + dataplace + string("RegDN2_IRR_AVG.txt"));
        cout << Data_DN2new->print() << endl;
        TableCourse* Data_DN2newStd = new TableCourse(folder + dataplace + string("RegDN2_IRR_STD.txt"));
        cout << Data_DN2newStd->print() << endl;

        if(nb == 290){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN2new, *Data_DN2newStd, NBootstrap);
            Data_DN2new = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN2new, 0, Data_DN2newStd); // 1 is the index of the experiment
        break;
    }

    case 211: case 261: case 291: { // RegDN3a_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN3);
        currentExperiment->Identification += "RegDN3a_WT" + DNA;
        configFile=string("Configs/DN3a_WT_1Var") + sfx + string(".txt");
        if(nb == 261) configFile=string("Configs/DN3a_WT_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN3anew = new TableCourse(folder + dataplace + string("RegDN3A_WT_AVG.txt"));
        cout << Data_DN3anew->print() << endl;
        TableCourse* Data_DN3anewStd = new TableCourse(folder + dataplace + string("RegDN3A_WT_STD.txt"));
        cout << Data_DN3anewStd->print() << endl;

        if(nb == 291){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN3anew, *Data_DN3anewStd, NBootstrap);
            Data_DN3anew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN3anew, 0, Data_DN3anewStd); // 1 is the index of the experiment
        break;
    }

    case 212: case 262: case 292: { // RegDN3A_IRR
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN3);
        currentExperiment->Identification += "RegDN3A_IRR" + DNA;
        configFile=string("Configs/DN3a_KO_1Var") + sfx + string(".txt");
        if(nb == 262) configFile=string("Configs/DN3a_KO_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN3anew = new TableCourse(folder + dataplace + string("RegDN3A_IRR_AVG.txt"));
        cout << Data_DN3anew->print() << endl;
        TableCourse* Data_DN3anewStd = new TableCourse(folder + dataplace + string("RegDN3A_IRR_STD.txt"));
        cout << Data_DN3anewStd->print() << endl;

        if(nb == 292){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN3anew, *Data_DN3anewStd, NBootstrap);
            Data_DN3anew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN3anew, 0, Data_DN3anewStd); // 1 is the index of the experiment
        break;
    }

    case 213: case 263: case 293: { // RegDN3B_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN3);
        currentExperiment->Identification += "RegDN3B_WT" + DNA;
        configFile=string("Configs/DN3b_WT_1Var") + sfx + string(".txt");
        if(nb == 263) configFile=string("Configs/DN3b_WT_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN3bnew = new TableCourse(folder + dataplace + string("RegDN3B_WT_AVG.txt"));
        cout << Data_DN3bnew->print() << endl;
        TableCourse* Data_DN3bnewStd = new TableCourse(folder + dataplace + string("RegDN3B_WT_STD.txt"));
        cout << Data_DN3bnewStd->print() << endl;

        if(nb == 293){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN3bnew, *Data_DN3bnewStd, NBootstrap);
            Data_DN3bnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN3bnew, 0, Data_DN3bnewStd); // 1 is the index of the experiment
        break;
    }

    case 214: case 264: case 294: { // RegDN3B_IRR
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN3);
        currentExperiment->Identification += "RegDN3B_IRR" + DNA;
        configFile=string("Configs/DN3b_KO_1Var") + sfx + string(".txt");
        if(nb == 264) configFile=string("Configs/DN3b_KO_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN3bnew = new TableCourse(folder + dataplace + string("RegDN3B_IRR_AVG.txt"));
        cout << Data_DN3bnew->print() << endl;
        TableCourse* Data_DN3bnewStd = new TableCourse(folder + dataplace + string("RegDN3B_IRR_STD.txt"));
        cout << Data_DN3bnewStd->print() << endl;

        if(nb == 294){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN3bnew, *Data_DN3bnewStd, NBootstrap);
            Data_DN3bnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN3bnew, 0, Data_DN3bnewStd); // 1 is the index of the experiment
        break;
    }

    case 215: case 265: case 295: { // RegDN4_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN4);
        currentExperiment->Identification += "RegDN4_WT" + DNA;
        configFile=string("Configs/DN4_WT_1Var") + sfx + string(".txt");
        if(nb == 265) configFile=string("Configs/DN4_WT_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN4new = new TableCourse(folder + dataplace + string("RegDN4_WT_AVG.txt"));
        cout << Data_DN4new->print() << endl;
        TableCourse* Data_DN4newStd = new TableCourse(folder + dataplace + string("RegDN4_WT_STD.txt"));
        cout << Data_DN4newStd->print() << endl;

        if(nb == 295){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN4new, *Data_DN4newStd, NBootstrap);
            Data_DN4new = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN4new, 0, Data_DN4newStd); // 1 is the index of the experiment
        break;
    }

    case 216: case 266: case 296: { // RegDN4_IRR
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expDN4);
        currentExperiment->Identification += "RegDN4_IRR" + DNA;
        configFile=string("Configs/DN4_KO_1Var") + sfx + string(".txt");
        if(nb == 266) configFile=string("Configs/DN4_KO_AllVar") + sfx + string(".txt");

        TableCourse* Data_DN4new = new TableCourse(folder + dataplace + string("RegDN4_IRR_AVG.txt"));
        cout << Data_DN4new->print() << endl;
        TableCourse* Data_DN4newStd = new TableCourse(folder + dataplace + string("RegDN4_IRR_STD.txt"));
        cout << Data_DN4newStd->print() << endl;

        if(nb == 296){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_DN4new, *Data_DN4newStd, NBootstrap);
            Data_DN4new = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_DN4new, 0, Data_DN4newStd); // 1 is the index of the experiment
        break;
    }

    case 221: case 271: case 297: case 307: { // RegPreSelDP_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expeDP);
        currentExperiment->Identification += "RegPreSelDP_WT" + DNA;
        configFile=string("Configs/PreSelDP_WT_1Var") + sfx + string(".txt");
        if(nb == 307) configFile=string("Configs/PreSelDP_WT_1VarNdivG0.txt");
        if(nb == 271) configFile=string("Configs/PreSelDP_WT_AllVar") + sfx + string(".txt");

        TableCourse* Data_preSelDPnew = new TableCourse(folder + dataplace + string("RegPRE_DP_WT_AVG.txt"));
        cout << Data_preSelDPnew->print() << endl;
        TableCourse* Data_preSelDPnewStd = new TableCourse(folder + dataplace + string("RegPRE_DP_WT_STD.txt"));
        cout << Data_preSelDPnewStd->print() << endl;

        if(nb == 297){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_preSelDPnew, *Data_preSelDPnewStd, NBootstrap);
            Data_preSelDPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_preSelDPnew, 0, Data_preSelDPnewStd); // 1 is the index of the experiment
        break;
    }

    case 222: case 272: case 298: case 308: { // RegPreSelDP_IRR
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::expeDP);
        currentExperiment->Identification += "RegPreSelDP_IRR" + DNA;
        configFile=string("Configs/PreSelDP_KO_1Var") + sfx + string(".txt");
        if(nb == 308) configFile=string("Configs/PreSelDP_KO_1VarNdivG0.txt");
        if(nb == 272) configFile=string("Configs/PreSelDP_KO_AllVar") + sfx + string(".txt");

        TableCourse* Data_preSelDPnew = new TableCourse(folder + dataplace + string("RegPRE_DP_IRR_AVG.txt"));
        cout << Data_preSelDPnew->print() << endl;
        TableCourse* Data_preSelDPnewStd = new TableCourse(folder + dataplace + string("RegPRE_DP_IRR_STD.txt"));
        cout << Data_preSelDPnewStd->print() << endl;

        if(nb == 298){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_preSelDPnew, *Data_preSelDPnewStd, NBootstrap);
            Data_preSelDPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_preSelDPnew, 0, Data_preSelDPnewStd); // 1 is the index of the experiment
        break;
    }




    case 223: case 299:  case 309: { // RegPostSelDP_WT
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::explDP);
        currentExperiment->Identification += "RegPostSelDP_WT" + DNA;
        //configFile=string("Configs/ConfigOnePopulationGeneric.txt");
        //configFile = config1pop;
        configFile=string("Configs/GenericConfig") + sfx + string(".txt");
        if(nb == 309) configFile=string("Configs/GenericConfigNdivG0.txt");

        TableCourse* Data_postSelDPnew = new TableCourse(folder + dataplace + string("RegPOST_DP_WT_AVG.txt"));
        cout << Data_postSelDPnew->print() << endl;
        TableCourse* Data_postSelDPnewStd = new TableCourse(folder + dataplace + string("RegPOST_DP_WT_STD.txt"));
        cout << Data_postSelDPnewStd->print() << endl;

        if(nb == 299){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_postSelDPnew, *Data_postSelDPnewStd, NBootstrap);
            Data_postSelDPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_postSelDPnew, 0, Data_postSelDPnewStd); // 1 is the index of the experiment
        break;
    }

    case 224: case 300: case 310: { // RegPostSelDP_IRR
        currentExperiment = new expDoubleLabel(currentModel, expDoubleLabel::explDP);
        currentExperiment->Identification += "RegPostSelDP_IRR" + DNA;
        //configFile=string("ConfigOnePopulationGeneric.txt");
        //configFile = config1pop;
        configFile=string("Configs/GenericConfig") + sfx + string(".txt");
        if(nb == 310) configFile=string("Configs/GenericConfigNdivG0.txt");

        TableCourse* Data_postSelDPnew = new TableCourse(folder + dataplace + string("RegPOST_DP_IRR_AVG.txt"));
        cout << Data_postSelDPnew->print() << endl;
        TableCourse* Data_postSelDPnewStd = new TableCourse(folder + dataplace + string("RegPOST_DP_IRR_STD.txt"));
        cout << Data_postSelDPnewStd->print() << endl;

        if(nb == 300){
            TableCourse Bootstrapped = bootstrap::randomize(*Data_postSelDPnew, *Data_postSelDPnewStd, NBootstrap);
            Data_postSelDPnew = new TableCourse(Bootstrapped);
        }

        currentExperiment->giveData(Data_postSelDPnew, 0, Data_postSelDPnewStd); // 1 is the index of the experiment
        break;
    }







    // ========== these are the WT and KO simultaneous simulations, not used anymore =================

    // New way of processing the data
    case 25: case 75: { // NewDN1_WT and KO, no DNA data
        currentExperiment = new expDoubleLabelWT_KO(currentModel);
        currentExperiment->Identification += "NewDN1_WT_KO_noDNA";
        configFile=string("ConfigDN1a_WT_KO.txt");

        configFile=string("Configs/DN1_WT_KO_1VarG1diff.txt");
        if(nb == 75) configFile=string("Configs/DN1_WT_KO_AllVarTotdiff.txt");

        TableCourse* Data_DN1new = new TableCourse(folder + dataplace + string("DN1_WT_POOL_AVG.txt"));
        cout << Data_DN1new->print() << endl;
        TableCourse* Data_DN1newStd = new TableCourse(folder + dataplace + string("DN1_WT_POOL_STD.txt"));
        cout << Data_DN1newStd->print() << endl;

        currentExperiment->giveData(Data_DN1new, Backgrounds::WT, Data_DN1newStd); // 1 is the index of the experiment


        TableCourse* Data_DN1KOnew = new TableCourse(folder + dataplace + string("DN1_KO_POOL_AVG.txt"));
        cout << Data_DN1KOnew->print() << endl;
        TableCourse* Data_DN1KOnewStd = new TableCourse(folder + dataplace + string("DN1_KO_POOL_STD.txt"));
        cout << Data_DN1KOnewStd->print() << endl;

        currentExperiment->giveData(Data_DN1KOnew, Backgrounds::DKO, Data_DN1KOnewStd); // 1 is the index of the experiment

        break;
    }

    // New way of processing the data
    case 26: case 76: { // NewDN2_WT and KO
        currentExperiment = new expDoubleLabelWT_KO(currentModel);
        currentExperiment->Identification += "NewDN2_WT_KO" + DNA;
        configFile=string("ConfigDN2a_WT_KO.txt");

        configFile=string("Configs/DN2_WT_KO_1VarG1diff.txt");
        if(nb == 76) configFile=string("Configs/DN2_WT_KO_AllVarTotdiff.txt");

        string dataplace2 = "DATA/4TP_Data1Data2Data3Data4_EduBrdu_Return_Steady_DNA/";
        //TableCourse* Data_DN2new = new TableCourse(folder + dataplace2 + string("DN2_WT_POOL_AVG") + DNA + string(".txt"));
        TableCourse* Data_DN2new = new TableCourse(folder + dataplace2 + string("DN2_WT_POOL_AVG") + string(".txt"));
        cout << Data_DN2new->print() << endl;
        //TableCourse* Data_DN2newStd = new TableCourse(folder + dataplace2 + string("DN2_WT_POOL_STD") + DNA + string(".txt"));
        TableCourse* Data_DN2newStd = new TableCourse(folder + dataplace2 + string("DN2_WT_POOL_STD") + string(".txt"));
        cout << Data_DN2newStd->print() << endl;

        currentExperiment->giveData(Data_DN2new, Backgrounds::WT, Data_DN2newStd); // 1 is the index of the experiment


        //TableCourse* Data_DN2KOnew = new TableCourse(folder + dataplace2 + string("DN2_KO_POOL_AVG") + DNA + string(".txt"));
        TableCourse* Data_DN2KOnew = new TableCourse(folder + dataplace2 + string("DN2_KO_POOL_AVG") + string(".txt"));
        cout << Data_DN2KOnew->print() << endl;
        //TableCourse* Data_DN2KOnewStd = new TableCourse(folder + dataplace2 + string("DN2_KO_POOL_STD") + DNA + string(".txt"));
        TableCourse* Data_DN2KOnewStd = new TableCourse(folder + dataplace2 + string("DN2_KO_POOL_STD") + string(".txt"));
        cout << Data_DN2KOnewStd->print() << endl;

        currentExperiment->giveData(Data_DN2KOnew, Backgrounds::DKO, Data_DN2KOnewStd); // 1 is the index of the experiment

        break;
    }

    // New way of processing the data
    case 27: case 77: { // NewDN3A_WT and KO
        currentExperiment = new expDoubleLabelWT_KO(currentModel);
        currentExperiment->Identification += "NewDN3A_WT_KO" + DNA;
        configFile=string("ConfigDN3Aa_WT_KO.txt");

        configFile=string("Configs/DN3a_WT_KO_1VarG1diff.txt");
        if(nb == 77) configFile=string("Configs/DN3a_WT_KO_AllVarTotdiff.txt");

        TableCourse* Data_DN3Anew = new TableCourse(folder + dataplace + string("DN3A_WT_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN3Anew->print() << endl;
        TableCourse* Data_DN3AnewStd = new TableCourse(folder + dataplace + string("DN3A_WT_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN3AnewStd->print() << endl;

        currentExperiment->giveData(Data_DN3Anew, Backgrounds::WT, Data_DN3AnewStd); // 1 is the index of the experiment


        TableCourse* Data_DN3AKOnew = new TableCourse(folder + dataplace + string("DN3A_KO_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN3AKOnew->print() << endl;
        TableCourse* Data_DN3AKOnewStd = new TableCourse(folder + dataplace + string("DN3A_KO_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN3AKOnewStd->print() << endl;

        currentExperiment->giveData(Data_DN3AKOnew, Backgrounds::DKO, Data_DN3AKOnewStd); // 1 is the index of the experiment

        break;
    }

    case 28: case 78: { // NewDN3B_WT and KO
        string dataplace2 = "DATA/4TP_Data1Data2Data3Data4_EduBrdu_Return_Steady_DNA/";
        currentExperiment = new expDoubleLabelWT_KO(currentModel);
        currentExperiment->Identification += "NewDN3B_WT_KO" + DNA;
        configFile=string("ConfigDN3Ba_WT_KO.txt");

        configFile=string("Configs/DN3b_WT_KO_1VarG1diff.txt");
        configFile=string("Configs/ConfigDN3b_WT_KO_V3.txt");
        if(nb == 78) configFile=string("Configs/DN3b_WT_KO_AllVarTotdiff.txt");

        //TableCourse* Data_DN3Bnew = new TableCourse(folder + dataplace + string("DN3B_WT_POOL_AVG") + DNA + string(".txt"));
        TableCourse* Data_DN3Bnew = new TableCourse(folder + dataplace2 + string("DN3B_WT_POOL_AVG") + string(".txt"));
        cout << Data_DN3Bnew->print() << endl;
        //TableCourse* Data_DN3BnewStd = new TableCourse(folder + dataplace + string("DN3B_WT_POOL_STD") + DNA + string(".txt"));
        TableCourse* Data_DN3BnewStd = new TableCourse(folder + dataplace2 + string("DN3B_WT_POOL_STD") + string(".txt"));
        cout << Data_DN3BnewStd->print() << endl;

        currentExperiment->giveData(Data_DN3Bnew, Backgrounds::WT, Data_DN3BnewStd); // 1 is the index of the experiment


        // TableCourse* Data_DN3BKOnew = new TableCourse(folder + dataplace + string("DN3B_KO_POOL_AVG") + DNA + string(".txt"));
        TableCourse* Data_DN3BKOnew = new TableCourse(folder + dataplace2 + string("DN3B_KO_POOL_AVG") + string(".txt"));
        cout << Data_DN3BKOnew->print() << endl;
        // TableCourse* Data_DN3BKOnewStd = new TableCourse(folder + dataplace + string("DN3B_KO_POOL_STD") + DNA + string(".txt"));
        TableCourse* Data_DN3BKOnewStd = new TableCourse(folder + dataplace2 + string("DN3B_KO_POOL_STD") + string(".txt"));
        cout << Data_DN3BKOnewStd->print() << endl;

        currentExperiment->giveData(Data_DN3BKOnew, Backgrounds::DKO, Data_DN3BKOnewStd); // 1 is the index of the experiment

        break;
    }
    case 29: case 79: { // NewDN4_WT and KO
        currentExperiment = new expDoubleLabelWT_KO(currentModel);
        currentExperiment->Identification += "NewDN4_WT_KO" + DNA;
        configFile=string("ConfigDN4a_WT_KO.txt");

        configFile=string("Configs/DN4_WT_KO_1VarG1diff.txt");
        if(nb == 79) configFile=string("Configs/DN4_WT_KO_AllVarTotdiff.txt");

        TableCourse* Data_DN4new = new TableCourse(folder + dataplace + string("DN4_WT_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN4new->print() << endl;
        TableCourse* Data_DN4newStd = new TableCourse(folder + dataplace + string("DN4_WT_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN4newStd->print() << endl;

        currentExperiment->giveData(Data_DN4new, Backgrounds::WT, Data_DN4newStd); // 1 is the index of the experiment


        TableCourse* Data_DN4KOnew = new TableCourse(folder + dataplace + string("DN4_KO_POOL_AVG") + DNA + string(".txt"));
        cout << Data_DN4KOnew->print() << endl;
        TableCourse* Data_DN4KOnewStd = new TableCourse(folder + dataplace + string("DN4_KO_POOL_STD") + DNA + string(".txt"));
        cout << Data_DN4KOnewStd->print() << endl;

        currentExperiment->giveData(Data_DN4KOnew, Backgrounds::DKO, Data_DN4KOnewStd); // 1 is the index of the experiment

        break;
    }
    case 30: case 80: { // NewpreSelDP_WT and KO
        currentExperiment = new expDoubleLabelWT_KO(currentModel);
        currentExperiment->Identification += "NewpreSelDP_WT_KO" + DNA;
        configFile=string("ConfigpreSelDPa_WT_KO.txt");

        configFile=string("Configs/PreSelDP_WT_KO_1VarG1diff.txt");
        if(nb == 80) configFile=string("Configs/PreSelDP_WT_KO_AllVarTotdiff.txt");

        TableCourse* Data_preSelDPnew = new TableCourse(folder + dataplace + string("preSel_WT_POOL_AVG") + DNA + string(".txt"));
        cout << Data_preSelDPnew->print() << endl;
        TableCourse* Data_preSelDPnewStd = new TableCourse(folder + dataplace + string("preSel_WT_POOL_STD") + DNA + string(".txt"));
        cout << Data_preSelDPnewStd->print() << endl;

        currentExperiment->giveData(Data_preSelDPnew, Backgrounds::WT, Data_preSelDPnewStd); // 1 is the index of the experiment


        TableCourse* Data_preSelDPKOnew = new TableCourse(folder + dataplace + string("preSel_KO_POOL_AVG") + DNA + string(".txt"));
        cout << Data_preSelDPKOnew->print() << endl;
        TableCourse* Data_preSelDPKOnewStd = new TableCourse(folder + dataplace + string("preSel_KO_POOL_STD") + DNA + string(".txt"));
        cout << Data_preSelDPKOnewStd->print() << endl;

        currentExperiment->giveData(Data_preSelDPKOnew, Backgrounds::DKO, Data_preSelDPKOnewStd); // 1 is the index of the experiment

        break;
    }
    case 31: { // NewpostSelDP_WT and KO
        currentExperiment = new expDoubleLabelWT_KO(currentModel);
        currentExperiment->Identification += "NewpostSelDP_WT_KO" + DNA;
        configFile=string("ConfigpostSelDPa_WT_KO.txt");

        TableCourse* Data_postSelDPnew = new TableCourse(folder + dataplace + string("postSelDP_WT_POOL_AVG") + DNA + string(".txt"));
        cout << Data_postSelDPnew->print() << endl;
        TableCourse* Data_postSelDPnewStd = new TableCourse(folder + dataplace + string("postSelDP_WT_POOL_STD") + DNA + string(".txt"));
        cout << Data_postSelDPnewStd->print() << endl;

        currentExperiment->giveData(Data_postSelDPnew, Backgrounds::WT, Data_postSelDPnewStd); // 1 is the index of the experiment


        TableCourse* Data_postSelDPKOnew = new TableCourse(folder + dataplace + string("postSelDP_KO_POOL_AVG") + DNA + string(".txt"));
        cout << Data_postSelDPKOnew->print() << endl;
        TableCourse* Data_postSelDPKOnewStd = new TableCourse(folder + dataplace + string("postSelDP_KO_POOL_STD") + DNA + string(".txt"));
        cout << Data_postSelDPKOnewStd->print() << endl;

        currentExperiment->giveData(Data_postSelDPKOnew, Backgrounds::DKO, Data_postSelDPKOnewStd); // 1 is the index of the experiment

        break;
    }
    case 32: { // NewCD4SP_WT and KO, no DNA data
        currentExperiment = new expDoubleLabelWT_KO(currentModel);
        currentExperiment->Identification += "NewCD4SP_WT_KO_noDNA";
        configFile=string("ConfigCD4SPa_WT_KO.txt");

        TableCourse* Data_CD4SPnew = new TableCourse(folder + dataplace + string("CD4SP_WT_POOL_AVG.txt"));
        cout << Data_CD4SPnew->print() << endl;
        TableCourse* Data_CD4SPnewStd = new TableCourse(folder + dataplace + string("CD4SP_WT_POOL_STD.txt"));
        cout << Data_CD4SPnewStd->print() << endl;

        currentExperiment->giveData(Data_CD4SPnew, Backgrounds::WT, Data_CD4SPnewStd); // 1 is the index of the experiment


        TableCourse* Data_CD4SPKOnew = new TableCourse(folder + dataplace + string("CD4SP_KO_POOL_AVG.txt"));
        cout << Data_CD4SPKOnew->print() << endl;
        TableCourse* Data_CD4SPKOnewStd = new TableCourse(folder + dataplace + string("CD4SP_KO_POOL_STD.txt"));
        cout << Data_CD4SPKOnewStd->print() << endl;

        currentExperiment->giveData(Data_CD4SPKOnew, Backgrounds::DKO, Data_CD4SPKOnewStd); // 1 is the index of the experiment

        break;
    }
    case 33: { // NewCD8SP_WT and KO, no DNA data
        currentExperiment = new expDoubleLabelWT_KO(currentModel);
        currentExperiment->Identification += "NewCD8SP_WT_KO_npDNA";
        configFile=string("ConfigCD8SPa_WT_KO.txt");

        TableCourse* Data_CD8SPnew = new TableCourse(folder + dataplace + string("CD8SP_WT_POOL_AVG.txt"));
        cout << Data_CD8SPnew->print() << endl;
        TableCourse* Data_CD8SPnewStd = new TableCourse(folder + dataplace + string("CD8SP_WT_POOL_STD.txt"));
        cout << Data_CD8SPnewStd->print() << endl;

        currentExperiment->giveData(Data_CD8SPnew, Backgrounds::WT, Data_CD8SPnewStd); // 1 is the index of the experiment


        TableCourse* Data_CD8SPKOnew = new TableCourse(folder + dataplace + string("CD8SP_KO_POOL_AVG.txt"));
        cout << Data_CD8SPKOnew->print() << endl;
        TableCourse* Data_CD8SPKOnewStd = new TableCourse(folder + dataplace + string("CD8SP_KO_POOL_STD.txt"));
        cout << Data_CD8SPKOnewStd->print() << endl;

        currentExperiment->giveData(Data_CD8SPKOnew, Backgrounds::DKO, Data_CD8SPKOnewStd); // 1 is the index of the experiment

        break;
    }












        // New way of processing the data
        case 326: case 376: { // NewDN2_CTR and IRR, no DNA data
            currentExperiment = new expDoubleLabelWT_KO(currentModel);
            currentExperiment->Identification += "NewDN2_CTR_IRR" + DNA;
            configFile=string("ConfigDN2a_WT_KO.txt");

            configFile=string("Configs/DN2_WT_KO_1VarG1diff.txt");
            if(nb == 376) configFile=string("Configs/DN2_WT_KO_AllVarTotdiff.txt");

            TableCourse* Data_DN2new = new TableCourse(folder + dataplace + string("RegDN2_WT_AVG.txt"));
            cout << Data_DN2new->print() << endl;
            TableCourse* Data_DN2newStd = new TableCourse(folder + dataplace + string("RegDN2_WT_STD.txt"));
            cout << Data_DN2newStd->print() << endl;

            currentExperiment->giveData(Data_DN2new, Backgrounds::WT, Data_DN2newStd); // 1 is the index of the experiment


            TableCourse* Data_DN2KOnew = new TableCourse(folder + dataplace + string("RegDN2_IRR_AVG.txt"));
            cout << Data_DN2KOnew->print() << endl;
            TableCourse* Data_DN2KOnewStd = new TableCourse(folder + dataplace + string("RegDN2_IRR_STD.txt"));
            cout << Data_DN2KOnewStd->print() << endl;

            currentExperiment->giveData(Data_DN2KOnew, Backgrounds::DKO, Data_DN2KOnewStd); // 1 is the index of the experiment

            break;
        }

        // New way of processing the data
        case 327: case 377: { // NewDN3A_WT and KO, no DNA data
            currentExperiment = new expDoubleLabelWT_KO(currentModel);
            currentExperiment->Identification += "NewDN3A_CTR_IRR" + DNA;
            configFile=string("ConfigDN3Aa_WT_KO.txt");

            configFile=string("Configs/DN3a_WT_KO_1VarG1diff.txt");
            if(nb == 377) configFile=string("Configs/DN3a_WT_KO_AllVarTotdiff.txt");

            TableCourse* Data_DN3Anew = new TableCourse(folder + dataplace + string("RegDN3A_WT_AVG.txt"));
            cout << Data_DN3Anew->print() << endl;
            TableCourse* Data_DN3AnewStd = new TableCourse(folder + dataplace + string("RegDN3A_WT_STD.txt"));
            cout << Data_DN3AnewStd->print() << endl;

            currentExperiment->giveData(Data_DN3Anew, Backgrounds::WT, Data_DN3AnewStd); // 1 is the index of the experiment


            TableCourse* Data_DN3AKOnew = new TableCourse(folder + dataplace + string("RegDN3A_IRR_AVG.txt"));
            cout << Data_DN3AKOnew->print() << endl;
            TableCourse* Data_DN3AKOnewStd = new TableCourse(folder + dataplace + string("RegDN3A_IRR_STD.txt"));
            cout << Data_DN3AKOnewStd->print() << endl;

            currentExperiment->giveData(Data_DN3AKOnew, Backgrounds::DKO, Data_DN3AKOnewStd); // 1 is the index of the experiment

            break;
        }

        case 328: case 378: { // NewDN3B_CTR and IRR, no DNA data
            currentExperiment = new expDoubleLabelWT_KO(currentModel);
            currentExperiment->Identification += "NewDN3B_WT_KO" + DNA;
            configFile=string("ConfigDN3Ba_WT_KO.txt");

            configFile=string("Configs/DN3b_WT_KO_1VarG1diff.txt");
            configFile=string("Configs/ConfigDN3b_WT_KO_V3.txt");
            if(nb == 378) configFile=string("Configs/DN3b_WT_KO_AllVarTotdiff.txt");

            TableCourse* Data_DN3Bnew = new TableCourse(folder + dataplace + string("RegDN3B_WT_AVG.txt"));
            cout << Data_DN3Bnew->print() << endl;
            TableCourse* Data_DN3BnewStd = new TableCourse(folder + dataplace + string("RegDN3B_WT_STD.txt"));
            cout << Data_DN3BnewStd->print() << endl;

            currentExperiment->giveData(Data_DN3Bnew, Backgrounds::WT, Data_DN3BnewStd); // 1 is the index of the experiment


            TableCourse* Data_DN3BKOnew = new TableCourse(folder + dataplace + string("RegDN3B_IRR_AVG.txt"));
            cout << Data_DN3BKOnew->print() << endl;
            TableCourse* Data_DN3BKOnewStd = new TableCourse(folder + dataplace + string("RegDN3B_IRR_STD.txt"));
            cout << Data_DN3BKOnewStd->print() << endl;

            currentExperiment->giveData(Data_DN3BKOnew, Backgrounds::DKO, Data_DN3BKOnewStd); // 1 is the index of the experiment

            break;
        }
        case 329: case 379: { // NewDN4_CTR and IRR, no DNA data
            currentExperiment = new expDoubleLabelWT_KO(currentModel);
            currentExperiment->Identification += "NewDN4_WT_KO" + DNA;
            configFile=string("ConfigDN4a_WT_KO.txt");

            configFile=string("Configs/DN4_WT_KO_1VarG1diff.txt");
            if(nb == 379) configFile=string("Configs/DN4_WT_KO_AllVarTotdiff.txt");

            TableCourse* Data_DN4new = new TableCourse(folder + dataplace + string("RegDN4_WT_AVG.txt"));
            cout << Data_DN4new->print() << endl;
            TableCourse* Data_DN4newStd = new TableCourse(folder + dataplace + string("RegDN4_WT_STD.txt"));
            cout << Data_DN4newStd->print() << endl;

            currentExperiment->giveData(Data_DN4new, Backgrounds::WT, Data_DN4newStd); // 1 is the index of the experiment


            TableCourse* Data_DN4KOnew = new TableCourse(folder + dataplace + string("RegDN4_IRR_AVG.txt"));
            cout << Data_DN4KOnew->print() << endl;
            TableCourse* Data_DN4KOnewStd = new TableCourse(folder + dataplace + string("RegDN4_IRR_STD.txt"));
            cout << Data_DN4KOnewStd->print() << endl;

            currentExperiment->giveData(Data_DN4KOnew, Backgrounds::DKO, Data_DN4KOnewStd); // 1 is the index of the experiment

            break;
        }
        case 330: case 380: { // NewpreSelDP_CTR and IRR, no DNA data
            currentExperiment = new expDoubleLabelWT_KO(currentModel);
            currentExperiment->Identification += "NewpreSelDP_WT_KO" + DNA;
            configFile=string("ConfigpreSelDPa_WT_KO.txt");

            configFile=string("Configs/PreSelDP_WT_KO_1VarG1diff.txt");
            if(nb == 380) configFile=string("Configs/PreSelDP_WT_KO_AllVarTotdiff.txt");

            TableCourse* Data_preSelDPnew = new TableCourse(folder + dataplace + string("RegPRE_DP_WT_AVG.txt"));
            cout << Data_preSelDPnew->print() << endl;
            TableCourse* Data_preSelDPnewStd = new TableCourse(folder + dataplace + string("RegPRE_DP_WT_STD.txt"));
            cout << Data_preSelDPnewStd->print() << endl;

            currentExperiment->giveData(Data_preSelDPnew, Backgrounds::WT, Data_preSelDPnewStd); // 1 is the index of the experiment


            TableCourse* Data_preSelDPKOnew = new TableCourse(folder + dataplace + string("RegPRE_DP_IRR_AVG.txt"));
            cout << Data_preSelDPKOnew->print() << endl;
            TableCourse* Data_preSelDPKOnewStd = new TableCourse(folder + dataplace + string("RegPRE_DP_IRR_STD.txt"));
            cout << Data_preSelDPKOnewStd->print() << endl;

            currentExperiment->giveData(Data_preSelDPKOnew, Backgrounds::DKO, Data_preSelDPKOnewStd); // 1 is the index of the experiment

            break;
        }
        case 331: { // NewpostSelDP_CTR and IRR, no DNA data
            currentExperiment = new expDoubleLabelWT_KO(currentModel);
            currentExperiment->Identification += "NewpostSelDP_WT_KO" + DNA;
            configFile=string("ConfigpostSelDPa_WT_KO.txt");

            TableCourse* Data_postSelDPnew = new TableCourse(folder + dataplace + string("RegPOST_DP_WT_AVG.txt"));
            cout << Data_postSelDPnew->print() << endl;
            TableCourse* Data_postSelDPnewStd = new TableCourse(folder + dataplace + string("RegPOST_DP_WT_STD.txt"));
            cout << Data_postSelDPnewStd->print() << endl;

            currentExperiment->giveData(Data_postSelDPnew, Backgrounds::WT, Data_postSelDPnewStd); // 1 is the index of the experiment


            TableCourse* Data_postSelDPKOnew = new TableCourse(folder + dataplace + string("RegPOST_DP_IRR_AVG.txt"));
            cout << Data_postSelDPKOnew->print() << endl;
            TableCourse* Data_postSelDPKOnewStd = new TableCourse(folder + dataplace + string("RegPOST_DP_IRR_STD.txt"));
            cout << Data_postSelDPKOnewStd->print() << endl;

            currentExperiment->giveData(Data_postSelDPKOnew, Backgrounds::DKO, Data_postSelDPKOnewStd); // 1 is the index of the experiment

            break;
        }











//    case 31: { // NewDNX_WT and KO, no DNA data
//        currentExperiment = new expDoubleLabelWT_KO(currentModel);
//        currentExperiment->Identification += "NewDNX_WT_KO";
//        configFile=string("ConfigDNXa_WT_KO.txt");

//        TableCourse* Data_DNXnew = new TableCourse(folder + dataplace + string("DNX_WT_POOL_AVG.txt"));
//        cout << Data_DNXnew->print() << endl;
//        TableCourse* Data_DNXnewStd = new TableCourse(folder + dataplace + string("DNX_WT_POOL_STD.txt"));
//        cout << Data_DNXnewStd->print() << endl;

//        currentExperiment->giveData(Data_DNXnew, Backgrounds::WT, Data_DNXnewStd); // 1 is the index of the experiment


//        TableCourse* Data_DNXKOnew = new TableCourse(folder + dataplace + string("DNX_KO_POOL_AVG.txt"));
//        cout << Data_DNXKOnew->print() << endl;
//        TableCourse* Data_DNXKOnewStd = new TableCourse(folder + dataplace + string("DNX_KO_POOL_STD.txt"));
//        cout << Data_DNXKOnewStd->print() << endl;

//        currentExperiment->giveData(Data_DNXKOnew, Backgrounds::DKO, Data_DNXKOnewStd); // 1 is the index of the experiment

//        break;
//    }


    default: { // Thymus model Thomas-Vaslin JI 2007 + single ODEs other ones
        break;
    }

    }// end switch

    if(newConfigFile.size() > 0)   {configFile = newConfigFile; cout << "   -> forcing configuration : " << newConfigFile << endl;}
    if(newParameterSet.size() > 0)  {
        cout << "   -> forcing parameter set : " << newParameterSet << endl;
        currentModel->loadParameters(newParameterSet);
    }


   // Step 6: Says to convert the data into evaluators (variable + time + exp Value + stddev (if applies))

    currentExperiment->loadEvaluators();




    cout << "   -> Using model : " << currentModel->name << " with experiment " << currentExperiment->Identification << "\n";




    //    case SQUARE_COST: res << "RSS"; break;
    //    case SQUARE_COST_STD: res << "RSS + StdDev"; break;
    //    case LOG_COST: res << "Log"; break;
    //    case PROPORTION_COST: res << "Ratios"; break;


    //    case NO_NORM: res << " (No Norm)"; break;
    //    case NORM_AVERAGE: res << " Norm/Avg vars"; break;
    //    case NORM_NB_PTS: res << " Norm/Nb Points"; break;
    //    case NORM_AVG_AND_NB_PTS: res << " Norm/Avg and Nb Pts "; break;
    //    case NORM_MAX: res << " Norm/Max vars"; break;
    //    case NORM_MAX_AND_NB_PTS: res << " Norm/Max and Nb Pts"; break;

    // Set the costs before calling the graphical interface, because it will not update once it's called
    // (although you would change the cost, the GUI would not realize and keep showing the old one)
    setTypeCost(SQUARE_COST);
    setTypeNorm(NORM_AVERAGE);




    #ifndef WITHOUT_QT
    cout << "Launching Graphical Interface ..." << endl;
    // Step 7a: Launch the graphical interface from an experiment (containing the model inside)
    simuWin* p = new simuWin(currentExperiment);

    // Step 7b: optionally give a config file
    p->loadConfig(folder + configFile);

    // Step 7c: show the graphical interface!
    p->show();

    // Step 7d: leave the control to Qt instead of finishing the program
    b.exec();

    // That's it !

    #else

    // Step 7a: Launch the graphical interface from an experiment (containing the model inside)

    currentExperiment->m->setBaseParameters();
    manageSims* MSim = new manageSims(currentExperiment);

    // Step 7b: optionally give a config file
    MSim->loadConfig(folder + configFile);

    int nbCombinations = MSim->nbCombs;
    int nRep = 10;

    for(int i = 0; i < nbCombinations; ++i){
        stringstream OptForComb; OptForComb << "OptimizerCase" << nb << "_Comb" << i << ".txt";
        string optimizerName = folder + OptForComb.str();
        string contentOpt = MSim->motherCreateOptimizerFile(i,optFileHeader(Genetic25k));
        ofstream optim(optimizerName.c_str());
        optim << contentOpt;
        optim.close();
        cout << OptForComb.str() << endl;

        for(int j = 0; j < nRep; ++j){
            stringstream codeSim; codeSim << "Case" << nb << "_Comb" << i << "_Rep" << j;
            createFolder(folder + codeSim.str());

            cout << codeSim.str() << endl;
            MSim->resetParamSetFromConfig(folder + configFile);
            MSim->initialize();
            MSim->motherOptimize(optimizerName, 10000);
            MSim->saveHistory(folder + codeSim.str() + "/TestHistory.txt");                         // SAVES all the best parameter sets. by default, 10 000, can be modified by             msi->history.resize(max_nb_sets_to_record);
            MSim->useParamSetFromHistory(0);                                                     // takes the first set of parameters (the best), also possible to use msi->useParamSetFromHistory(0, i); for overriding only parameters from this combination,
            MSim->simulate();
            cout << "Sim OK, cost " << MSim->getCost() << endl;
            MSim->makeTextReportParamSet(folder + codeSim.str() + "/", i);
        }
    }
    // if want to do identifiability, here:
    if(0){
        int idParam = dividingModel::avgG1;
        int idConfig = 1;
        stringstream codeIdentif; codeIdentif << "Case" << nb << "_Identif" << idParam << "/";
        createFolder(folder + codeIdentif.str());

        MSim->resetParamSetFromConfig(folder + configFile);
        vector<double> paramsRef = currentModel->getParameters();
        MSim->prepareOptFilesForIdentifibiality(folder + codeIdentif.str(),idParam, idConfig, optFileHeader(GeneticFast));
        MSim->motherIdentifiability(paramsRef, idParam, 15);
        MSim->makeTextReportParamSet(folder + codeIdentif.str(), idConfig);
    }
    #endif

    cout << "   -> Script " << nb << " finished !\n   _____________________________________________________________\n";
}


// To switch the optimizers in fast mode (just a few simulations).
#define TESTINGMODE 0
#endif
