#ifndef _tk_spline2_h
#define _tk_spline2_h

#include <cstdio>
#include <cassert>
#include <vector>
#include <algorithm>
#include <algorithm>
#include <string>
using namespace std;

#include "spline.h"



// extended version, Philippe Robert 2018-06-01
struct adaptiveSpline {
    int size_groups;
    bool spline3; // false means linear
    int in_current_group; // always points to a position not written yet
    void add(double X, double Y);
    double get(double X);
    int getGroup(double X);
    vector< vector<double>* > listX;
    vector< vector<double>* > listY;
    vector<tk::spline*> listSplines;
    int currentGroup;
    vector<double> minXPerGroup;
    vector<double> maxXPerGroup;
    double lastXinSpline;
    double previousX;
    adaptiveSpline(int _size_groups = 50, bool usingSpline = false);
    ~adaptiveSpline();
    string print();
};



#endif /* _tk_spline_h */
