#include "viewdistrib.h"
#include "ui_viewdistrib.h"

viewDistrib::viewDistrib(DistribWidget* _toUse, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::viewDistrib)
{
    ui->setupUi(this);
    if(_toUse == NULL) return;
    _toUse->setParent(this);
    toUse = _toUse; // be careful that this is not deleted !!!, still owned by another class ...
    currentGraphe = new grapheCustom(ui->widgetForPlotting);
    currentGraphe->setTitle(QString("Distribution"));
    currentGraphe->setNbCurves(2);
    replot();
    QObject::connect(this, SIGNAL(accepted()), this, SLOT(closing()));
    QObject::connect(this, SIGNAL(finished(int)), this, SLOT(closing()));
    ui->lineEditFileDistrib->setFocus();



}



void viewDistrib::closing(){

    toUse->restoreParent(this);
}

void viewDistrib::replot(){
    Law* toPlay = toUse->getLaw();
    if(!toPlay) {cerr << "WRN: viewDistrib::replot(), law not initialized yet.\n"; return;}
    toPlay->setRecord(true);
    for(int i = 0; i < 10000; ++i){
        toPlay->getRandValue();
    }
    histogramFromDistrib a = toPlay->getHistogram();
    //cout << a.print() << endl;
    currentGraphe->Plot(0, a.densities, a.averageXs, QString("measured"), QColor(Qt::blue));
    currentGraphe->rescaleY(0., a.maxDens);
    // currentGraphe->clear();
    toPlay->setRecord(false);
}

viewDistrib::~viewDistrib()
{
    delete ui;
}
