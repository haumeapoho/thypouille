#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <set>
using namespace std;

#include "../Moonfit/moonfit.h"

#include "random.h"
#include "bootstrap.h"
#include "dividingmodelTwoPops.h"
#include "expLabelling.h"


static string folder = "C:/Users/pprobert/Desktop/Softwares/NewArchaeropteryx/Sources/Meluriana/";
static string folderBaseResults = "C:/Users/pprobert/Desktop/Softwares/NewArchaeropteryx/Sources/Meluriana/Results/";

// to call it: ./Meluriana synchro DN2 WT startS
int mainSynchro(int argc, char *argv[]){

    if(!dirExists(folder)) cerr << "Could not find the project folder for finding data and configurations: " << folder << endl;
    if(!dirExists(folderBaseResults.c_str())) createFolder(folderBaseResults);

    // Filtering input in a list of authorized options
    string command           = string(argv[1]);
    string population        = string(argv[2]);
    string condition         = (argc >= 4) ? string(argv[3]) : "WT";
    string startPhase        = (argc >= 5) ? string(argv[4]) : "startS";

    set<string> commands = {"twoPops"};
    set<string> populations = {"DN1", "DN2", "DN3A", "DN3B", "DN4", "preSel", "postSel", "CD4SP", "CD8SP"};
    set<string> conditions = {"WT", "KO", "CTR", "IRR", "WT_KO", "CTR_IRR"};

    bool success = true;
    if(commands.find(command)                  == commands.end()) {success = false; cerr << "option " << command << " not understood" << endl;}
    if(populations.find(population)            == populations.end()) {success = false; cerr << "option " << population << " not understood" << endl;}
    if(conditions.find(condition)              == conditions.end()) {success = false; cerr << "option " << condition << " not understood" << endl;}
    if(!success) {return 0;}

    Model* currentModel = new dividingModelWithLabelledInflow();
    Experiment* currentExperiment = nullptr;

    cout << "Synchronized proliferation is not implemented yet " << endl;
    return 0;
    // ============================================= END =============================


    //currentExperiment = new expSynchronizedCycle(currentModel);
    cout << "   -> Using model : " << currentModel->name << " with experiment " << currentExperiment->Identification << "\n";

    // Creating a code for these settings, and creating an output result folder
    currentExperiment->Identification = "synchro" + population + "+" + condition;
    createFolder(folderBaseResults + currentExperiment->Identification);
    cout << "Will save results in " << folderBaseResults + currentExperiment->Identification << endl;


    cout << "   -> Using model : " << currentModel->name << " with experiment " << currentExperiment->Identification << "\n";

    #ifndef WITHOUT_QT
    QApplication b(argc, argv);             // Starts the Qt application

    cout << "Launching Graphical Interface ..." << endl;
    // Step 7a: Launch the graphical interface from an experiment (containing the model inside)
    simuWin* p = new simuWin(currentExperiment);


    // Step 7c: show the graphical interface!
    p->show();

    // Step 7d: leave the control to Qt instead of finishing the program
    b.exec();

    #else
    cerr << "You have compiled Meluriana without the graphical libraries, so the 'gui' option doesn't run. Check that #WITHOUT_QT is not defined, then recompile" << endl;
    #endif

    cout << " ===== The End! ==== " << endl;
    return 0;
}
